package utils;

import staticData.Colors;

import java.awt.*;
import javax.swing.*;

/**
 * Broken code taken from https://examples.javacodegeeks.com/desktop-java/swing/java-swing-date-picker-example/
 * and fixed to a working state
 */
public class DatePicker {

    private int month = java.util.Calendar.getInstance().get(java.util.Calendar.MONTH);
    private int year = java.util.Calendar.getInstance().get(java.util.Calendar.YEAR);
    private String day = "";

    private JLabel monthAndYearLabel = new JLabel("", JLabel.CENTER);
    private JDialog jPopUpDialog;
    private JButton[] dayButtons = new JButton[49];

    public DatePicker(JFrame parent) {

        jPopUpDialog = new JDialog(parent);
        jPopUpDialog.setModal(true);

        String[] header = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        JPanel calendarButtonPanel = new JPanel(new GridLayout(7, 7));
        calendarButtonPanel.setPreferredSize(new Dimension(530, 220));

        Font bold = new Font(Font.SANS_SERIF, Font.BOLD, 12);

        for (int x = 0; x < dayButtons.length; x++) {
            dayButtons[x] = new JButton();

            if (x > 6) {
                dayButtons[x].addActionListener(actionEvent -> {
                    day = actionEvent.getActionCommand();
                    if (!day.equals("")) {
                        jPopUpDialog.dispose();
                    }
                });
            }

            if (x < 7) {
                dayButtons[x].setText(header[x]);
                Colors.setButtonColors(dayButtons[x], Colors.PRIMARY, Colors.WHITE);
            }

            dayButtons[x].setFont(bold);
            calendarButtonPanel.add(dayButtons[x]);
        }

        JPanel bottomPanel = new JPanel(new GridLayout(1, 3));
        JButton prev = new JButton("Prev");
        JButton next = new JButton("Next");

        prev.addActionListener(ae -> {
            month--;
            displayDate();
        });

        next.addActionListener(ae -> {
            month++;
            displayDate();
        });

        Colors.setButtonColors(prev, Colors.DANGER, Colors.WHITE);
        Colors.setButtonColors(next, Colors.DANGER, Colors.WHITE);
        monthAndYearLabel.setFont(bold);

        bottomPanel.add(prev);
        bottomPanel.add(monthAndYearLabel);
        bottomPanel.add(next);

        jPopUpDialog.add(calendarButtonPanel, BorderLayout.CENTER);
        jPopUpDialog.add(bottomPanel, BorderLayout.SOUTH);
        jPopUpDialog.pack();

        displayDate();
        jPopUpDialog.setVisible(true);
    }

    private void displayDate() {
        for (int x = 7; x < dayButtons.length; x++) {
            dayButtons[x].setText("");
        }

        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMM yyyy");
        java.util.Calendar cal = java.util.Calendar.getInstance();

        cal.set(year, month, 1);

        int dayOfWeek = cal.get(java.util.Calendar.DAY_OF_WEEK);
        int daysInMonth = cal.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);

        for (int x = 6 + dayOfWeek, day = 1; day <= daysInMonth; x++, day++) {
            dayButtons[x].setText("" + day);
        }

        monthAndYearLabel.setText(sdf.format(cal.getTime()));
        jPopUpDialog.setTitle("Date Picker");
    }

    /**
     * Retrieve the SimpleDate object from the currently picked date in the calendar
     * @return The SimpleDate object
     */
    public SimpleDate getDate() {
        SimpleDate simpleDate = new SimpleDate();
        try {
            simpleDate.setDay(Integer.parseInt(day));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        simpleDate.setMonth(month + 1);
        simpleDate.setYear(year);
        return simpleDate;
    }
}
