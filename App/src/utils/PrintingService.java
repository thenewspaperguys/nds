package utils;

import dataStructures.*;

import java.util.ArrayList;

public class PrintingService {

    private String output;
    private Settings settings;

    public PrintingService(Settings settings) {
        this.settings = settings;
    }

    /**
     * Constructor with the Invoice object as parameter
     *
     * @param invoice The invoice that will be converted to a string
     * @param settings The settings object
     */
    public PrintingService(Invoice invoice, Settings settings) {
        this.output = invoiceToString(invoice);
        this.settings = settings;
    }

    /**
     * Constructor with the DeliveryList object as parameter
     *
     * @param deliveryList The delivery list that will be converted to a string
     * @param settings The settings object
     */
    public PrintingService(DeliveryList deliveryList, Settings settings) {
        this.output = deliveryListToString(deliveryList);
        this.settings = settings;
    }

    /**
     * Constructor with the ArrayList<Delivery> object as parameter
     * @param summaryList The list of all deliveries for the summary
     * @param settings The settings object
     */
    public PrintingService(ArrayList<Delivery> summaryList, Settings settings) {
        this.output = summaryListToString(summaryList);
        this.settings = settings;
    }

    /**
     * Constructor with the ArrayList<Delivery> object as parameter
     * @param summaryList The list of all deliveries for the summary
     * @param year The year of the annual report
     * @param settings The settings object
     */
    public PrintingService(ArrayList<Delivery> summaryList, int year, Settings settings) {
        this.output = reportListToString(summaryList, year);
        this.settings = settings;
    }

    /**
     * Convert the delivery list to a string
     *
     * @param reportList The delivery list that will be converted to a string for a daily summary
     * @return The stringified delivery list
     */
    private String reportListToString(ArrayList<Delivery> reportList, int year) {

        String date = "" + year;
        String outputString = "";
        ArrayList<Integer> distinctCustomers = Helpers.getDistinctCustomersFromDeliveries(reportList);
        double saleTotal = 0;

        // add a heading
        outputString += hr("=");
        outputString += "\n A N N U A L  S U M M A R Y";
        outputString += hr("=");
        outputString += br();

        // add deliveries on a per customer basis
        for (Integer customerID : distinctCustomers) {
            double perCustomerSale = 0;

            // add customer info
            Customer customer = Helpers.getCustomerByIDFromDelivery(customerID, reportList);
            outputString += hr("=");
            outputString += "\nCustomer Name : " + customer.getName();
            outputString += "\nAddress       : " + customer.getAddress();
            outputString += hr("-");
            outputString += String.format("\n%-30s %-20s %-20s", "Publication Name", "Qty", "Value");
            outputString += hr("-");

            for (Delivery delivery : reportList) {
                if (customerID == delivery.getOrder().getCustomer().getID()) {
                    // add publication info
                    String publicationName = delivery.getOrder().getPublication().getTitle();
                    int qty = delivery.getOrder().getQty();
                    double price = delivery.getOrder().getPublication().getPrice();
                    double value = price * qty;
                    outputString += String.format("\n%-30s %-20d %-20.2f", publicationName, qty, value);

                    perCustomerSale += value;
                }
            }
            saleTotal += perCustomerSale;

            outputString += hr("-");
            outputString += String.format("\n%51s %-20.2f", "Total : ", perCustomerSale);
            outputString += hr("=");
            outputString += br();
            outputString += br();
        }

        outputString += hr("*");
        outputString += String.format("\n%40s %15.2f Euro", "Total Sales in " + date , saleTotal);
        outputString += hr("*");
        outputString += br();
        outputString += br();

        return outputString;
    }
    /**
     * Convert the delivery list to a string
     *
     * @param summaryList The delivery list that will be converted to a string for a daily summary
     * @return The stringified delivery list
     */
    private String summaryListToString(ArrayList<Delivery> summaryList) {

        String date = summaryList.get(0).getDeliveryDate().toString();
        String outputString = "";
        ArrayList<Integer> distinctCustomers = Helpers.getDistinctCustomersFromDeliveries(summaryList);
        double saleTotal = 0;

        // add a heading
        outputString += hr("=");
        outputString += "\n D A I L Y  S U M M A R Y";
        outputString += hr("=");
        outputString += br();

        // add deliveries on a per customer basis
        for (Integer customerID : distinctCustomers) {
            double perCustomerSale = 0;

            // add customer info
            Customer customer = Helpers.getCustomerByIDFromDelivery(customerID, summaryList);
            outputString += hr("=");
            outputString += "\nCustomer Name : " + customer.getName();
            outputString += "\nAddress       : " + customer.getAddress();
            outputString += hr("-");
            outputString += String.format("\n%-30s %-20s %-20s", "Publication Name", "Qty", "Value");
            outputString += hr("-");

            for (Delivery delivery : summaryList) {
                if (customerID == delivery.getOrder().getCustomer().getID()) {
                    // add publication info
                    String publicationName = delivery.getOrder().getPublication().getTitle();
                    int qty = delivery.getOrder().getQty();
                    double price = delivery.getOrder().getPublication().getPrice();
                    double value = price * qty;
                    outputString += String.format("\n%-30s %-20d %-20.2f", publicationName, qty, value);

                    perCustomerSale += value;
                }
            }
            saleTotal += perCustomerSale;

            outputString += hr("-");
            outputString += String.format("\n%51s %-20.2f", "Total : ", perCustomerSale);
            outputString += hr("=");
            outputString += br();
            outputString += br();
        }

        outputString += hr("*");
        outputString += String.format("\n%40s %15.2f Euro", "Total Sales on " + date , saleTotal);
        outputString += hr("*");
        outputString += br();
        outputString += br();

        return outputString;
    }

    /**
     * Convert the invoice to a string
     *
     * @param invoice The invoice that will be converted to a string
     * @return The stringified invoice
     */
    private String invoiceToString(Invoice invoice) {
        String outputString = "";
        double priceTotal = 0;

        // add a heading
        outputString += hr("=");
        outputString += "\n I N V O I C E ";
        outputString += hr("=");
        outputString += br();

        // add the delivery date and the company name on top
        outputString += hr("=");
        String date = invoice.getDeliveryDate().toString();
        outputString += String.format("\n%-30s Date : %-20s", "The Newspaper Guys", date);
        outputString += hr("=");
        outputString += br();

        // add the customer info
        outputString += "\nCustomer Name : " + invoice.getCustomer().getName();
        outputString += "\nAddress       : " + invoice.getCustomer().getAddress();
        outputString += hr("-");
        outputString += String.format("\n%-30s %-10s %-10s", "Publication Name", "Qty", "Price");
        outputString += hr("-");

        // add each publication sold
        for (Publication publication: invoice.getPublications()) {
            String publicationName = publication.getTitle();
            int publicationQty = publication.getQty();
            double publicationPrice = publication.getPrice();
            priceTotal += publicationPrice * publicationQty;

            outputString += String.format("\n%-30s %-10d %-10.2f", publicationName, publicationQty, publicationPrice);
        }

        // add total
        outputString += hr("-");
        outputString += String.format("\n%35s %10.2f Euro", "Total : ", priceTotal);

        return outputString;
    }

    /**
     * Convert the delivery list to a string
     *
     * @param deliveryList The delivery list that will be converted to a string
     * @return The stringified delivery list
     */
    private String deliveryListToString(DeliveryList deliveryList) {
        String outputString = "";
        ArrayList<Integer> distinctCustomers = Helpers.getDistinctCustomersFromDeliveries(deliveryList.getDeliveries());

        // add a heading
        outputString += hr("=");
        outputString += "\n D E L I V E R Y   D O C U M E N T ";
        outputString += hr("=");
        outputString += br();

        // add region name and address
        String regionName = deliveryList.getDeliveries().get(0).getOrder().getCustomer().getRegion();
        String date = new SimpleDate().toString();
        outputString += hr("=");
        outputString += String.format("\nRegion : %-30s Date: %-20s", regionName, date);
        outputString += br();

        // add driver Info
        String driverName = deliveryList.getDriver().getName();
        int driverID = deliveryList.getDriver().getID();
        outputString += String.format("\nDriver : %-30s Driver ID : %-20d", driverName, driverID);
        outputString += hr("=");

        // add deliveries on a per customer basis
        for (Integer customerID : distinctCustomers) {

            // add customer info
            Customer customer = Helpers.getCustomerByIDFromDelivery(customerID, deliveryList.getDeliveries());
            outputString += hr("=");
            outputString += "\nCustomer Name : " + customer.getName();
            outputString += "\nAddress       : " + customer.getAddress();
            outputString += "\nPhone No      : " + customer.getPhoneNumber();
            outputString += "\nEmail         : " + customer.getEmail();
            outputString += hr("-");
            outputString += String.format("\n%-30s %-20s", "Publication Name", "Qty");
            outputString += hr("-");

            for (Delivery delivery : deliveryList.getDeliveries()) {
                if (customerID == delivery.getOrder().getCustomer().getID()) {
                    // add publication info
                    String publicationName = delivery.getOrder().getPublication().getTitle();
                    int qty = delivery.getOrder().getQty();
                    outputString += String.format("\n%-30s %-20d", publicationName, qty);
                }
            }
            outputString += hr("-");
            outputString += br();
            outputString += br();
        }

        return outputString;
    }

    /**
     * Add a horizontal line
     *
     * @param hrType The character type to be repeated
     * @return The line that consists of 79 characters
     */
    private String hr(String hrType) {
        StringBuilder out = new StringBuilder();
        out.append("\n");
        for (int i = 0; i < 79; i++) {
            out.append(hrType);
        }
        return out.toString();
    }

    /**
     * Break line character
     *
     * @return Break line character
     */
    private String br() {
        return "\n";
    }

    /**
     * Return the output string
     *
     * @return The string with the Invoice or Delivery List data
     */
    public String toString() {
        return this.output;
    }
}
