package utils;

/**
 * Helpers for casting types quickly
 */
public class Casters {

    /**
     * Parse a string and return an integer, return zero if it fails
     * @param value The string to be parsed
     * @return Return the string cast to an integer. Return zero if it fails
     */
    public static int safeToInt(String value) {
        int output = 0;
        try {
            output = Integer.parseInt(value);
        } catch (Exception e) {
            //
        }
        return output;
    }

    /**
     * Parse a string and return a double, return zero if it fails
     * @param value The string to be parsed
     * @return Return the string cast to a double. Return zero if it fails
     */
    public static double safeToDouble(String value) {
        double output = 0;
        try {
            output = Double.parseDouble(value);
        } catch (Exception e) {
            //
        }
        return output;
    }
}
