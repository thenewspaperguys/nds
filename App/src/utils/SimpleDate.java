package utils;

import java.time.LocalDateTime;

/**
 * SimpleDate is a very simplified version of the Date java class
 */
public class SimpleDate implements Comparable<SimpleDate> {

    private int day = 1, month = 1, year = 1;

    /**
     * Create the date with today's date
     */
    public SimpleDate() {
        now();
    }

    /**
     * Create the date object from a string in the YYYY-MM-DD, YYYY/MM/DD,
     * DD-MM-YYYY or DD/MM/YYYY format
     * 
     * @param date
     *            The date in the string format
     */
    public SimpleDate(String date) {
        parseDate(date);
        validateDate();
    }

    /**
     * Create the date object with integers as parameters
     * 
     * @param day
     *            The day of the month
     * @param month
     *            The month of the year
     * @param year
     *            The year
     */
    public SimpleDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
        validateDate();
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
        validateDate();
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
        validateDate();
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
        validateDate();
    }

    /**
     * Convert a string date to integers
     * 
     * @param date
     *            The string to be parsed
     */
    private void parseDate(String date) {
        String[] dateSplit;
        boolean yearMonthDay = true;

        if (date.matches("[\\d]{4}-[\\d]{2}-[\\d]{2}")) {
            dateSplit = date.split("-");
        } else if (date.matches("[\\d]{4}/[\\d]{2}/[\\d]{2}")) {
            dateSplit = date.split("/");
        } else if (date.matches("[\\d]{2}-[\\d]{2}-[\\d]{4}")) {
            dateSplit = date.split("-");
            yearMonthDay = false;
        } else if (date.matches("[\\d]{2}/[\\d]{2}/[\\d]{4}")) {
            dateSplit = date.split("/");
            yearMonthDay = false;
        } else {
            throw new RuntimeException(
                    "The date is in an incorrect format, acceptable format is YYYY-MM-DD, YYYY/MM/DD, DD-MM-YYYY or DD/MM/YYYY");
        }

        if (dateSplit.length != 3) {
            throw new RuntimeException(
                    "The date is in an incorrect format, acceptable format is YYYY-MM-DD, YYYY/MM/DD, DD-MM-YYYY or DD/MM/YYYY");
        }

        try {
            setMonth(Integer.parseInt(dateSplit[1]));
            if (yearMonthDay) {
                setYear(Integer.parseInt(dateSplit[0]));
                setDay(Integer.parseInt(dateSplit[2]));
            } else {
                setYear(Integer.parseInt(dateSplit[2]));
                setDay(Integer.parseInt(dateSplit[0]));
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Check if the date's day, month and year is in the correct range
     */
    private void validateDate() {
        if (day < 1 || day > 31) {
            throw new RuntimeException("Day can only be in the range of 1-31");
        }
        if (month < 1 || month > 12) {
            throw new RuntimeException("Month can only be in the range of 1-12");
        }
        if (year < 1 || year > 9999) {
            throw new RuntimeException("Year can only be in the range of 1-9999");
        }
    }

    /**
     * Represent the date as a single integer of days from year 0
     * 
     * @return The date as an integer
     */
    private int toDays() {
        return day + (month * 31) + (year * 365);
    }

    /**
     * Set the date as today's date
     */
    public void now() {
        this.day = LocalDateTime.now().getDayOfMonth();
        this.month = LocalDateTime.now().getMonthValue();
        this.year = LocalDateTime.now().getYear();
    }

    @Override
    public String toString() {
        return dayStringifier(day) + "/" + monthStringifier(month) + "/" + year;
    }

    /**
     * Convert the DateSimple object to an SQL friendly string
     * @return The SQL friendly representation of the date
     */
    public String toStringDb() {
        return year + "-" + monthStringifier(month) + "-" + dayStringifier(day);
    }

    /**
     * Convert the day to a DD format instead of D, if the day is less than 10
     * @param day The day integer to be converted
     * @return The converted string
     */
    private String dayStringifier(int day) {
        return day < 10 ? "0" + day : "" + day;
    }

    /**
     * Convert the month to a MM format instead of M, if the month is less than 10
     * @param month The month integer to be converted
     * @return The converted string
     */
    private String monthStringifier(int month) {
        return month < 10 ? "0" + month : "" + month;
    }

    @Override
    public int compareTo(SimpleDate simpleDate) {
        return toDays() - simpleDate.toDays();
    }
}
