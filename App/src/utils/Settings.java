package utils;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class Settings {

    HashMap<String, String> hmString = new HashMap<>();
    HashMap<String, Integer> hmInteger = new HashMap<>();
    HashMap<String, String[]> hmStringArray = new HashMap<>();

    NodeList xmlNodeList;
    Document doc;

    String fileName;

    public Settings(String fileName) {
        this.fileName = fileName;
        loadFile();
        parseData("string");
        parseData("integer");
        parseData("stringArray");
    }

    public String getString(String key) {
        return hmString.get(key);
    }

    public int getInt(String key) {
        return hmInteger.get(key);
    }
    
    public String [] getStringArray(String key) {
        return hmStringArray.get(key);
    }

    private void loadFile() {

        try {
            File inputFile = new File(this.fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            this.doc = dBuilder.parse(inputFile);

            this.doc.getDocumentElement().normalize();

        } catch (Exception e) {

            System.out.println("Settings file " + this.fileName + ", loading error!!!");
            System.out.println(e.getMessage());
        }
    }

    private void parseData(String tagName) {

        this.xmlNodeList = doc.getElementsByTagName(tagName);

        for (int i = 0; i < xmlNodeList.getLength(); i++) {

            Node node = xmlNodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                String name = element.getAttribute("name");
                String content = element.getTextContent();

                if (tagName.equals("string")) {
                    hmString.put(name, content);

                } else if (tagName.equals("integer")) {
                    hmInteger.put(name, new Integer(content));

                } else if (tagName.equals("stringArray")) {
                    hmStringArray.put(name, content.split(","));
                }
            }
        }
    }
}
