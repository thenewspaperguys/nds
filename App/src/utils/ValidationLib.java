package utils;

import dataStructures.*;

/**
 * ValidationLib allows to quickly validate various objects passed within the
 * application, eg. Customer, Order, Publication, Driver...
 */
public class ValidationLib {

    Settings errorMsg = new Settings("./ErrorMessages.xml");

    /**
     * Check if the Customer object is valid
     *
     * @param customer
     *            Object to be validated
     * @return ValidationResult class that indicates if the the passed in object is
     *         valid and what kind of errors it contains (if any)
     */
    public ValidationResult validateCustomer(Customer customer) {
        ValidationResult validationResult = new ValidationResult();

        // Null object validation
        if (customer == null) {
            validationResult.addError(errorMsg.getString("errCusNull"));
            return validationResult;
        }

        // Name validation
        if (!Validator.isNotEmpty(customer.getName())) {
            validationResult.addError(errorMsg.getString("errCusNameEmpty"));
        } else if (!Validator.isName(customer.getName())) {
            validationResult.addError(errorMsg.getString("errCusNameInv"));
        } else if (!Validator.isMaxLength(customer.getName(), 100)) {
            validationResult.addError(errorMsg.getString("errCusNameTooLong"));
        }

        // Phone number validation
        if (!Validator.isNotEmpty(customer.getPhoneNumber())) {
            validationResult.addError(errorMsg.getString("errCusPhoneEmpty"));
        } else if (!Validator.isPhoneNumber(customer.getPhoneNumber())) {
            validationResult.addError(errorMsg.getString("errCusPhoneInv"));
        } else if (!Validator.isMaxLength(customer.getPhoneNumber(), 20)) {
            validationResult.addError(errorMsg.getString("errCusPhoneTooLong"));
        }

        // Email validation
        if (!Validator.isNotEmpty(customer.getEmail())) {
            validationResult.addError(errorMsg.getString("errCusEmailEmpty"));
        } else if (!Validator.isEmail(customer.getEmail())) {
            validationResult.addError(errorMsg.getString("errCusEmailInv"));
        } else if (!Validator.isMaxLength(customer.getEmail(), 40)) {
            validationResult.addError(errorMsg.getString("errCusEmailTooLong"));
        }

        // Address validation
        if (!Validator.isNotEmpty(customer.getAddress())) {
            validationResult.addError(errorMsg.getString("errCusAddrEmpty"));
        } else if (!Validator.isAlpha(customer.getAddress())) {
            validationResult.addError(errorMsg.getString("errCusAddrInv"));
        }

        // Region validation
        if (!Validator.isNotEmpty(customer.getRegion())) {
            validationResult.addError(errorMsg.getString("errCusRegionEmpty"));
        } else if (!Validator.isAlpha(customer.getRegion())) {
            validationResult.addError(errorMsg.getString("errCusRegionInv"));
        }

        return validationResult;
    }

    /**
     * Check if the Order object is valid
     *
     * @param order
     *            Object to be validated
     * @return ValidationResult class that indicates if the the passed in object is
     *         valid and what kind of errors it contains (if any)
     */
    public ValidationResult validateOrder(Order order) {
        ValidationResult validationResult = new ValidationResult();

        // Null object validation
        if (order == null) {
            validationResult.addError(errorMsg.getString("errOrdNull"));
            return validationResult;
        }

        // Customer within the order validation
        if (order.getCustomer() == null) {
            validationResult.addError(errorMsg.getString("errOrdCusEmpty"));
        } else if (order.getCustomer().getID() == 0) {
            validationResult.addError(errorMsg.getString("errOrdCusInv"));
        }

        // Publication within the order validation
        if (order.getPublication() == null) {
            validationResult.addError(errorMsg.getString("errOrdPubNull"));
        } else if (order.getPublication().getID() == 0) {
            validationResult.addError(errorMsg.getString("errOrdPubInv"));
        }

        // Order quantity validation
        if (order.getQty() <= 0) {
            validationResult.addError(errorMsg.getString("errOrdQty"));
        }

        // Order date not specified
        if (order.getNextDelivery() == null) {
            validationResult.addError(errorMsg.getString("errOrdDate"));
        }

        return validationResult;
    }

    /**
     * Check if the Publication object is valid
     *
     * @param publication
     *            Object to be validated
     * @return ValidationResult class that indicates if the the passed in object is
     *         valid and what kind of errors it contains (if any)
     */
    public ValidationResult validatePublication(Publication publication) {
        ValidationResult validationResult = new ValidationResult();

        // Null object validation
        if (publication == null) {
            validationResult.addError(errorMsg.getString("errPubNull"));
            return validationResult;
        }

        // Title validation
        if (!Validator.isNotEmpty(publication.getTitle())) {
            validationResult.addError(errorMsg.getString("errPubTitleEmpty"));
        } else if (!Validator.isAlpha(publication.getTitle())) {
            validationResult.addError(errorMsg.getString("errPubTitleInv"));
        } else if (!Validator.isMaxLength(publication.getTitle(), 200)) {
            validationResult.addError(errorMsg.getString("errPubTitleTooLong"));
        }

        // Price validation
        if (publication.getPrice() < 0) {
            validationResult.addError(errorMsg.getString("errPubPrice"));
        }

        return validationResult;
    }

    /**
     * Check if the Driver object is valid
     *
     * @param driver
     *            Object to be validated
     * @return ValidationResult class that indicates if the the passed in object is
     *         valid and what kind of errors it contains (if any)
     */
    public ValidationResult validateDriver(Driver driver) {
        ValidationResult validationResult = new ValidationResult();

        // Null object validation
        if (driver == null) {
            validationResult.addError(errorMsg.getString("errDrivNull"));
            return validationResult;
        }

        // Name validation
        if (!Validator.isNotEmpty(driver.getName())) {
            validationResult.addError(errorMsg.getString("errDrivNameEmpty"));
        } else if (!Validator.isName(driver.getName())) {
            validationResult.addError(errorMsg.getString("errDrivNameInv"));
        } else if (!Validator.isMaxLength(driver.getName(), 100)) {
            validationResult.addError(errorMsg.getString("errDrivNameTooLong"));
        }

        // Phone number validation
        if (!Validator.isNotEmpty(driver.getPhoneNumber())) {
            validationResult.addError(errorMsg.getString("errDrivPhoneEmpty"));
        } else if (!Validator.isPhoneNumber(driver.getPhoneNumber())) {
            validationResult.addError(errorMsg.getString("errDrivPhoneInv"));
        } else if (!Validator.isMaxLength(driver.getPhoneNumber(), 20)) {
            validationResult.addError(errorMsg.getString("errDrivPhoneTooLong"));
        }

        return validationResult;
    }

    /**
     * Check if the Region object is valid
     *
     * @param region
     *            Object to be validated
     * @return ValidationResult class that indicates if the the passed in object is
     *         valid and what kind of errors it contains (if any)
     */
    public ValidationResult validateRegion(Region region) {
        ValidationResult validationResult = new ValidationResult();

        // null object validation
        if (region == null) {
            validationResult.addError(errorMsg.getString("errRegionNull"));
            return validationResult;
        }

        // Name validation
        if (!Validator.isNotEmpty(region.getName())) {
            validationResult.addError(errorMsg.getString("errRegionNameEmpty"));
        } else if (!Validator.isAlpha(region.getName())) {
            validationResult.addError(errorMsg.getString("errRegionNameInv"));
        } else if (!Validator.isMaxLength(region.getName(), 30)) {
            validationResult.addError(errorMsg.getString("errRegionNameTooLong"));
        }

        return validationResult;
    }
}
