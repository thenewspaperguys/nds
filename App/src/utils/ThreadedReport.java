package utils;

import dataStructures.Delivery;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import mvc.view.ViewPrintService;

import java.util.ArrayList;

public class ThreadedReport extends Thread {

    NDSModel model;
    NDSView view;
    NDSController controller;
    Settings settings;
    int year;

    public ThreadedReport(NDSModel model, NDSView view, NDSController controller, Settings settings, int year, String threadName) {
        super(threadName);
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.year = year;
    }

    public void run() {

        // look for deliveries for the specified date
        ArrayList<Delivery> allDeliveries = model.summary.getDeliveriesForReport(this.year);

        if (allDeliveries == null) {
            // db error
            view.status.set(settings.getString("dbError"), 5000);

        } else if (allDeliveries.size() == 0) {
            // no results
            String msg = settings.getString("noReport") + year;
            view.dialog.okDialog(msg);

        } else {
            // print the summary on the screen
            PrintingService printingService = new PrintingService(allDeliveries, this.year, settings);
            String sumTitle = settings.getString("lReport") + " " + year;
            ViewPrintService framePrintService = new ViewPrintService(printingService.toString(), sumTitle);
        }
    }
}
