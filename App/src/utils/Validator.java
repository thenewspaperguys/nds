package utils;

public class Validator {

    /**
     * Check if the string passed in represents a valid email address
     *
     * @param value
     *            Email address to be checked
     * @return true or false (valid/invalid) email address
     */
    public static boolean isEmail(String value) {
        if (value == null) return false;

        return value.toLowerCase().matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");
    }

    /**
     * Check if the string passed in represents a valid name value. A valid name
     * allows lower and uppercase letters, single quotes, spaces and hyphens
     *
     * @param value
     *            Name to be checked
     * @return true/false (valid/invalid) name
     */
    public static boolean isName(String value) {
        if (value == null) return false;

        return value.toUpperCase().matches("^[A-Z][A-Z' -]+");
    }

    /**
     * Check if the string passed in represent an alphanumerical string
     * @param value
     *            String to be checked
     * @return true/false (valid/invalid) string
     */
    public static boolean isAlpha(String value) {
        if (value == null) return false;

        return value.toUpperCase().matches("[A-Z0-9',.;\\- \\r\\n|\\n|\\r]+");
    }

    /**
     * Check if the string passed in is not empty
     *
     * @param value
     *            The string to be checked
     * @return False if string is empty, true if non-empty
     */
    public static boolean isNotEmpty(String value) {
        if (value == null) return false;

        if (value.length() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Check if a string is at least the required length
     *
     * @param value
     *            The string to be checked
     * @param n
     *            The maximum amount of characters the string needs to be
     * @return False if the string is longer than n, true if it is less or equal to
     *         n characters
     */
    public static boolean isMaxLength(String value, int n) {
        if (value == null) return false;

        if (value.length() <= n) {
            return true;
        }
        return false;
    }

    /**
     * Check if the passed string represents a valid phone number
     *
     * @param value
     *            The string to be checked
     * @return False if it is not a valid phone number, true if it is
     */
    public static boolean isPhoneNumber(String value) {
        if (value == null) return false;

        return value.matches("^[0+][0-9 ]{7,19}");
    }

    /**
     * Check if the passed string represents an integer value
     *
     * @param value
     *            The string to be checked
     * @return False if it is not a valid integer, true if it is
     */
    public static boolean isInteger(String value) {
        if (value == null) return false;

        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if the passed string represents a positive integer
     *
     * @param value
     *            The string to be checked
     * @return False if it is not a valid integer, true if it is
     */
    public static boolean isPositiveInteger(String value) {
        if (value == null) return false;

        try {
            int x = Integer.parseInt(value);
            if (x > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if the passed string represents a decimal number
     *
     * @param value
     *            The string to be checked
     * @return False if it is not a valid decimal number, true if it is
     */
    public static boolean isDecimal(String value) {
        if (value == null) return false;

        try {
            Double.parseDouble(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if the passed string represents a positive decimal number
     *
     * @param value
     *            The string to be checked
     * @return False if it is not a valid decimal number, true if it is
     */
    public static boolean isPositiveDecimal(String value) {
        if (value == null) return false;

        try {
            double x = Double.parseDouble(value);
            if (x > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
