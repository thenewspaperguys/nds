package utils;

import java.util.ArrayList;

public class ValidationResult {

    private ArrayList<String> errors = new ArrayList<String>();

    public ValidationResult() {
        //
    }

    public boolean isValid() {
        if (errors.size() > 0) {
            return false;
        }
        return true;
    }

    public void addError(String error) {
        errors.add(error);
    }
    
    public int errors() {
        return errors.size();
    }

    public String toString() {
        String output = "";
        for (String error : errors) {
            output += error + "\n\n";
        }
        return output;
    }
}
