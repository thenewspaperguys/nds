package utils;

import dataStructures.Customer;
import dataStructures.Delivery;
import dataStructures.DeliveryList;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Helpers {

    /**
     * Extract distinct Customers from a pool of deliveries
     *
     * @param deliveries The pool of Deliveries to look through
     * @return Distinct Customers from the Deliveries pool
     */
    public static ArrayList<Integer> getDistinctCustomersFromDeliveries(ArrayList<Delivery> deliveries) {
        ArrayList<Integer> extractedCustomers = new ArrayList<>();

        for (Delivery delivery : deliveries) {
            extractedCustomers.add(delivery.getOrder().getCustomer().getID());
        }

        // remove duplicates
        return new ArrayList<Integer>(new LinkedHashSet<Integer>(extractedCustomers));
    }

    /**
     * Get customer info by ID
     *
     * @param customerID ID of the customer
     * @return The customer object
     */
    public static Customer getCustomerByIDFromDelivery(int customerID, ArrayList<Delivery> deliveries) {
        for (Delivery delivery : deliveries) {
            if (customerID == delivery.getOrder().getCustomer().getID()) {
                return delivery.getOrder().getCustomer();
            }
        }
        return null;
    }

    /**
     * Create an array of integers between 2000 and 2100
     * @return The array of integers
     */
    public static Integer [] createYearsArray() {
        Integer [] year = new Integer[101];
        int idx = 0;

        for (int i = 2000; i < 2101; i++) {
            year[idx] = i;
            idx++;
        }

        return year;
    }

    /**
     * Set the width for a column in a table
     * @param table The table for which the effect will occur
     * @param column The index of the column
     * @param width The max width of the column
     */
    public static void setTableColumnWidth(JTable table, int column, int width) {
        TableColumn tableColumn = table.getColumnModel().getColumn(0);
        tableColumn.setMaxWidth(width);
    }
}
