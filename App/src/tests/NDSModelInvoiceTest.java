package tests;

import java.util.ArrayList;

import mvc.NDSModel;
import utils.Settings;
import utils.SimpleDate;
import dataStructures.Customer;
import dataStructures.Invoice;
import dataStructures.Publication;
import junit.framework.TestCase;

public class NDSModelInvoiceTest extends TestCase {

    Settings settings = new Settings("./AppSettings.xml");
    NDSModel model = new NDSModel(settings);
    boolean init = model.init();

    // Test: 001
    // Objective: Verify that the Invoice has list of publications, invoice id
    // and price correction
    // Input(s): Invoice invoice
    // Output(s): 1

    public void test001() {
        try {
            ArrayList<Publication> pub = new ArrayList<Publication>();
            pub.add(new Publication(1, 50, 20, "The Irish Times", 2.00));
            Invoice invoice = new Invoice(0, new Customer(10, "Bobby Joe", null, null, null, null, null, null), pub,
                    new SimpleDate(), 0.0, false);
                int result = model.invoices.createInvoice(invoice);
                assertEquals(1, result);
        } catch (Exception e) {
            fail("There was an error in the code");
        }
    }

    // Test: 002
    // Objective: Verify doesn't take in invoice ids
    // Input(s): Customer customer
    // Output(s): 0

    public void test002() {
        try {
        	Invoice invoice = new Invoice();
        	invoice.setID(1);
        	assertEquals(0, model.invoices.createInvoice(invoice));
        } catch (Exception e) {
            fail("There was an error in the code");
        }
    }

    // Test: 003
    // Objective: Verify that the Invoice that was pulled from the database is
    // the same as the one inside the database
    // Input(s): invoice id
    // Output(s): Invoice Object
    public void test003() {
        try {

                Invoice resultGetInvoice = model.invoices.getInvoiceByID(1);
                assertEquals(1, resultGetInvoice.getID());

        } catch (Exception e) {
            fail("Error in product code");
        }
    }

    // Test: 004
    // Objective: Verify that getInvoice only takes in ids that exist
    // Input(s): invoice id
    // Output(s): Invoice Object
    public void test004() {
        try {
                Invoice resultGetInvoice = model.invoices.getInvoiceByID(-1);
                assertEquals(null, resultGetInvoice);

        } catch (Exception e) {
            fail("Error in product code");
        }
    }

    // Test: 005
    // Objective: Verify that the method can search by names
    // Input(s): String
    // Output(s): ArrayList of Invoices
    public void test005() {
        try {
            ArrayList<Invoice> inv = new ArrayList<Invoice>();
                inv = model.invoices.getAllInvoices("Jenna Burke");
                for (Invoice invoice : inv) {
                    assertEquals("Jenna Burke", invoice.getCustomer().getName());
            }

        } catch (Exception e) {
            fail("Error inside of product code");
        }
    }

    // Test: 006
    // Objective: Verify that the method can will not search if the name is wrong
    // Input(s): String
    // Output(s): ArrayList of Invoices
    public void test006() {
        try {
            ArrayList<Invoice> inv = new ArrayList<Invoice>();
                inv = model.invoices.getAllInvoices("Billy Bob");
                for (Invoice invoice : inv) {
                    assertEquals(null, invoice);
                }
        } catch (Exception e) {
            fail("Error inside of product code");
        }
    }

    // Test: 007
    // Objective: Verify that getAllInvoices can search by names that exist
    // Input(s): invoice id
    // Output(s): Invoice Object
    public void test007() {
        try {
            ArrayList<Invoice> inv = new ArrayList<Invoice>();
            inv = model.invoices.getAllInvoices("asadfads asdfdafas");
            assertEquals(0, inv.size());
        } catch (Exception e) {
            fail("Error in product code");
        }
    }

    // Test: 008
    // Objective: Verify that the method calls the createInvoice method once a month
    // Input(s):
    // Output(s): 1

    public void test008() {
        try {
            int resultCreateAllInvoice = model.invoices.createAllInvoices();
            assertEquals(1, resultCreateAllInvoice);
        } catch (Exception e) {
        	fail("Error inside of code");
        }
    }

    // Test: 009
    // Objective: Verify that the method returns a 1 when a month hasn't passed
    // Input(s):
    // Output(s): 1

    public void test009() {
        try {
            int resultCreateAllInvoice = model.invoices.createAllInvoices();
            assertEquals(0, resultCreateAllInvoice);
        } catch (Exception e) {
            fail("Error inside of code");
        }
    }

    // Test: 010
    // Objective: Verify that non billed deliveries are billed
    // Input(s): 4
    // Output(s): 1

    public void test010() {
        try {
            int resultCreateAllInvoice = model.invoices.updateIsBilled(4);
            assertEquals(1, resultCreateAllInvoice);
        } catch (Exception e) {
            fail("Error inside of code");
        }
    }

    // Test: 011
    // Objective: Verify that billed deliveries are not changed
    // Input(s): 4
    // Output(s): 0

    public void test011() {
        try {
            int resultCreateAllInvoice = model.invoices.updateIsBilled(4);
            assertEquals(0, resultCreateAllInvoice);
        } catch (Exception e) {
            fail("Error inside of code");
        }
    }
}
