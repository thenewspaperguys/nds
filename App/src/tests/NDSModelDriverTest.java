package tests;

import mvc.NDSModel;
import utils.Settings;
import dataStructures.Customer;
import dataStructures.Driver;
import dataStructures.Order;
import dataStructures.Publication;
import junit.framework.TestCase;

public class NDSModelDriverTest extends TestCase {
	Settings settings = new Settings("./AppSettings.xml");
	NDSModel testObj = new NDSModel(settings);
	boolean init = testObj.init();
	
	
	//test:001
	//test Object:test if the method insert a driver
	//Input(s):new driver
	//output:1
	public void testNDSModel001()
	{
		try
		{
			Driver driver = new Driver();
			driver.setID(1);
			driver.setName("Joe Mullins");
			driver.setPhoneNumber(null);
			
		}
		catch(Exception e)
		{
			fail("You should input the right code.");
		}
	}
}


