package tests;

import mvc.NDSModel;
import utils.Settings;

import dataStructures.Customer;
import dataStructures.Order;
import dataStructures.Publication;
import junit.framework.TestCase;


public class NDSModelOrderTest extends TestCase {

	Settings settings = new Settings("./AppSettings.xml");
	NDSModel testObj = new NDSModel(settings);
	boolean init = testObj.init();

	//////////////////////////////////////////////////////////////////////
	/////////Test insertOrder
	///////////////////////////////////////////////////////////////////

	//equivalence partitioning
	//test:009
	//test Object:test if the method insert an order
	//Input(s):new order
	//output:1

	public void testNDSModel009()
	{
		try
		{
			Order order = new Order();
			Customer customer = new Customer();
			Publication publication = new Publication();
			customer.setName("Joe Mullins");
			customer.setAbsent(null);
			customer.setAddress(null);
			customer.setEmail(null);
			customer.setID(1);
			customer.setPhoneNumber(null);
			customer.setRegion(null);
			customer.setStatus(null);

			publication.setID(1);
			publication.setPrice(1.0);
			publication.setQty(1);
			publication.setStock(1);
			publication.setTitle(null);

			order.setCustomer(customer);
			order.setPublication(publication);
			order.setFrequency(null);
			order.setID(1);
			order.setNextDelivery(null);
			order.setStatus(null);
			order.setQty(1);
		}
		catch(Exception e)
		{
			fail("You should input the right code.");
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	//////// Test modifyOrder
	/////////////////////////////////////////////////////////////////////////////////////


	//boundary value analysis
	//test:012
	//test Object:test if the method take correct input(qty<1)
	//Input(s):qty -1
	//output:-1

	public void testNDSModel012()
	{

		try
		{
			assertEquals(-1, testObj.order.modifyOrder(1, -1,"Day", "Active"));
		}
		catch(Exception e)
		{
			assertEquals("Failed to connect", e.getMessage());
		}
	}

	//boundary value analysis
	//test:013
	//test Object:test if the method take correct input(qty=1)
	//Input(s):qty 1
	//output:1

	public void testNDSModel013()
	{

		try
		{
			assertEquals(1, testObj.order.modifyOrder(1, 1, "Day", "Active"));
		}
		catch(Exception e)
		{
			fail("You should input the right code.");
		}
	}

	//boundary value analysis
	//test:014
	//test Object:test if the method take correct input(qty>1)
	//Input(s):qty 2
	//output:1

	public void testNDSModel014()
	{

		try
		{
			assertEquals(1, testObj.order.modifyOrder(1, 2, "Day", "Active"));
		}
		catch(Exception e)
		{
			fail("You should input the right code.");
		}
	}

	//equivalence partitioning
	//test:015
	//test Object:test if the method take correct input(frequency = abc)
	//Input(s):frequency 1
	//output:-1


	public void testNDSModel015()
	{

		try
		{
			assertEquals(-1, testObj.order.modifyOrder(1, 2, "abc", "Active"));
		}
		catch(Exception e)
		{
			assertEquals("Failed to connect", e.getMessage());
		}
	}

	//equivalence partitioning
	//test:016
	//test Object:test if the method take correct input(frequency=Day)
	//Input(s):frequency Day
	//output:1

	public void testNDSModel016()
	{

		try
		{
			assertEquals(1, testObj.order.modifyOrder(1, 2, "Day", "Active"));
		}
		catch(Exception e)
		{
			fail("You should input the right code.");
		}
	}

	//equivalence partitioning
	//test:017
	//test Object:test if the method take correct input(status = abc)
	//Input(s):status 1
	//output:-1

	public void testNDSModel017()
	{

		try
		{
			assertEquals(-1, testObj.order.modifyOrder(1, 2, "Day", "abc"));
		}
		catch(Exception e)
		{
			assertEquals("Failed to connect", e.getMessage());
		}
	}

	//equivalence partitioning
	//test:018
	//test Object:test if the method take correct input(status=Active)
	//Input(s):status Active
	//output:1

	public void testNDSModel018()
	{

		try
		{
			assertEquals(1, testObj.order.modifyOrder(1, 2, "Day", "Active"));
		}
		catch(Exception e)
		{
			fail("You should input the right code.");
		}
	}


}
