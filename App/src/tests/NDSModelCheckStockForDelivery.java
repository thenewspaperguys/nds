package tests;

import java.util.ArrayList;

import dataStructures.Delivery;
import dataStructures.DeliveryList;
import dataStructures.Order;
import dataStructures.Publication;
import junit.framework.TestCase;
import mvc.NDSModel;
import utils.Settings;

public class NDSModelCheckStockForDelivery extends TestCase {

    Settings settings = new Settings("./AppSettings.xml");
    NDSModel model = new NDSModel(settings);
    boolean init = model.init();

    ////////////////////////////////////////////////////////////
    // Verify adding a delivery list into an accumulated list of added publications
    // Employing EP
    // One valid partition:
    // 1.Many publications of the same type get added together
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Verify that the same type of publications get added together
    // Input: addDeliveryPublications; "Times" qty 10, "Times" qty 15
    // Expected Output: 25
    public void testMergePublications001() {
        DeliveryList delList = new DeliveryList();
        Publication pub1 = new Publication(0, 0, 0, "Times", 0);
        Publication pub2 = new Publication(0, 0, 0, "Times", 0);
        
        Order order1 = new Order();
        order1.setPublication(pub1);
        order1.setQty(10);
        Order order2 = new Order();
        order2.setPublication(pub2);
        order2.setQty(15);

        ArrayList<Delivery> deliveries = new ArrayList<>();
        deliveries.add(new Delivery(0, order1, false, false, null));
        deliveries.add(new Delivery(0, order2, false, false, null));

        delList.setDeliveries(deliveries);

        ArrayList<Publication> testObj = model.deliveries.addDeliveryPublicationsQuantities(delList);
        
        assertEquals(25, testObj.get(0).getQty());
    }

    // Test number: 002
    // Test objective: Verify that the different types of publications get added together
    // Input: addDeliveryPublications; "Times" qty 10, "MagPI" qty 5, "Times" qty 15, "MagPI" 6, "Athlone" 8
    // Expected Output: 25
    public void testMergePublications002() {
        DeliveryList delList = new DeliveryList();
        Publication pub1 = new Publication(0, 0, 0, "Times", 0);
        Publication pub2 = new Publication(0, 0, 0, "MagPI", 0);
        Publication pub3 = new Publication(0, 0, 0, "Times", 0);
        Publication pub4 = new Publication(0, 0, 0, "MagPI", 0);
        Publication pub5 = new Publication(0, 0, 0, "Athlone", 0);
        
        Order order1 = new Order();
        order1.setPublication(pub1);
        order1.setQty(10);

        Order order2 = new Order();
        order2.setPublication(pub2);
        order2.setQty(5);

        Order order3 = new Order();
        order3.setPublication(pub3);
        order3.setQty(15);

        Order order4 = new Order();
        order4.setPublication(pub4);
        order4.setQty(6);

        Order order5 = new Order();
        order5.setPublication(pub5);
        order5.setQty(8);

        ArrayList<Delivery> deliveries = new ArrayList<>();
        deliveries.add(new Delivery(0, order1, false, false, null));
        deliveries.add(new Delivery(0, order2, false, false, null));
        deliveries.add(new Delivery(0, order3, false, false, null));
        deliveries.add(new Delivery(0, order4, false, false, null));
        deliveries.add(new Delivery(0, order5, false, false, null));

        delList.setDeliveries(deliveries);

        ArrayList<Publication> testObj = model.deliveries.addDeliveryPublicationsQuantities(delList);
        
        assertEquals(25, testObj.get(0).getQty()); // test Times
        assertEquals(11, testObj.get(1).getQty()); // test MagPI
        assertEquals(8, testObj.get(2).getQty()); // test Athlone
    }
}
