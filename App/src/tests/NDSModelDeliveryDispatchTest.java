package tests;

import java.util.ArrayList;

import dataStructures.Delivery;
import dataStructures.DeliveryList;
import dataStructures.Driver;
import dataStructures.Order;
import junit.framework.TestCase;
import mvc.NDSModel;
import utils.Settings;
import utils.SimpleDate;

public class NDSModelDeliveryDispatchTest extends TestCase {

    Settings settings = new Settings("./AppSettings.xml");
    NDSModel model = new NDSModel(settings);
    boolean init = model.init();

    ////////////////////////////////////////////////////////////
    // Testing orders for dispatch retrieval
    // Employing EP
    // One valid partition:
    // 1.Array list with Order objects
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Retrieve Orders from the database
    // Input: model.getOrdersForDispatch
    // Expected Output: 12 orders
    public void testGetOrdersForDispatch001() {
        ArrayList<Order> orders = model.deliveries.getOrdersForDispatch();
        assertEquals(12, orders.size());
    }

    ////////////////////////////////////////////////////////////
    // Testing dispatching an order for delivery
    // Employing EP
    // One valid partition:
    // 1.Order dispatched, returns 1
    // One invalid partition:
    // 1.Null object passed
    // 2.DeliveryList object has no deliveries
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Dispatching a delivery list
    // Input: model.dispatchDelivery(deliveries)
    // Expected Output: 2
    public void testDispatchDelivery001() {
        Order order = new Order(1, null, null, 0, "week", "Active", new SimpleDate());
        Delivery delivery = new Delivery(0, order, false, false, null);
        Driver driver = new Driver(1, "Joe", "085 5555555");

        ArrayList<Delivery> deliveries = new ArrayList<>();
        deliveries.add(delivery);
        deliveries.add(delivery);

        DeliveryList delList = new DeliveryList(driver, deliveries);

        int result = model.deliveries.dispatchDeliveries(delList);

        assertEquals(2, result);

    }

    // Test number: 002
    // Test objective: Dispatching a delivery list as null
    // Input: model.dispatchDelivery(null);
    // Expected Output: 0
    public void testDispatchDelivery002() {
        int result = model.deliveries.dispatchDeliveries(null);
        assertEquals(0, result);
    }

    // Test number: 003
    // Test objective: Dispatching a delivery list with no deliveries assigned
    // Input: model.dispatchDelivery(deliveries);
    // Expected Output: 0
    public void testDispatchDelivery003() {
        Order order = new Order(1, null, null, 0, "week", "Active", new SimpleDate());
        Driver driver = new Driver(0, "Joe", "085 5555555");

        ArrayList<Delivery> deliveries = new ArrayList<>();

        DeliveryList delList = new DeliveryList(driver, deliveries);

        int result = model.deliveries.dispatchDeliveries(delList);

        assertEquals(0, result);
    }

    ////////////////////////////////////////////////////////////
    // Testing marking a delivery as delivered
    // Employing EP
    // One valid partition:
    // 1.Delivery marked successfully
    // One invalid partition:
    // 1.Delivery passed in is null
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Marking a delivery as successful
    // Input: Delivery with ID 1
    // Expected Output: 1
    public void testSetDeliveryAsDelivered001() {
        Delivery delivery = new Delivery(1, null, false, false, null);

        int result = model.deliveries.setDeliveryAsDelivered(delivery);

        assertEquals(1, result);
    }

    // Test number: 002
    // Test objective: Passing in a null delivery
    // Input: nul
    // Expected Output: 0
    public void testSetDeliveryAsDelivered002() {
        int result = model.deliveries.setDeliveryAsDelivered(null);
        assertEquals(0, result);
    }
}
