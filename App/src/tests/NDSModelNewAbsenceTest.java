package tests;

import junit.framework.TestCase;
import mvc.NDSModel;
import utils.Settings;

public class NDSModelNewAbsenceTest extends TestCase {

    Settings settings = new Settings("./AppSettings.xml");
    NDSModel model = new NDSModel(settings);
    boolean init = model.init();


    ////////////////////////////////////////////////////////////
    // Testing New absence for valid and invalid input
    // Employing BVA
    // One valid partition:
    // 1. Date is outside of existing absence
    // One invalid partition:
    // 1. Data is inside the existing absence or is on the existing absence
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Test valid input, outside of an existing absence
    // Input:  (4, '2018-01-28', '2018-01-29')
    // Expected Output: false
    public void testDateOutsideOfExistingAbsence001() {
        String from = "2018-01-28";
        String to = "2018-01-29";
        int cusID = 4;

        assertEquals(false, model.absent.isAbsenceInCollision(cusID, from, to));
    }

    // Test number: 002
    // Test objective: Test invalid input, on the border of the existing date
    // Input:  (4, '2018-01-30', '2018-05-23')
    // Expected Output: true
    public void testDateOnTheBorderOfExistingAbsence001() {
        String from = "2018-01-30";
        String to = "2018-05-23";
        int cusID = 4;

        assertEquals(true, model.absent.isAbsenceInCollision(cusID, from, to));
    }

    // Test number: 003
    // Test objective: Test invalid input, inside the border of the existing date
    // Input:  (4, '2018-01-31', '2018-05-22')
    // Expected Output: true
    public void testDateOnTheBorderOfExistingAbsence002() {
        String from = "2018-01-31";
        String to = "2018-05-22";
        int cusID = 4;

        assertEquals(true, model.absent.isAbsenceInCollision(cusID, from, to));
    }
}
