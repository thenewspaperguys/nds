package tests;

import dataStructures.Publication;
import junit.framework.TestCase;
import mvc.NDSModel;
import utils.Settings;

public class NDSModelPublicationTest extends TestCase {

    Settings settings = new Settings("./AppSettings.xml");
    NDSModel model = new NDSModel(settings);
    boolean init = model.init();

    // Test: 001
    // Objective: Verify that the data is inserted into the database.
    // Input(s): Null, 20, "Boris The Sashlik King", 20.
    // Output(s): 1

    public void InsertionTest001() {
        try {
            Publication publication = new Publication();
            publication.setPrice(20);
            publication.setStock(20);
            publication.setTitle("Boris the Sashlik King");
            int result = model.publication.insertPublication(publication);
            assertEquals(1, result);

        } catch (Exception e) {
            fail("There was an error in the code");
        }
    }

    /////////////////////////////////////////////////
    // Testing Modify Publications method
    // Using Equivalence Partition
    // Valid price and name
    // Test number: 002
    // Test objective: Passing valid input
    // Input: 1, 20, "Times"
    // Expected Output: 1

    public void testModifyPublication001() {
        int result = model.publication.modifyPublication(1, 20, "Times");
        assertEquals(1, result);
    }

    // Valid price and name
    // Test number: 003
    // Test objective: Passing invalid int & valid string.
    // Input: 1, -5, "Times"
    // Expected Output: 1
    public void testModifyPublication002() {
        int result = model.publication.modifyPublication(1, -5, " Times");
        assertEquals(0, result);
    }

    // Valid price and name
    // Test number: 004
    // Test objective: Passing invalid title & valid price
    // Input: 1, 55, "%#$^@"
    // Expected Output: 1
    public void testModifyPublication003() {
        int result = model.publication.modifyPublication(1, 55, " %#$^@");
        assertEquals(0, result);
    }

    // Valid price and name
    // Test number: 005
    // Test objective: Passing invalid price and title
    // Input: 1, -120, "%#$^@"
    // Expected Output: 0
    public void testModifyPublication004() {
        int result = model.publication.modifyPublication(1, -120, " %#$^@");
        assertEquals(0, result);
    }

    /////////////////////////////////////////////////
    // Testing Modify Publications method
    // Using Boundary Value Analysis
    // Invalid Price
    // Test number: 006
    // Test objective: Passing invalid price
    // Input: 1, -1, "Times"
    // Expected Output: 0

    public void testModifyPublication005() {
        int result = model.publication.modifyPublication(1, -1, "Times");
        assertEquals(0, result);
    }
    
    // Test number: 007
    // Test objective: Passing invalid price and title
    // Input: 1, 0, "Times"
    // Expected Output: 0
    public void testModifyPublication006() {
        int result = model.publication.modifyPublication(1, 0, "Times");
        assertEquals(0, result);
    }
    
    // Test number: 008
    // Test objective: Passing valid price.
    // Input: 1, 1, "Times"
    // Expected Output: 1
    public void testModifyPublication007() {
        int result = model.publication.modifyPublication(1, 1, "Times");
        assertEquals(1, result);
    }
    
}
