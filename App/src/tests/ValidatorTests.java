package tests;

import junit.framework.TestCase;
import utils.Validator;

public class ValidatorTests extends TestCase {

    ////////////////////////////////////////////////////////////
    // Email input test
    // Employing EP
    // One valid partition:
    // 1.String with a valid email
    // Two invalid partitions
    // 1.String with an invalid email (no @ symbol)
    // 2.Empty string
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Test valid input for Email
    // Input: "j.doe@gmail.com"
    // Expected Output: true
    public void testEmail001() {
        assertEquals(true, Validator.isEmail("j.doe@gmail.com"));
    }

    // Test number: 002
    // Test objective: Test invalid string input for email
    // Input: "j.doe"
    // Expected Output: false
    public void testEmail002() {
        assertEquals(false, Validator.isEmail("j.doe"));
    }

    // Test number: 003
    // Test objective: Test empty input
    // Input: ""
    // Expected Output: false
    public void testEmail003() {
        assertEquals(false, Validator.isEmail(""));
    }

    ////////////////////////////////////////////////////////////
    // Is alphanumerical test
    // Employing EP
    // Two valid partition:
    // 1.String containing letters
    // 2.String containing digits
    // Two invalid partition
    // 1.Empty string
    // 2.String containing illegal characters like !$%&*
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: String with letter
    // Input: "Hello"
    // Expected Output: true
    public void testIsAlphanumerical001() {
        assertEquals(true, Validator.isAlpha("Hello"));
    }

    // Test number: 002
    // Test objective: String with digits
    // Input: "0123456789"
    // Expected Output: true
    public void testIsAlphanumerical002() {
        assertEquals(true, Validator.isAlpha("0123456789"));
    }

    // Test number: 003
    // Test objective: Empty string
    // Input: ""
    // Expected Output: false
    public void testIsAlphanumerical003() {
        assertEquals(false, Validator.isAlpha(""));
    }

    // Test number: 004
    // Test objective: Illegal characters test
    // Input: "!$%&*"
    // Expected Output: false
    public void testIsAlphanumerical005() {
        assertEquals(false, Validator.isAlpha("!"));
        assertEquals(false, Validator.isAlpha("$"));
        assertEquals(false, Validator.isAlpha("%"));
        assertEquals(false, Validator.isAlpha("&"));
        assertEquals(false, Validator.isAlpha("*"));
    }

    ////////////////////////////////////////////////////////////
    // Is a string not empty test
    // Employing EP
    // One valid partition:
    // 1.String with at least one character
    // One invalid partitions
    // 1.Empty string
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: One character string
    // Input: "a"
    // Expected Output: true
    public void testIsNotEmpty001() {
        assertEquals(true, Validator.isNotEmpty("a"));
    }

    // Test number: 002
    // Test objective: Empty string
    // Input: ""
    // Expected Output: false
    public void testIsNotEmpty002() {
        assertEquals(false, Validator.isNotEmpty(""));
    }

    ////////////////////////////////////////////////////////////
    // Maximum length test
    // Employing EP
    // Two valid partition:
    // 1.String with a length greater than zero, required length greater than zero
    // 2.Empty string, and a required length of zero
    // Two invalid partitions:
    // 1.Non-empty string, and a negative required length
    // 2.String longer than the required length
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Non-empty string and a positive required length
    // Input: "Hello", 5
    // Expected Output: true
    public void testRequiredLength001() {
        assertEquals(true, Validator.isMaxLength("Hello", 5));
    }

    // Test number: 002
    // Test objective: Empty string and a required length of zero
    // Input: "", 0
    // Expected Output: true
    public void testRequiredLength002() {
        assertEquals(true, Validator.isMaxLength("", 0));
    }

    // Test number: 003
    // Test objective: Non-empty string and negative required length
    // Input: "Hello", -1
    // Expected Output: false
    public void testRequiredLength003() {
        assertEquals(false, Validator.isMaxLength("Hello", -1));
    }

    // Test number: 004
    // Test objective: String longer than the required length
    // Input: "Hello", 4
    // Expected Output: false
    public void testRequiredLength004() {
        assertEquals(false, Validator.isMaxLength("Hello", 4));
    }

    ////////////////////////////////////////////////////////////
    // Name input test
    // Employing EP
    // One valid partition:
    // 1.String with a valid name, including the apostrophe
    // Two invalid partitions:
    // 1.String with an invalid name, starts with apostrophe or
    // any other invalid non-alpha characters
    // 2.Empty string
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Test valid name input
    // Input: "John Doe", "Mary O'Dwyer", "Mary"
    // Expected Output: true
    public void testName001() {
        assertEquals(true, Validator.isName("John Doe"));
        assertEquals(true, Validator.isName("Mary O'Dwyer"));
        assertEquals(true, Validator.isName("Mary"));
    }

    // Test number: 002
    // Test objective: Test invalid name input
    // Input: Test a name that starts with a non letter and characters like $,%,*
    // Expected Output: false
    public void testName002() {
        assertEquals(false, Validator.isName("'John Doe"));
        assertEquals(false, Validator.isName("$"));
        assertEquals(false, Validator.isName("*"));
        assertEquals(false, Validator.isName("%*"));
    }

    // Test number: 003
    // Test objective: Test empty string
    // Input: ""
    // Expected Output: false
    public void testName003() {
        assertEquals(false, Validator.isName(""));
    }

    ////////////////////////////////////////////////////////////
    // Phone number input test
    // Employing EP
    // Two valid partitions:
    // 1.String with a valid phone number, just numbers and spaces
    // 2.String with a valid phone number, with a + symbol and spaces
    // Three invalid partitions:
    // 1.String with less than 8 characters, implying empty is also invalid
    // 2.String with more than 20 characters
    // 3.String with non-number, non-space characters
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Test valid a phone number input
    // Input: "00353 85 555555"
    // Expected Output: true
    public void testPhone001() {
        assertEquals(true, Validator.isPhoneNumber("00353 85 555555"));
    }

    // Test number: 002
    // Test objective: Test a valid phone number with a + symbol
    // Input: "+353 85 555555"
    // Expected Output: true
    public void testPhone002() {
        assertEquals(true, Validator.isPhoneNumber("+353 85 555555"));
    }

    // Test number: 003
    // Test objective: Test with less than 8 characters
    // Input: "1234567"
    // Expected Output: false
    public void testPhone003() {
        assertEquals(false, Validator.isPhoneNumber("1234567"));
    }

    // Test number: 004
    // Test objective: Test with more than 20 characters
    // Input: "123456789012345678901"
    // Expected Output: false
    public void testPhone004() {
        assertEquals(false, Validator.isPhoneNumber("123456789012345678901"));
    }

    // Test number: 005
    // Test objective: Test letters as input
    // Input: "john doe +-JOHN DOE"
    // Expected Output: false
    public void testPhone005() {
        assertEquals(false, Validator.isPhoneNumber("john doe +-JOHN DOE"));
    }

    ////////////////////////////////////////////////////////////
    // Integer input test
    // Employing EP
    // One valid partition:
    // 1.String with a valid integer value in it, be it positive, negative or zero
    // Three invalid partitions:
    // 2.String with an decimal value in it
    // 3.String with a non-digits in it
    // 4.Empty string
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Positive, negative numbers and zero
    // Input: -1, 0, 1
    // Expected Output: true
    public void testNumber001() {
        assertEquals(true, Validator.isInteger("-1"));
        assertEquals(true, Validator.isInteger("0"));
        assertEquals(true, Validator.isInteger("1"));
    }

    // Test number: 002
    // Test objective: Decimal value
    // Input: "1.5"
    // Expected Output: false
    public void testNumber002() {
        assertEquals(false, Validator.isInteger("1.5"));
    }

    // Test number: 003
    // Test objective: Non digit input
    // Input: "a", "+", ";"
    // Expected Output: false
    public void testNumber003() {
        assertEquals(false, Validator.isInteger("a"));
        assertEquals(false, Validator.isInteger("+"));
        assertEquals(false, Validator.isInteger(";"));
    }

    // Test number: 004
    // Test objective: Empty string
    // Input: ""
    // Expected Output: false
    public void testNumber004() {
        assertEquals(false, Validator.isInteger(""));
    }

    ////////////////////////////////////////////////////////////
    // Positive integer input test
    // Employing EP
    // One valid partition:
    // 1.String with a positive integer value
    // Five invalid partitions:
    // 1.String with a zero in it
    // 2.String with a negative number in it
    // 3.String with an decimal value in it
    // 4.String with a non-digit in it
    // 5.Empty string
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Positive value
    // Input: "1"
    // Expected Output: true
    public void testPositiveInteger001() {
        assertEquals(true, Validator.isPositiveInteger("1"));
    }

    // Test number: 002
    // Test objective: Zero value
    // Input: "0"
    // Expected Output: false
    public void testPositiveInteger002() {
        assertEquals(false, Validator.isPositiveInteger("0"));
    }

    // Test number: 003
    // Test objective: Negative value
    // Input: "-1"
    // Expected Output: false
    public void testPositiveInteger003() {
        assertEquals(false, Validator.isPositiveInteger("-1"));
    }

    // Test number: 004
    // Test objective: Decimal value
    // Input: "1.5"
    // Expected Output: false
    public void testPositiveInteger004() {
        assertEquals(false, Validator.isPositiveInteger("1.5"));
    }

    // Test number: 005
    // Test objective: Non digit input
    // Input: "a", "+", ";"
    // Expected Output: false
    public void testPositiveInteger005() {
        assertEquals(false, Validator.isPositiveInteger("a"));
        assertEquals(false, Validator.isPositiveInteger("+"));
        assertEquals(false, Validator.isPositiveInteger(";"));
    }

    // Test number: 006
    // Test objective: Empty string
    // Input: ""
    // Expected Output: false
    public void testPositiveInteger006() {
        assertEquals(false, Validator.isPositiveInteger(""));
    }

    ////////////////////////////////////////////////////////////
    // Decimal input test
    // Employing EP
    // One valid partition:
    // 1.String with a valid decimal value in it, be it positive, negative or zero
    // Three invalid partitions:
    // 2.String with a valid decimal value but two points
    // 3.String with non digits in it
    // 4.Empty string
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Positive, negative decimal values and a zero
    // Input: "-1.5", "0", "1.5"
    // Expected Output: true
    public void testDecimal001() {
        assertEquals(true, Validator.isDecimal("-1.5"));
        assertEquals(true, Validator.isDecimal("0"));
        assertEquals(true, Validator.isDecimal("1.5"));
    }

    // Test number: 002
    // Test objective: Positive decimal number with two points
    // Input: "1..5", "123.45.5"
    // Expected Output: false
    public void testDecimal002() {
        assertEquals(false, Validator.isDecimal("1..5"));
        assertEquals(false, Validator.isDecimal("123.45.5"));
    }

    // Test number: 003
    // Test objective: Non digit input
    // Input: "a", "+", ";"
    // Expected Output: false
    public void testDecimal003() {
        assertEquals(false, Validator.isDecimal("a"));
        assertEquals(false, Validator.isDecimal("+"));
        assertEquals(false, Validator.isDecimal(";"));
    }

    // Test number: 004
    // Test objective: Empty string
    // Input: ""
    // Expected Output: false
    public void testDecimal004() {
        assertEquals(false, Validator.isDecimal(""));
    }

    ////////////////////////////////////////////////////////////
    // Positive Decimal input test
    // Employing EP
    // One valid partition:
    // 1.String with a positive decimal value
    // Five invalid partitions:
    // 1.String with a zero in it
    // 2.String with a negative decimal value in it
    // 3.String with a positive decimal value but two points
    // 4.String with non digits in it
    // 5.Empty string
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Positive decimal value
    // Input: "1.5"
    // Expected Output: true
    public void testPositiveDecimal001() {
        assertEquals(true, Validator.isPositiveDecimal("1.5"));
    }

    // Test number: 002
    // Test objective: Zero or negative decimal value
    // Input: "0", "-1.5"
    // Expected Output: false
    public void testPositiveDecimal002() {
        assertEquals(false, Validator.isPositiveDecimal("0"));
    }

    // Test number: 003
    // Test objective: Negative decimal value
    // Input: "-1.5"
    // Expected Output: false
    public void testPositiveDecimal003() {
        assertEquals(false, Validator.isPositiveDecimal("-1.5"));
    }

    // Test number: 004
    // Test objective: Positive decimal number with two points
    // Input: "1..5", "123.45.5"
    // Expected Output: false
    public void testPositiveDecimal004() {
        assertEquals(false, Validator.isPositiveDecimal("1..5"));
        assertEquals(false, Validator.isPositiveDecimal("123.45.5"));
    }

    // Test number: 005
    // Test objective: Non digit input
    // Input: "a", "+", ";"
    // Expected Output: false
    public void testPositiveDecimal005() {
        assertEquals(false, Validator.isPositiveDecimal("a"));
        assertEquals(false, Validator.isPositiveDecimal("+"));
        assertEquals(false, Validator.isPositiveDecimal(";"));
    }
}
