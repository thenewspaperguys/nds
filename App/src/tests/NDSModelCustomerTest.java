package tests;

import java.util.ArrayList;

import junit.framework.TestCase;
import mvc.NDSModel;
import utils.Settings;
import dataStructures.Customer;


public class NDSModelCustomerTest extends TestCase {

	Settings settings = new Settings("./AppSettings.xml");
	NDSModel model = new NDSModel(settings);
	boolean init = model.init();

    // Test number: 001
    // Test objective: Passing valid input
    // Input: 1, Kuba Pe�sko, 08612312, email@kuby.pl, Stettin
    // Expected Output: 1
    public void testModifyCustomer() {
        int result = model.customer.modifyCustomer(1,"Kuba Pe�ko","08612312","email@kuby.pl", "Stettin", "Monksland", "Active");
        assertEquals(1, result);
    }

	// Test: 001
	// Objective: Verify customer is added to the database.
	// Input(s): Null, "Neil Wright", "6", "Active", "0862471932", "Willow", "neilwright45@gmail.com".
	// Output(s): 1

	public void testAddCustomer001()
	{
		try
		{
			Customer customer = new Customer();
			customer.setName("Neil Wright");
			customer.setRegion("6");
			customer.setStatus("Active");

			customer.setPhoneNumber("0862471932");
			customer.setAddress("Willow");
			customer.setEmail("neilwright45@gmail.com");
			int result = model.customer.insertCustomer(customer);
			assertEquals(1, result);

		}
		catch (Exception e)
		{
			fail("There was an error in the code");
		}
	}

	// Test: 002
	// Objective: Verify customer is retrieved by name
	// Input(s): SQL Query
	// Output(s): ArrayList of Customers

	public void testGetAllCustomers001() {
        ArrayList<Customer> customers = model.customer.getAllCustomers();
        assertEquals(10, customers.size());
    }
}
