package tests;

import dataStructures.Customer;
import dataStructures.Driver;
import dataStructures.Order;
import dataStructures.Publication;
import dataStructures.Region;
import junit.framework.TestCase;
import utils.SimpleDate;
import utils.ValidationLib;
import utils.ValidationResult;

public class ValidationLibTests extends TestCase {

    ValidationLib validator = new ValidationLib();
    ////////////////////////////////////////////////////////////
    // Customer object validation test
    // Employing EP
    // One valid partition:
    // 1.Object with all valid inputs
    // Two invalid partitions
    // 1.Object with invalid fields
    // 2.Object with empty fields
    //
    // Employing BVA
    // 1. Valid string length on name(99-100), phone number(19-20) and email(39-40)
    // 2. Invalid string length on name(101), phone number(21) and email(41)
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Object with valid fields
    // Input: Customer(null, "John Doe", "085 8558585", "j.doe@gmail.com", "26 Athlone", "Region 12Z", null)
    // Expected Output: true
    public void testValidateCustomer001() {
        Customer testObj = new Customer(0, "John Doe", "085 8558585", "j.doe@gmail.com", "26 Athlone", "Region 12Z", null, null);

        assertEquals(true, validator.validateCustomer(testObj).isValid());
    }

    // Test number: 002
    // Test objective: Object with invalid fields
    // Input: Customer(null, "J0hn Doe!", "085 65", "j.doe!gmail.com", "", "", null)
    // Expected Output: 5 errors
    public void testValidateCustomer002() {
        Customer testObj = new Customer(0, "J0hn Doe!", "085 65", "j.doe!gmail.com", "!$%^&*", "!$%^&*", null, null);

        assertEquals(5, validator.validateCustomer(testObj).errors());
    }

    // Test number: 003
    // Test objective: Object with empty fields
    // Input: Customer()
    // Expected Output: 5 error
    public void testValidateCustomer003() {
        Customer testObj = new Customer();

        assertEquals(5, validator.validateCustomer(testObj).errors());
    }

    // Test number: 004
    // Test objective: Test near border string lengths
    // Input: 99 character name, 19 character phone number, 39 character email
    // Expected Output: true
    public void testValidateCustomer004() {
        Customer testObj = new Customer(0, null, "085 8558585", "j.doe@gmail.com", "26 Athlone", "Region 12Z", null, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        testObj.setPhoneNumber("0999999999999999999");
        testObj.setEmail("johndoeeeeeeeeeeeeeeeeeeeeeee@gmail.com");
        assertEquals(true, validator.validateCustomer(testObj).isValid());
    }

    // Test number: 005
    // Test objective: Test border string lengths
    // Input: 100 character name, 20 character phone number, 40 character email
    // Expected Output: true
    public void testValidateCustomer005() {
        Customer testObj = new Customer(0, null, null, null, "26 Athlone", "Region 12Z", null, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        testObj.setPhoneNumber("09999999999999999999");
        testObj.setEmail("johndoeeeeeeeeeeeeeeeeeeeeeeee@gmail.com");
        assertEquals(true, validator.validateCustomer(testObj).isValid());
    }

    // Test number: 006
    // Test objective: Test beyond border string lengths
    // Input: 101 character name, 21 character phone number, 41 character email
    // Expected Output: 3 errors
    public void testValidateCustomer006() {
        Customer testObj = new Customer(0, null, null, null, "26 Athlone", "Region 12Z", null, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        testObj.setPhoneNumber("099999999999999999999");
        testObj.setEmail("johndoeeeeeeeeeeeeeeeeeeeeeeeee@gmail.com");
        assertEquals(3, validator.validateCustomer(testObj).errors());
    }

    ////////////////////////////////////////////////////////////
    // Order object validation test
    // Employing EP
    // One valid partition:
    // 1.Object with a customer assigned, publication assigned
    // frequency assigned, quantity assigned, next delivery assigned
    // Two invalid partitions
    // 1.Empty object
    // 2.No quantity set
    // 3.No delivery date set
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Object with customer and a publication in it
    // Input: Order(customer, publications)
    // Expected Output: true
    public void testValidateOrder001() {
        Customer testObjCustomer = new Customer(1, "John Doe", "085 8558585", "j.doe@gmail.com", "26 Athlone", "Region 12Z", null, null);
        Publication testObjPublication = new Publication(1, 1, 1, "Times", 10.99);

        Order testObj = new Order(0, testObjCustomer, testObjPublication, 10, "day", "Active", new SimpleDate());

        assertEquals(true, validator.validateOrder(testObj).isValid());
    }

    // Test number: 002
    // Test objective: Empty object
    // Input: Order()
    // Expected Output: 4 errors
    public void testValidateOrder002() {
        Order testObj = new Order();

        assertEquals(4, validator.validateOrder(testObj).errors());
    }

    // Test number: 003
    // Test objective: No quantity set
    // Input: Order()
    // Expected Output: 1 error
    public void testValidateOrder003() {
        Customer testObjCustomer = new Customer(1, "John Doe", "085 8558585", "j.doe@gmail.com", "26 Athlone", "Region 12Z", null, null);
        Publication testObjPublication = new Publication(1, 1, 1, "Times", 10.99);

        Order testObj = new Order(0, testObjCustomer, testObjPublication, 0, "day", "Active", new SimpleDate());

        assertEquals(1, validator.validateOrder(testObj).errors());
    }

    // Test number: 003
    // Test objective: No delivery date set
    // Input: Order()
    // Expected Output: 1 error
    public void testValidateOrder004() {
        Customer testObjCustomer = new Customer(1, "John Doe", "085 8558585", "j.doe@gmail.com", "26 Athlone", "Region 12Z", null, null);
        Publication testObjPublication = new Publication(1, 1, 1, "Times", 10.99);

        Order testObj = new Order(0, testObjCustomer, testObjPublication, 10, "day", "Active", null);

        assertEquals(1, validator.validateOrder(testObj).errors());
    }

    ////////////////////////////////////////////////////////////
    // Publication object validation test
    // Employing EP
    // One valid partition:
    // 1.Object with all valid inputs
    // Two invalid partitions
    // 1.Object with invalid fields
    // 2.Object with empty fields
    //
    // Employing BVA
    // 1. Valid string length on title(199-200)
    // 2. Invalid string length on title(201)
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Object with all valid fields
    // Input: Publication(0, 1, 0, "Times", 10.99)
    // Expected Output: true
    public void testValidatePublication001() {
        Publication testObj = new Publication(0, 1, 0, "Times", 10.99);

        assertEquals(true, validator.validatePublication(testObj).isValid());
    }

    // Test number: 002
    // Test objective: Object with invalid fields
    // Input: Publication(0, 0, 0, "!", -1);
    // Expected Output: 2 errors
    public void testValidatePublication002() {
        Publication testObj = new Publication(0, -1, 0, "!", -1);

        assertEquals(2, validator.validatePublication(testObj).errors());
    }

    // Test number: 003
    // Test objective: Object with empty fields
    // Input: Publication();
    // Expected Output: 1 errors
    public void testValidatePublication003() {
        Publication testObj = new Publication();

        assertEquals(1, validator.validatePublication(testObj).errors());
    }

    // Test number: 004
    // Test objective: Test near border string length
    // Input: title 199 characters
    // Expected Output: true
    public void testValidatePublication004() {
        Publication testObj = new Publication(0, 1, 0, null, 10.99);
        testObj.setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        assertEquals(true, validator.validatePublication(testObj).isValid());

    }

    // Test number: 005
    // Test objective: Test at border string length
    // Input: title 200 characters
    // Expected Output: true
    public void testValidatePublication005() {
        Publication testObj = new Publication(0, 1, 0, null, 10.99);
        testObj.setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        assertEquals(true, validator.validatePublication(testObj).isValid());

    }

    // Test number: 006
    // Test objective: Test beyond border string length
    // Input: title 201 characters
    // Expected Output: 1 error
    public void testValidatePublication006() {
        Publication testObj = new Publication(0, 1, 0, null, 10.99);
        testObj.setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        assertEquals(1, validator.validatePublication(testObj).errors());

    }

    ////////////////////////////////////////////////////////////
    // Driver object validation test
    // Employing EP
    // One valid partition:
    // 1.Object with all valid inputs
    // Two invalid partitions
    // 1.Object with invalid fields
    // 2.Object with empty fields
    //
    // Employing BVA
    // 1. Valid string length on name(99-100)
    // 2. Invalid string length on name(101)
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Object with valid inputs
    // Input: Driver(0, "John Doe", "085 8588585")
    // Expected Output: true
    public void testValidateDriver001() {
        Driver testObj = new Driver(0, "John Doe", "085 8588585");
        assertEquals(true, validator.validateDriver(testObj).isValid());
    }

    // Test number: 002
    // Test objective: Object with invalid inputs
    // Input: Driver(0, "J0hn Do4", "085 85")
    // Expected Output: 2 errors
    public void testValidateDriver002() {
        Driver testObj = new Driver(0, "J0hn Do4", "085 85");

        assertEquals(2, validator.validateDriver(testObj).errors());
    }

    // Test number: 003
    // Test objective: Object with empty fields
    // Input: Driver()
    // Expected Output: 2 errors
    public void testValidateDriver003() {
        Driver testObj = new Driver();

        assertEquals(2, validator.validateDriver(testObj).errors());
    }

    // Test number: 004
    // Test objective: Test near border string lengths
    // Input: 99 character name
    // Expected Output: true
    public void testValidateDriver004() {
        Driver testObj = new Driver(0, null, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        testObj.setPhoneNumber("0999999999999999999");
        assertEquals(true, validator.validateDriver(testObj).isValid());
    }

    // Test number: 005
    // Test objective: Test border string lengths
    // Input: 100 character name
    // Expected Output: true
    public void testValidateDriver005() {
        Driver testObj = new Driver(0, null, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        testObj.setPhoneNumber("09999999999999999999");
        assertEquals(true, validator.validateDriver(testObj).isValid());
    }

    // Test number: 006
    // Test objective: Test beyond border string lengths
    // Input: 101 character name
    // Expected Output: 2 errors
    public void testValidateDriver006() {
        Driver testObj = new Driver(0, null, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        testObj.setPhoneNumber("099999999999999999999");
        assertEquals(2, validator.validateDriver(testObj).errors());
    }

    ////////////////////////////////////////////////////////////
    // Region object validation test
    // Employing EP
    // One valid partition:
    // 1.Object with all valid inputs
    // Two invalid partitions
    // 1.Object with invalid fields
    // 2.Object with empty fields
    //
    // Employing BVA
    // 1. Valid string length on name(29-30)
    // 2. Invalid string length on name(31)
    ////////////////////////////////////////////////////////////
    // Test number: 001
    // Test objective: Object with all valid fields
    // Input: Region(0, "Athlone")
    // Expected Output: true
    public void testValidateRegion001() {
        Region testObj = new Region(0, "Athlone");

        assertEquals(true, validator.validateRegion(testObj).isValid());
    }

    // Test number: 002
    // Test objective: Object with invalid fields
    // Input: Region(0, "!");
    // Expected Output: 1 errors
    public void testValidateRegion002() {
        Region testObj = new Region(0, "!");

        assertEquals(1, validator.validateRegion(testObj).errors());
    }

    // Test number: 003
    // Test objective: Object with empty fields
    // Input: Region();
    // Expected Output: 1 errors
    public void testValidateRegion003() {
        Region testObj = new Region();

        assertEquals(1, validator.validateRegion(testObj).errors());
    }

    // Test number: 004
    // Test objective: Test near border string length
    // Input: name 29 characters
    // Expected Output: true
    public void testValidateRegion004() {
        Region testObj = new Region(0, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        assertEquals(true, validator.validateRegion(testObj).isValid());

    }

    // Test number: 005
    // Test objective: Test at border string length
    // Input: name 30 characters
    // Expected Output: true
    public void testValidateRegion005() {
        Region testObj = new Region(0, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        assertEquals(true, validator.validateRegion(testObj).isValid());

    }

    // Test number: 006
    // Test objective: Test beyond border string length
    // Input: name 31 characters
    // Expected Output: 1 error
    public void testValidateRegion006() {
        Region testObj = new Region(0, null);
        testObj.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        assertEquals(1, validator.validateRegion(testObj).errors());

    }
}
