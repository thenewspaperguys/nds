package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;

import dataStructures.Absent;
import dataStructures.Customer;
import mvc.NDSView;
import staticData.Colors;
import utils.Settings;
import utils.SimpleDate;

/**
 * PanelAbsent displays a GUI for viewing and creating new customer absence
 */
public class ViewAbsent extends javax.swing.JPanel {

    Settings settings;
    NDSView view;
    String[] headings;
    ArrayList<Customer> customers;
    Absent currentlySelectedAbsence = new Absent();

    public ViewAbsent(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jComboBoxCustomer = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        BoxLayout jPanelHeaderLayout = new BoxLayout(jPanel1, BoxLayout.Y_AXIS);
        jPanel1.setLayout(jPanelHeaderLayout);

        jLabel1.setText(settings.getString("lAbsent"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabel1.setFont(font);

        jButton1.setText(settings.getString("lAbsentFrom"));
        Colors.setButtonColors(jButton1, Colors.WARNING, Colors.WHITE);
        jButton2.setText(settings.getString("lAbsentTo"));
        Colors.setButtonColors(jButton2, Colors.WARNING, Colors.WHITE);
        jButton3.setText(settings.getString("bCreateAbsent"));
        Colors.setButtonColors(jButton3, Colors.SUCCESS, Colors.WHITE);

        jPanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanel3.setLayout(new FlowLayout(FlowLayout.CENTER));

        // Add header label
        jPanel2.add(jLabel1);

        // Add Absence buttons
        jPanel3.add(jComboBoxCustomer);
        jPanel3.add(jButton1);
        jPanel3.add(jButton2);
        jPanel3.add(jButton3);

        // Add header label panel and header buttons to the top panel
        jPanel1.add(jPanel2);
        jPanel1.add(jPanel3);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jScrollPane1.setViewportView(jTable1);
        jScrollPane1.setPreferredSize(new Dimension(settings.getInt("viewPanelWidth"), settings.getInt("viewPanelHeight")));

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }

    /////////////////////////////////////////////////////////
    // Panel setup
    /////////////////////////////////////////////////////////

    /**
     * Set the panel's table data
     *
     * @param data Array of all the orders
     */
    public void setTableData(ArrayList<Absent> data) {
        javax.swing.table.DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel();
        tableModel.setColumnIdentifiers(this.headings);

        for (int i = 0; i < data.size(); i++) {
            tableModel.addRow(data.get(i).toArray());
        }

        jTable1.setModel(tableModel);

        jTable1.updateUI();
    }

    /**
     * Set the data for the class
     *
     * @param data     List of all the customers
     * @param headings The headings for the table
     */
    public void setPanelData(ArrayList<Customer> data, String[] headings) {
        this.customers = data;
        this.headings = headings;

        setComboBoxCustomer(this.customers);
    }

    /////////////////////////////////////////////////////////
    // Data getters
    /////////////////////////////////////////////////////////

    /**
     * Retrieve combobox's customer ID
     *
     * @return The order's name as a string
     */
    public int getAbsentCustomerID() {
        return getCurrentlySelectedAbsentCustomer().getID();
    }

    /**
     * Retrieve currently selected customer
     * @return The customer object
     */
    public Customer getCurrentlySelectedAbsentCustomer() {
        int idx = jComboBoxCustomer.getSelectedIndex();
        return this.customers.get(idx);
    }

    /**
     * Return the customer absence object
     *
     * @return The absent object
     */
    public Absent getSelectedAbsence() {
        if (currentlySelectedAbsence.isValid()) {
            return currentlySelectedAbsence;
        } else {
            return null;
        }
    }

    /**
     * Set the currently selected combo box option
     * @param customerId The customer ID sought for
     */
    public void setComboBoxSelection(int customerId) {
        int cusId = 0;

        for (int i = 0; i < this.customers.size(); i++) {

            int currentId = this.customers.get(i).getID();
            if (currentId == customerId) {
                jComboBoxCustomer.setSelectedIndex(i);
                break;
            }
        }

    }

    /////////////////////////////////////////////////////////
    // Data setters
    /////////////////////////////////////////////////////////

    /**
     * Set the currently selected absence's "from" field
     *
     * @param date Absence "from"
     */
    public void setFromAbsence(SimpleDate date) {
        currentlySelectedAbsence.setFrom(date);
        jButton1.setText(date.toString());
    }

    /**
     * Set the currently selected absence's "to" field
     *
     * @param date Absence "to"
     */
    public void setToAbsence(SimpleDate date) {
        currentlySelectedAbsence.setTo(date);
        jButton2.setText(date.toString());
    }

    /**
     * Set the dropdown (combobox) options in the GUI form for a customer selection
     *
     * @param customersList Dropdown options
     */
    private void setComboBoxCustomer(ArrayList<Customer> customersList) {

        String[] options = new String[customersList.size()];

        for (int i = 0; i < options.length; i++) {
            options[i] = customersList.get(i).getName() + "  ID:" + customersList.get(i).getID();
        }
        jComboBoxCustomer.setModel(new javax.swing.DefaultComboBoxModel<>(options));
        jComboBoxCustomer.updateUI();
    }

    /////////////////////////////////////////////////////////
    // Action listeners
    /////////////////////////////////////////////////////////

    /**
     * Attach a listener to the "From" absence button
     *
     * @param listener The action listener to execute
     */
    public void attachFromAbsenceButtonListener(ActionListener listener) {
        jButton1.addActionListener(listener);
    }

    /**
     * Attach a listener to the "To" absence button
     *
     * @param listener The action listener to execute
     */
    public void attachToAbsenceButtonListener(ActionListener listener) {
        jButton2.addActionListener(listener);
    }

    /**
     * Attach a listener to the New Order button
     *
     * @param listener The action listener to execute
     */
    public void attachNewAbsenceButtonListener(ActionListener listener) {
        jButton3.addActionListener(listener);
    }

    /**
     * Attach a listener to the customer chooser combo box
     *
     * @param listener The action listener to execute
     */
    public void attachComboBoxActionListener(ActionListener listener) {
        jComboBoxCustomer.addActionListener(listener);
    }

    /**
     * Show New Customer Absence Panel
     * @param data A list of all customers
     * @param headings Headings for the table
     */
    public void showPanel(ArrayList<Customer> data, String[] headings) {
        setPanelData(data, headings);
        view.changePanel(this, view.main.jButtonCustomers);
    }

    private javax.swing.JComboBox<String> jComboBoxCustomer;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
}

