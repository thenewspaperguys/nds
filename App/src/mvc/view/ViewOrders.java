package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import dataStructures.Order;
import mvc.NDSView;
import staticData.Colors;
import utils.Helpers;
import utils.Settings;

/**
 * PanelOrders displays a table with a list of given orders.
 */
public class ViewOrders extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewOrders(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        BoxLayout jPanelHeaderLayout = new BoxLayout(jPanel1, BoxLayout.Y_AXIS);
        jPanel1.setLayout(jPanelHeaderLayout);

        jLabel1.setText(settings.getString("lOrders"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabel1.setFont(font);

        Font fontSm = new Font(Font.SANS_SERIF, Font.PLAIN, settings.getInt("fSizeStatus"));
        jLabel2.setFont(fontSm);
        jLabel2.setText(settings.getString("colEditable"));

        Font fontMedium = new Font(Font.SANS_SERIF, Font.PLAIN, settings.getInt("fSizeBigButton"));

        jButton1.setText(settings.getString("bNewOrder"));
        Colors.setButtonColors(jButton1, Colors.SUCCESS, Colors.WHITE);
        jButton1.setFont(fontMedium);

        jPanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanel3.setLayout(new FlowLayout(FlowLayout.CENTER));

        // Add header label
        jPanel2.add(jLabel1);

        // Add Header Button
        jPanel3.add(jButton1);

        // Add information label and panel
        jPanel4.setLayout(new FlowLayout(FlowLayout.RIGHT));
        jPanel4.add(jLabel2);

        // Add header label panel and header buttons to the top panel
        jPanel1.add(jPanel2);
        jPanel1.add(jPanel3);
        jPanel1.add(jPanel4);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jScrollPane1.setViewportView(jTable1);
        jScrollPane1.setPreferredSize(new Dimension(settings.getInt("viewPanelWidth"), settings.getInt("viewPanelHeight")));

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }

    /**
     * Set the panel's table data
     *
     * @param data
     *            Array of all the orders
     * @param headings
     *            Headings for the data
     */
    public void setTableData(ArrayList<Order> data, String[] headings) {
        javax.swing.table.DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{}) {

        	Class[] types = new Class[]{java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Integer.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class};

            boolean[] canEdit = new boolean[]{false, false, false, true, true, true, false};

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };

        tableModel.setColumnIdentifiers(headings);

        for (int i = 0; i < data.size(); i++) {
            tableModel.addRow(data.get(i).toArray());
        }

        jTable1.setModel(tableModel);

        setComboBoxColumn(settings.getStringArray("ordStatus"));

        Helpers.setTableColumnWidth(jTable1, 0, settings.getInt("idColWidth"));

        jTable1.updateUI();
    }

    /**
     * Retrieve a value from a cell at the specified row and column
     * @param row The row at which the cell resides
     * @param column The column at which the cell resides
     * @return The value of the cell
     */
    public String getStringAt(int row, int column) {
        return (String) jTable1.getModel().getValueAt(row, column);
    }

    /**
     * Retreive a value from a cell at the specified row and column
     * @param row The row at which the cell resides
     * @param column The column at which the cell resides
     * @return The value of the cell
     */
    public int getIntAt(int row, int column)  {
        return (int) jTable1.getModel().getValueAt(row, column);
    }

    /**
     * Attach a listener to the Order's table
     * @param listener The action listener to execute
     */
    public void attachOrderTableListener(TableModelListener listener) {
    	jTable1.getModel().addTableModelListener(listener);
    }

    /**
     * Set the combo boxes for the status column
     * @param comboOptions Array of options for the combo box
     */
    private void setComboBoxColumn(String [] comboOptions) {
        // combo box for the status column
        JComboBox comboBoxStatus = new JComboBox();
        comboBoxStatus.setModel(new DefaultComboBoxModel<>(comboOptions));

        // combo box for the frequency column
        JComboBox comboBoxFrequency = new JComboBox();
        comboBoxFrequency.setModel(new DefaultComboBoxModel<>(settings.getStringArray("ordFrequency")));

        TableColumn tableColumnStatus = jTable1.getColumnModel().getColumn(5);
        tableColumnStatus.setCellEditor(new DefaultCellEditor(comboBoxStatus));

        TableColumn tableColumnFrequency = jTable1.getColumnModel().getColumn(4);
        tableColumnFrequency.setCellEditor(new DefaultCellEditor(comboBoxFrequency));

        DefaultTableCellRenderer rendererStatus = new DefaultTableCellRenderer();
        tableColumnStatus.setCellRenderer(rendererStatus);

        DefaultTableCellRenderer rendererFrequency = new DefaultTableCellRenderer();
        tableColumnFrequency.setCellRenderer(rendererFrequency);
    }

    /**
     * Attach a listener to the New Order button
     *
     * @param listener
     *            The action listener to execute
     */
    public void attachNewOrderButtonListener(ActionListener listener) {
        jButton1.addActionListener(listener);
    }

    /**
     * Show the Orders Panel
     *
     * @param data
     *            Array of all the orders for the table
     * @param headings
     *            Headings for the data table
     */
    public void showPanel(ArrayList<Order> data, String[] headings) {
        setTableData(data, headings);
        view.changePanel(this, view.main.jButtonOrders);
    }

    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
}
