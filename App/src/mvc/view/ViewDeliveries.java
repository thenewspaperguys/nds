package mvc.view;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import dataStructures.Delivery;
import dataStructures.DeliveryList;
import dataStructures.Driver;
import dataStructures.Order;
import mvc.NDSView;
import staticData.Colors;
import utils.Settings;
import utils.SimpleDate;

/**
 * PanelDeliveries displays a table with a list of given deliveries to do.
 */
public class ViewDeliveries extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewDeliveries(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        // inits
        jPanelHeader = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanelMainContent = new javax.swing.JPanel();

        jPanelRegions = new ArrayList<>();
        jComboBoxDrivers = new ArrayList<>();
        jButtonDispatchRegion = new ArrayList<>();

        // set main panel layout
        setLayout(new java.awt.BorderLayout());

        // set header label and font
        jLabelHeader.setText(settings.getString("lDeliveries"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabelHeader.setFont(font);

        // Add Header to the top panel
        jPanelHeader.add(jLabelHeader);
        jPanelHeader.setBorder(new EmptyBorder(0, 0, 10, 0));
        add(jPanelHeader, java.awt.BorderLayout.PAGE_START);

        // set layout of the main content
        BoxLayout mainContentLayout = new BoxLayout(jPanelMainContent, BoxLayout.Y_AXIS);
        jPanelMainContent.setLayout(mainContentLayout);

        // add main content to the scroller
        jScrollPane1.setViewportView(jPanelMainContent);
        jScrollPane1.setPreferredSize(new Dimension(settings.getInt("viewScrollPanelWidth"), settings.getInt("viewScrollPanelHeight")));
        jScrollPane1.setBorder(new EmptyBorder(0, 0, 0, 0));

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main inits
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set the panel's table data
     *
     * @param orders  Array of all the orders to dispatch today
     * @param drivers Array of all the drivers available
     */
    public void setDeliveriesData(ArrayList<Order> orders, ArrayList<Driver> drivers) {
        this.orders = orders;
        this.drivers = drivers;
        this.distinctRegions = getDistinctRegions();

        refreshGUI();
    }

    /**
     * Refresh and/or recreate all element of the Panel
     */
    private void refreshGUI() {
        resetGUIElements();

        prepareBlankElements(this.distinctRegions.size());

        setDriversComboBoxes();

        addEachRegionToMainPanelArray();

        attachComboBoxListeners();

        initButtonText();

        initDriversToDeliveryList();

        this.updateUI();
    }

    /**
     * Reinitialise panel's arrays and the main view panel
     */
    private void resetGUIElements() {
        this.jPanelMainContent.removeAll();
        this.jComboBoxDrivers = new ArrayList<>();
        this.jPanelRegions = new ArrayList<>();
        this.jButtonDispatchRegion = new ArrayList<>();
        this.deliveryList = new ArrayList<>();
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // Public getters
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Retrieve a list of deliveries for a specific region
     *
     * @param regionIdx The index of the region on the screen (and inside of the DeliveryList array)
     * @return The list of deliveries as a DeliveryList object
     */
    public DeliveryList getDeliveryList(int regionIdx) {
        return deliveryList.get(regionIdx);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Listeners attachments
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Attach listeners to the combo boxes. Selecting a a value in the combo box assigns a driver to that region
     */
    private void attachComboBoxListeners() {
        for (int i = 0; i < this.jComboBoxDrivers.size(); i++) {
            int finalI = i;
            this.jComboBoxDrivers.get(i).addActionListener(actionEvent -> {
                JComboBox comboBox = (JComboBox) actionEvent.getSource();
                int driver = comboBox.getSelectedIndex();
                setButtonText(finalI, drivers.get(driver).getName());
                assignDriverToDelivery(finalI);
            });
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Delivery Assignments
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Assign a driver to a specific region
     *
     * @param regionIdx The index of the region on the screen (and inside of the driver list array)
     */
    private void assignDriverToDelivery(int regionIdx) {
        deliveryList.get(regionIdx).setDriver(getDriverFromCombo(regionIdx));
    }

    /**
     * Get the driver selected from within the combo box
     *
     * @param comboIdx The index of the combo box on the screen (and inside of the combo box array)
     * @return The driver assigned by the combo box
     */
    private Driver getDriverFromCombo(int comboIdx) {
        int comboBoxSelection = jComboBoxDrivers.get(comboIdx).getSelectedIndex();
        return drivers.get(comboBoxSelection);
    }

    /**
     * Pre-assign drivers to each of the regions at startup
     */
    private void initDriversToDeliveryList() {
        for (int i = 0; i < this.distinctRegions.size(); i++) {
            assignDriverToDelivery(i);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Element setters
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set text of a button. Used to put the selected driver's name inside of the button for clarification purposes
     *
     * @param buttonIdx The index of the button on the screen (and inside of the button array)
     * @param text      The text that will be displayed as: "Dispatch to: " + text
     */
    private void setButtonText(int buttonIdx, String text) {
        this.jButtonDispatchRegion.get(buttonIdx).setText(settings.getString("lDispatchTo") + text);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // GUI elements preparation
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prepare blank elements, the amount of elements prepared is based around the amount of regions.
     * If there are 4 regions to dispatch to, then 4 combo boxes, 4 button and 4 panels will be prepared.
     *
     * @param regionCount
     */
    private void prepareBlankElements(int regionCount) {
        String dispatchTo = settings.getString("lDispatchTo");
        // add combo boxes based on the amount of regions
        for (int i = 0; i < regionCount; i++) {
            this.jComboBoxDrivers.add(new JComboBox<String>());
            this.jButtonDispatchRegion.add(new JButton(dispatchTo));
            this.jPanelRegions.add(new JPanel());
        }
    }

    /**
     * Put the driver names inside of the combo boxes.
     */
    private void setDriversComboBoxes() {
        String[] options = new String[this.drivers.size()];

        // add driver names with their ID's
        for (int i = 0; i < options.length; i++) {
            options[i] = this.drivers.get(i).getName() + "  ID:" + this.drivers.get(i).getID();
        }

        for (int i = 0; i < this.jComboBoxDrivers.size(); i++) {
            this.jComboBoxDrivers.get(i).setModel(new javax.swing.DefaultComboBoxModel<>(options));
            this.jComboBoxDrivers.get(i).updateUI();
        }
    }

    /**
     * Preset the button texts with the names of the drivers at startup
     */
    private void initButtonText() {
        for (int i = 0; i < this.distinctRegions.size(); i++) {
            setButtonText(i, getDriverFromCombo(i).getName());
            Colors.setButtonColors(this.jButtonDispatchRegion.get(i), Colors.SUCCESS, Colors.WHITE);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Getting distinct regions, customers, orders
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Extract distinct Regions from the pool of all the Orders
     *
     * @return Distinct Regions from the pool of Orders
     */
    private ArrayList<String> getDistinctRegions() {
        ArrayList<String> extractedRegions = new ArrayList<>();

        for (Order order : this.orders) {
            extractedRegions.add(order.getCustomer().getRegion());
        }

        // remove duplicates
        return new ArrayList<String>(new LinkedHashSet<String>(extractedRegions));
    }

    /**
     * Extract distinct Customers from a pool of Orders
     *
     * @param orders The pool of Orders to look through
     * @return Distinct Customers from the Order pool
     */
    private ArrayList<Integer> getDistinctCustomers(ArrayList<Order> orders) {
        ArrayList<Integer> extractedCustomers = new ArrayList<>();

        for (Order order : orders) {
            extractedCustomers.add(order.getCustomer().getID());
        }

        // remove duplicates
        return new ArrayList<Integer>(new LinkedHashSet<Integer>(extractedCustomers));
    }

    /**
     * Extract distinct orders for a specific customer from a pool of orders
     *
     * @param eachRegionsOrders A pool of orders for a specific region
     * @param customerID        The ID of the customer of interest
     * @return Distinct orders for the required customer
     */
    private ArrayList<Order> getDistinctCustomerOrders(ArrayList<Order> eachRegionsOrders, int customerID) {
        ArrayList<Order> distinctCustomerOrders = new ArrayList<>();

        for (int i = 0; i < eachRegionsOrders.size(); i++) {
            if (eachRegionsOrders.get(i).getCustomer().getID() == customerID) {
                distinctCustomerOrders.add(eachRegionsOrders.get(i));
            }
        }
        return distinctCustomerOrders;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Panel constructors
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Iterate through each of the distinct regions.
     * Create orders and delivery objects for that region.
     * Create a delivery list for the region.
     * Construct the panel for that region
     * Add each of the regions made to the main scrollable content
     */
    private void addEachRegionToMainPanelArray() {
        for (int i = 0; i < this.distinctRegions.size(); i++) {
            String eachDistinctRegion = this.distinctRegions.get(i);

            ArrayList<Order> ordersInRegion = new ArrayList<>();
            ArrayList<Delivery> deliveries = new ArrayList<>();

            for (int j = 0; j < this.orders.size(); j++) {
                // if the order is in the searched region, then add the order to the list
                if (this.orders.get(j).getCustomer().getRegion().equals(eachDistinctRegion)) {
                    ordersInRegion.add(this.orders.get(j));
                }
            }

            // construct the panel for the region and add it to the list of panels
            this.jPanelRegions.set(i, constructRegionPanel(ordersInRegion, i));

            // add the orders to the delivery list
            for (Order eachOrder : ordersInRegion) {
                deliveries.add(new Delivery(0, eachOrder, false, false, new SimpleDate()));
            }

            // create a list of deliveries for each of the regions; set the drivers after init
            deliveryList.add(new DeliveryList(null, deliveries));
        }
        // add the panels to the main content
        for (int i = 0; i < this.jPanelRegions.size(); i++) {
            this.jPanelMainContent.add(this.jPanelRegions.get(i));
        }
    }

    /**
     * Construct a region panel
     *
     * @param ordersForRegion A pool of orders for this specific region
     * @param panelNumber     The panel number to which the orders get added to (helps with combo and button assignments)
     * @return The constructed panel
     */
    private JPanel constructRegionPanel(ArrayList<Order> ordersForRegion, int panelNumber) {
        JPanel jPanelRegion = new JPanel();
        JPanel jPanelHeader = new JPanel();
        JPanel jPanelDriverSelectAndDispatch = new JPanel();
        JLabel jLabelRegionName = new JLabel();
        JLabel jLabelSelectDriver = new JLabel();

        // set layout of the region to box
        BoxLayout panelRegionLayout = new BoxLayout(jPanelRegion, BoxLayout.Y_AXIS);
        jPanelRegion.setLayout(panelRegionLayout);

        // region label
        Font bold = new Font(Font.SANS_SERIF, Font.BOLD | Font.ITALIC, 15);
        jLabelRegionName.setText(settings.getString("lRegionHeader") + ordersForRegion.get(0).getCustomer().getRegion());
        jLabelRegionName.setFont(bold);
        Colors.setLabelColors(jLabelRegionName, null, Colors.WHITE);

        jPanelHeader.add(jLabelRegionName);
        Colors.setPanelColors(jPanelHeader, Colors.DARKPRIMARY, null);
        jPanelHeader.setBorder(new EmptyBorder(10, 0, 10, 0));

        // select driver panel
        jLabelSelectDriver.setText(settings.getString("lDriverSelect"));
        jPanelDriverSelectAndDispatch.add(jLabelSelectDriver);
        jPanelDriverSelectAndDispatch.add(this.jComboBoxDrivers.get(panelNumber));
        jPanelDriverSelectAndDispatch.add(this.jButtonDispatchRegion.get(panelNumber));

        // add sections in the correct order of appearance
        jPanelRegion.add(jPanelHeader);
        jPanelRegion.add(jPanelDriverSelectAndDispatch);

        ArrayList<Integer> distinctCustomers = getDistinctCustomers(ordersForRegion);

        for (int i = 0; i < distinctCustomers.size(); i++) {
            int distinctCustomerID = distinctCustomers.get(i);  // grab customer id
            // grab orders from the distinct customer
            JPanel jPanelCustomerOrderDetails = constructCustomerDeliveryPanel(getDistinctCustomerOrders(ordersForRegion, distinctCustomerID));
            // add the panel
            jPanelRegion.add(jPanelCustomerOrderDetails);
        }

        jPanelRegion.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 10, 50, 10), new LineBorder(Colors.BLACK)));

        return jPanelRegion;
    }

    /**
     * Construct a panel with customer's individual deliveries table
     *
     * @param individualCustomerOrder A pool of orders for a specific customer
     * @return The constructed panel
     */
    private JPanel constructCustomerDeliveryPanel(ArrayList<Order> individualCustomerOrder) {
        JPanel jPanelCustomerOrder = new JPanel();
        JTable jTableCustomerOrders = new JTable();
        JLabel jLabelCustomerName = new JLabel();
        JScrollPane jScrollPaneCustomerOrders = new JScrollPane();

        // set layout
        BoxLayout panelCustomerOrderLayout = new BoxLayout(jPanelCustomerOrder, BoxLayout.Y_AXIS);
        jPanelCustomerOrder.setLayout(panelCustomerOrderLayout);

        // add customer name
        Font bold = new Font(Font.SANS_SERIF, Font.BOLD, 12);
        String cusName = individualCustomerOrder.get(0).getCustomer().getName();
        String suffix = "'s orders";
        if (cusName.charAt(cusName.length() - 1) == 's') {
            suffix = "' orders";
        }

        // add label to the top
        jLabelCustomerName.setText(cusName + suffix);
        jLabelCustomerName.setFont(bold);
        jPanelCustomerOrder.add(jLabelCustomerName);

        // create table model and add rows
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(settings.getStringArray("cusDispatch"));

        for (int i = 0; i < individualCustomerOrder.size(); i++) {
            String title = individualCustomerOrder.get(i).getPublication().getTitle();
            String qty = Integer.toString(individualCustomerOrder.get(i).getQty());
            tableModel.addRow(new String[]{title, qty});
        }

        // assign the model to the table
        jTableCustomerOrders.setModel(tableModel);
        jTableCustomerOrders.updateUI();

        jScrollPaneCustomerOrders.setViewportView(jTableCustomerOrders);
        jScrollPaneCustomerOrders.setWheelScrollingEnabled(false);
        // hacky scrollPane height, based off the number of rows in the table
        jScrollPaneCustomerOrders.setPreferredSize(new Dimension(300, (individualCustomerOrder.size() * 18) + 28));
        jScrollPaneCustomerOrders.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 10, 0, 10), new LineBorder(Color.GRAY)));

        jPanelCustomerOrder.add(jScrollPaneCustomerOrders);
        jPanelCustomerOrder.setBorder(new EmptyBorder(0, 0, 20, 0));

        return jPanelCustomerOrder;
    }

    /**
     * Show the Deliveries Panel
     *
     * @param orders
     *            Array of all the orders to dispatch today
     * @param drivers
     *            Drivers available
     */
    public void showPanel(ArrayList<Order> orders, ArrayList<Driver> drivers) {
        setDeliveriesData(orders, drivers);
        view.changePanel(this, view.main.jButtonDeliveries);
    }

    // header
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JLabel jLabelHeader;

    // main content
    private javax.swing.JPanel jPanelMainContent;
    private javax.swing.JScrollPane jScrollPane1;

    // region panel
    private ArrayList<javax.swing.JPanel> jPanelRegions;
    private ArrayList<javax.swing.JComboBox<String>> jComboBoxDrivers;
    public ArrayList<javax.swing.JButton> jButtonDispatchRegion;

    // deliveries data
    private ArrayList<Order> orders;
    private ArrayList<Driver> drivers;
    private ArrayList<String> distinctRegions;
    private ArrayList<DeliveryList> deliveryList;

}
