package mvc.view;

import staticData.Colors;

import javax.swing.*;
import java.awt.*;

/**
 * Frame that displays the printable output
 */
public class ViewPrintService {

    String windowTitle;
    /**
     * Creates new form FramePrintService
     */
    public ViewPrintService(String content, String windowTitle) {
        this.windowTitle = windowTitle;
        initComponents(content);
    }

    /**
     * Setup default "Nimbus" look and feel
     */
    private void setupUX() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewPrintService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewPrintService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewPrintService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewPrintService.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    private void initComponents(String content) {

        jFrame = new javax.swing.JFrame();
        jButtonPrintNClose = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        jFrame.setTitle(this.windowTitle);

        // print and close button
        jButtonPrintNClose.setText("Print And Close");
        Colors.setButtonColors(jButtonPrintNClose, Colors.SUCCESS, Colors.WHITE);
        jButtonPrintNClose.addActionListener((e) -> {
            jFrame.setVisible(false);
            jFrame.dispose();
        });
        jFrame.getContentPane().add(jButtonPrintNClose, java.awt.BorderLayout.PAGE_START);

        // text area
        jTextArea1.setColumns(80);
        jTextArea1.setRows(30);
        jTextArea1.setWrapStyleWord(true);
        Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
        jTextArea1.setFont(font);
        jTextArea1.setText(content);
        jScrollPane1.setViewportView(jTextArea1);

        // scroll pane
        jFrame.getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jFrame.pack();
        jFrame.setVisible(true);
    }

    private javax.swing.JFrame jFrame;
    private javax.swing.JButton jButtonPrintNClose;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
}
