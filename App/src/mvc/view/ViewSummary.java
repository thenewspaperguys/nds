package mvc.view;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.border.EmptyBorder;

import mvc.NDSView;
import staticData.Colors;
import utils.Settings;
import utils.SimpleDate;

/**
 * PanelSummary displays daily sales summary
 */
public class ViewSummary extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewSummary(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jComboBox = new javax.swing.JComboBox<Integer>();

        setLayout(new java.awt.BorderLayout());

        BoxLayout jPanelHeaderLayout = new BoxLayout(jPanel1, BoxLayout.Y_AXIS);
        jPanel1.setLayout(jPanelHeaderLayout);

        // set header label
        jLabel1.setText(settings.getString("lSummary"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabel1.setFont(font);

        // set label 2 and buttons
        Font fontMedium = new Font(Font.DIALOG, Font.PLAIN, settings.getInt("fSizeBigButton"));
        jButton1.setFont(fontMedium);
        Colors.setButtonColors(jButton1, Colors.WARNING, Colors.WHITE);
        jButton2.setFont(fontMedium);
        Colors.setButtonColors(jButton2, Colors.SUCCESS, Colors.WHITE);
        jLabel2.setFont(fontMedium);

        jLabel2.setText(settings.getString("eSelectDate"));

        jButton2.setText(settings.getString("bShowSummary"));

        //set label 3 and button
        jButton3.setFont(fontMedium);
        Colors.setButtonColors(jButton3, Colors.SUCCESS, Colors.WHITE);
        jLabel3.setFont(fontMedium);

        jLabel3.setText(settings.getString("lSelectYear"));
        jButton3.setText(settings.getString("bShowReport"));

        // set header and button container layouts
        jPanel2.setBorder(new EmptyBorder(0, 0, 40, 0));
        jPanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanel3.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanel4.setLayout(new FlowLayout(FlowLayout.CENTER));

        // Add header label
        jPanel2.add(jLabel1);

        // Add buttons and the description
        jPanel3.add(jLabel2);
        jPanel3.add(jButton1);
        jPanel3.add(jButton2);

        jPanel4.add(jLabel3);
        jPanel4.add(jComboBox);
        jPanel4.add(jButton3);

        // Add header label panel and header buttons to the top panel
        jPanel1.add(jPanel2);
        jPanel1.add(jPanel3);
        jPanel1.add(jPanel4);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);
    }

    public void init() {
        setJButtonDate(new SimpleDate().toString());
    }

    /**
     * Set the main button's text
     *
     * @param text The string that will be set as the button's text
     */
    public void setJButtonDate(String text) {
        jButton1.setText(text);
    }

    /**
     * Retrieve the currently selected date
     * @return The currently selected date as a SimpleDate object
     */
    public SimpleDate getSelectedDate() {
        return new SimpleDate(jButton1.getText());
    }

    /**
     * Retrieve customer's region
     *
     * @return The customer's region as string
     */
    public int getSelectedYear() {
        return (int) jComboBox.getSelectedItem();
    }

    /**
     * Set a value in the combobox
     * @param year
     */
    private void setSelectedYear(int year) {
        jComboBox.setSelectedItem(year);
    }

    /**
     * Set the current year as the default value in the combo box
     */
    private void setCurrentYearAsDefault() {
        SimpleDate simpleDate = new SimpleDate();
        setSelectedYear(simpleDate.getYear());
    }

    /**
     * Set the dropdown (combobox) options in the GUI form
     *
     * @param options Dropdown options
     */
    public void setComboBox(Integer[] options) {
        jComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(options));
        jComboBox.updateUI();
    }

    /**
     * Attach an action listener to the date chooser button
     *
     * @param listener The listener to be attached
     */
    public void attachSummaryDateButtonListener(ActionListener listener) {
        jButton1.addActionListener(listener);
    }

    /**
     * Attach an action listener to the "show summary" button
     *
     * @param listener The listener to be attached
     */
    public void attachShowSummaryButtonListener(ActionListener listener) {
        jButton2.addActionListener(listener);
    }

    /**
     * Attach an action listener to the "show report" button
     * @param listener The listener to be attached
     */
    public void attachShowReportButtonListener(ActionListener listener) {
        jButton3.addActionListener(listener);
    }

    /**
     * Show the Summary Panel
     */
    public void showPanel(Integer [] years) {
        init();
        setComboBox(years);
        setCurrentYearAsDefault();
        view.changePanel(this, view.main.jButtonSummary);
    }

    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JComboBox jComboBox;
}
