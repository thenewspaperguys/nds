package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;

import mvc.NDSView;
import staticData.Colors;
import utils.Settings;

/**
 * PanelNewDriver displays a form with input fields to create a new customer.
 */
public class ViewNewDriver extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewNewDriver(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jLabelName = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();

        jLabelPhoneNumber = new javax.swing.JLabel();
        jTextFieldPhoneNumber = new javax.swing.JTextField();

        jButtonOK = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jPanelHeader = new javax.swing.JPanel();
        jPanelForm = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();

        BoxLayout mainLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(mainLayout);

        jLabelHeader.setText(settings.getString("lNewDriver"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabelHeader.setFont(font);

        jPanelHeader.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanelHeader.add(jLabelHeader);

        GridLayout panelLayout = new GridLayout(3, 2);
        panelLayout.setVgap(10);
        panelLayout.setHgap(5);
        jPanelForm.setLayout(panelLayout);

        jLabelName.setText(settings.getString("lName"));
        jPanelForm.add(jLabelName);

        jPanelForm.add(jTextFieldName);

        jLabelPhoneNumber.setText(settings.getString("lPhone"));
        jPanelForm.add(jLabelPhoneNumber);

        jPanelForm.add(jTextFieldPhoneNumber);

        jButtonOK.setText(settings.getString("ok"));
        Colors.setButtonColors(jButtonOK, Colors.SUCCESS, Colors.WHITE);
        jPanelForm.add(jButtonOK);

        jButtonCancel.setText(settings.getString("cancel"));
        Colors.setButtonColors(jButtonCancel, Colors.DANGER, Colors.WHITE);
        jPanelForm.add(jButtonCancel);

        add(jPanelHeader);
        add(jPanelForm);

        setMinimumSize(new Dimension(100, 100));
        setPreferredSize(new Dimension(400, 250));
    }

    /**
     * Attach a listener to the New Driver's OK button
     *
     * @param listener The action listener to execute
     */
    public void attachNewDriverOKButtonListener(ActionListener listener) {
        jButtonOK.addActionListener(listener);
    }

    /**
     * Attach a listener to the New Driver's CANCEL button
     *
     * @param listener The action listener to execute
     */
    public void attachNewDriverCancelButtonListener(ActionListener listener) {
        jButtonCancel.addActionListener(listener);
    }

    /**
     * Retrieve driver's name
     *
     * @return The driver's name as a string
     */
    public String getDriverName() {
        return jTextFieldName.getText();
    }

    /**
     * Retrieve driver's phone number
     *
     * @return The driver's phone number as string
     */
    public String getDriverPhoneNumber() {
        return jTextFieldPhoneNumber.getText();
    }

    /**
     * Reset all the fields in the GUI form to blank
     */
    public void clearFields() {
        jTextFieldName.setText("");
        jTextFieldPhoneNumber.setText("");
    }

    /**
     * Show the New Driver Panel
     */
    public void showPanel() {
        clearFields();
        view.changePanel(this, view.main.jButtonDrivers);
    }

    private javax.swing.JLabel jLabelName;
    private javax.swing.JLabel jLabelPhoneNumber;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldPhoneNumber;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelForm;
    private javax.swing.JLabel jLabelHeader;
}
