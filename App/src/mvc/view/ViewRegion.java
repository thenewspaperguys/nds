package mvc.view;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import dataStructures.Region;
import mvc.NDSView;
import staticData.Colors;
import utils.Helpers;
import utils.Settings;

/**
 * PanelRegion displays a table with a list of given regions.
 */
public class ViewRegion extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewRegion(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        BoxLayout jPanelHeaderLayout = new BoxLayout(jPanel1, BoxLayout.Y_AXIS);
        jPanel1.setLayout(jPanelHeaderLayout);

        jLabel1.setText(settings.getString("lRegions"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabel1.setFont(font);

        Font fontSm = new Font(Font.SANS_SERIF, Font.PLAIN, settings.getInt("fSizeStatus"));
        jLabel2.setFont(fontSm);
        jLabel2.setText(settings.getString("colEditable"));

        Font fontMedium = new Font(Font.SANS_SERIF, Font.PLAIN, settings.getInt("fSizeBigButton"));

        jButton1.setText(settings.getString("bNewRegions"));
        Colors.setButtonColors(jButton1, Colors.SUCCESS, Colors.WHITE);
        jButton1.setFont(fontMedium);

        jPanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanel3.setLayout(new FlowLayout(FlowLayout.CENTER));

        // Add header label
        jPanel2.add(jLabel1);

        // Add Header Button
        jPanel3.add(jButton1);

        // Add information label and panel
        jPanel4.setLayout(new FlowLayout(FlowLayout.RIGHT));
        jPanel4.add(jLabel2);

        // Add header label panel and header buttons to the top panel
        jPanel1.add(jPanel2);
        jPanel1.add(jPanel3);
        jPanel1.add(jPanel4);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jScrollPane1.setViewportView(jTable1);
        jScrollPane1.setPreferredSize(new Dimension(settings.getInt("viewPanelWidth"), settings.getInt("viewPanelHeight")));

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }

    /**
     * Set the panel's table data
     *
     * @param data
     *            Array of all the drivers
     * @param headings
     *            Headings for the data
     */
    public void setTableData(ArrayList<Region> data, String[] headings) {
        javax.swing.table.DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel();
        tableModel.setColumnIdentifiers(headings);

        for (int i = 0; i < data.size(); i++) {
            tableModel.addRow(data.get(i).toArray());
        }

        jTable1.setModel(tableModel);

        Helpers.setTableColumnWidth(jTable1, 0, settings.getInt("idColWidth"));

        jTable1.updateUI();
    }

    /**
     * Attach a listener to the New Region button
     *
     * @param listener
     *            The action listener to execute
     */
    public void attachNewRegionButtonListener(ActionListener listener) {
        jButton1.addActionListener(listener);
    }

    /**
     * Show the Region Panel
     *
     * @param data
     *            Array of all the drivers for the table
     * @param headings
     *            Headings for the data table
     */
    public void showPanel(ArrayList<Region> data, String[] headings) {
        setTableData(data, headings);
        view.changePanel(this, view.main.jButtonRegions);
    }

    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
}
