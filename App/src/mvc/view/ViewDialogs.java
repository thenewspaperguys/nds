package mvc.view;

import mvc.NDSView;
import utils.DatePicker;
import utils.Settings;
import utils.SimpleDate;

import javax.swing.*;

public class ViewDialogs {

    NDSView view;
    Settings settings;

    public ViewDialogs(NDSView view, Settings settings) {
        this.view = view;
        this.settings = settings;
    }

    /**
     * Show a dialog with a single "OK" button
     * @param message Message that will be displayed in the dialog
     */
    public void okDialog(String message) {
        JOptionPane.showMessageDialog(view.main, message);
    }

    /**
     * Show a dialog with "OK" and "CANCEL" buttons
     * @param message Message that will be displayed in the dialog
     * @return Return true if OK was pressed, false otherwise
     */
    public boolean okCancelDialog(String message) {
        int result = JOptionPane.showConfirmDialog(view.main, message, "alert", JOptionPane.OK_CANCEL_OPTION);
        if (result == 2 || result == -1) {
            return false;
        }
        return true;
    }

    /**
     * Show the date picker dialog
     * @return The SimpleDate object with the selected date
     */
    public SimpleDate datePicker() {
        DatePicker datePicker = new DatePicker(view.main);
        return datePicker.getDate();
    }

    /**
     * Convenience method to construct a dialog from an array of strings and a prompt
     * @param publications Array of strings
     * @param prompt The prompt to be displayed
     */
    public void fromStringArray(String[] publications, String prompt) {
        if (publications == null || publications.length == 0) {
            return;
        }
        String output = prompt + ": \n\n";

        for (String string : publications) {
            output += string + "\n";
        }

        okDialog(output);
    }
}
