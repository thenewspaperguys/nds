package mvc.view;

import mvc.NDSView;
import staticData.Colors;
import utils.Settings;

import javax.swing.Timer;
import java.awt.*;

public class ViewStatus {

    NDSView view;
    Settings settings;

    private Timer statusTimer;

    public ViewStatus(NDSView view, Settings settings) {
        this.view = view;
        this.settings = settings;
    }

    /**
     * Display a message in the status bar
     *
     * @param status
     *            Message that will be displayed. Add "+" at the beginning of
     *            the string to display a green success style message. Or add
     *            a "!" at the beginning of the string to display a warning
     * @param delayMs
     *            Time in milliseconds after which the status will disappear
     */
    public void set(String status, int delayMs) {
        setStatusLabel(status);
        statusTimer = new Timer(delayMs, (e) -> {
            statusTimer.stop();
            clearStatus();
        });
        statusTimer.start();
    }

    /**
     * Display a message in the status bar permanently, or until cleared manually
     *
     * @param status
     *            Message that will be displayed
     */
    public void set(String status) {
        setStatusLabel(status);
    }

    /**
     * Set the text of the status bar JLable
     *
     * @param status
     *            Message that will be displayed
     */
    private void setStatusLabel(String status) {
        boolean extract = false;

        if (status.charAt(0) == '!') {
            Colors.setPanelColors(view.main.jPanelStatus, Colors.DANGER, null);
            Colors.setLabelColors(view.main.jLabelStatus, null, Colors.WHITE);
            extract = true;

        } else if (status.charAt(0) == '+') {
            Colors.setPanelColors(view.main.jPanelStatus, Colors.SUCCESS, null);
            Colors.setLabelColors(view.main.jLabelStatus, null, Colors.WHITE);
            extract = true;

        } else {
            Colors.setPanelColors(view.main.jPanelStatus, Color.GRAY, null);
            Colors.setLabelColors(view.main.jLabelStatus, null, Colors.WHITE);
        }

        if (extract) {
            status = status.substring(1, status.length());
        }
        view.main.jLabelStatus.setText(settings.getString("status") + status);
    }

    /**
     * Clear the status bar and set it to display "Status: OK"
     */
    private void clearStatus() {
        setStatusLabel(settings.getString("ok"));
    }
}
