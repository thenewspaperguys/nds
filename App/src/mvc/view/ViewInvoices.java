package mvc.view;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import dataStructures.Invoice;
import mvc.NDSView;
import staticData.Colors;
import utils.Settings;

/**
 * PanelInvoices displays a table with a list of given invoices.
 */
public class ViewInvoices extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewInvoices(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        // inits
        jPanelHeader = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanelMainContent = new javax.swing.JPanel();

        jPanelCustomers = new ArrayList<>();

        // set main panel layout
        setLayout(new java.awt.BorderLayout());

        // set header label and font
        jLabelHeader.setText(settings.getString("lInvoices"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabelHeader.setFont(font);

        // add header label to the top panel
        jPanelHeader.add(jLabelHeader);
        jPanelHeader.setBorder(new EmptyBorder(0, 0, 10, 0));
        add(jPanelHeader, BorderLayout.PAGE_START);

        // main content layout
        BoxLayout mainContentLayout = new BoxLayout(jPanelMainContent, BoxLayout.Y_AXIS);
        jPanelMainContent.setLayout(mainContentLayout);

        // add main content to the scroller
        jScrollPane1.setViewportView(jPanelMainContent);
        jScrollPane1.setPreferredSize(new Dimension(settings.getInt("viewScrollPanelWidth"), settings.getInt("viewScrollPanelHeight")));
        jScrollPane1.setBorder(new EmptyBorder(0, 0, 0, 0));

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main inits
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set the panel's table data
     *
     * @param invoices Array of all the invoices
     */
    public void setTableData(ArrayList<Invoice> invoices) {
        this.invoices = invoices;
        this.distinctCustomerIDs = getDistinctCustomerIDs();
        refreshGUI();
    }

    /**
     * Refresh and/or recreate all elements of the Panel
     */
    private void refreshGUI() {
        resetGUIElements();

        addEachCustomerToMainPanelArray();

        this.updateUI();
    }

    /**
     * Reinitialise panel's arrays and the main view panel
     */
    private void resetGUIElements() {
        this.jPanelMainContent.removeAll();
        this.jPanelCustomers = new ArrayList<>();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Getting distinct customers and invoices
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Extract distinct customer IDs from all the invoices
     *
     * @return Distinct customer IDs
     */
    private ArrayList<Integer> getDistinctCustomerIDs() {
        ArrayList<Integer> extractedCustomers = new ArrayList<>();

        for (Invoice invoice : this.invoices) {
            extractedCustomers.add(invoice.getCustomer().getID());
        }
        // remove duplicates
        return new ArrayList<Integer>(new LinkedHashSet<Integer>(extractedCustomers));
    }

    /**
     * Get all invoices that belong to an individual customer
     *
     * @param customerID The ID of the customer of interest
     * @return Distinct invoices of the required cutomer
     */
    private ArrayList<Invoice> getIndividualCustomerInvoices(int customerID) {
        ArrayList<Invoice> customerInvoices = new ArrayList<>();

        for (int i = 0; i < this.invoices.size(); i++) {

            if (this.invoices.get(i).getCustomer().getID() == customerID) {
                customerInvoices.add(this.invoices.get(i));
            }
        }

        return customerInvoices;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Panel constructors
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Iterate through the array of the distinct Customers.
     * Create individual invoice views per customer with a
     * list of all publications sold within that particular invoice.
     */
    private void addEachCustomerToMainPanelArray() {

        for (int i = 0; i < this.distinctCustomerIDs.size(); i++) {

            int currentCustomerID = this.distinctCustomerIDs.get(i);
            ArrayList<Invoice> individualCustomerInvoices = getIndividualCustomerInvoices(currentCustomerID);

            this.jPanelCustomers.add(constructCustomerPanel(individualCustomerInvoices));
        }

        for (JPanel eachCustomer : this.jPanelCustomers) {
            this.jPanelMainContent.add(eachCustomer);
        }
    }

    /**
     * Construct a panel for an individual customer
     *
     * @param individualCustomerInvoices List of all the distinct invoices for the customer
     * @return The constructed panel
     */
    private JPanel constructCustomerPanel(ArrayList<Invoice> individualCustomerInvoices) {

        JPanel jPanelCustomer = new JPanel();
        JPanel jPanelHeader = new JPanel();
        JLabel jLabelCustomerDetails = new JLabel();

        // set layout of the customer panel
        BoxLayout panelCustomerLayout = new BoxLayout(jPanelCustomer, BoxLayout.Y_AXIS);
        jPanelCustomer.setLayout(panelCustomerLayout);

        // customer label
        Font bold = new Font(Font.SANS_SERIF, Font.BOLD | Font.ITALIC, 15);
        jLabelCustomerDetails.setText(settings.getString("lCusName") + individualCustomerInvoices.get(0).getCustomer().getName());
        jLabelCustomerDetails.setFont(bold);
        Colors.setLabelColors(jLabelCustomerDetails, null, Colors.WHITE);

        jPanelHeader.add(jLabelCustomerDetails);
        Colors.setPanelColors(jPanelHeader, Colors.DARKPRIMARY, null);
        jPanelHeader.setBorder(new EmptyBorder(10, 0, 10, 0));

        // add section of the panel in the correct order
        jPanelCustomer.add(jPanelHeader);

        for (int i = 0; i < individualCustomerInvoices.size(); i++) {
            jPanelCustomer.add(constructInvoicePanel(individualCustomerInvoices.get(i)));
        }

        jPanelCustomer.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 15, 40, 10), new LineBorder(Colors.BLACK)));

        return jPanelCustomer;
    }

    /**
     * Construct a panel for every individual invoice
     *
     * @param individualInvoice The invoice that will be constructed
     * @return The panel constructed with the invoice details
     */
    private JPanel constructInvoicePanel(Invoice individualInvoice) {
        JPanel jPanelInvoiceDetails = new JPanel();
        JPanel jPanelInvoiceTotal = new JPanel();
        JLabel jLabelInvoiceDetails = new JLabel();
        JLabel jLabelInvoiceTotal = new JLabel();
        JTable jTablePublications = new JTable();
        JScrollPane jScrollPublication = new JScrollPane();
        double invoiceTotal = 0;

        // set layout of the invoice panel
        BoxLayout panelInvoiceLayout = new BoxLayout(jPanelInvoiceDetails, BoxLayout.Y_AXIS);
        jPanelInvoiceDetails.setLayout(panelInvoiceLayout);

        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(settings.getStringArray("headingInvoices"));

        for (int i = 0; i < individualInvoice.getPublications().size(); i++) {
            int qty = individualInvoice.getPublications().get(i).getQty();
            double price = individualInvoice.getPublications().get(i).getPrice();

            String title = individualInvoice.getPublications().get(i).getTitle();

            tableModel.addRow(new String[]{title, Integer.toString(qty), Double.toString(price * qty)});

            invoiceTotal += (qty * price);
        }

        invoiceTotal += individualInvoice.getPriceCorrection();

        // add invoice info to the label
        Font bold = new Font(Font.SANS_SERIF, Font.BOLD, 12);
        String invInfo = settings.getString("lInvDetailID") + individualInvoice.getID();
        invInfo += settings.getString("lInvDetailDate") + individualInvoice.getDeliveryDate();
        String totalPrice = settings.getString("lInvDetailPrice") + invoiceTotal + " " + settings.getString("currency");

        // add id and date label
        jLabelInvoiceDetails.setFont(bold);
        jLabelInvoiceDetails.setText(invInfo);
        Colors.setLabelColors(jLabelInvoiceDetails, null, Colors.DARKGRAY);
        jPanelInvoiceDetails.add(jLabelInvoiceDetails);

        // set model
        jTablePublications.setModel(tableModel);
        jTablePublications.updateUI();

        jScrollPublication.setViewportView(jTablePublications);
        jScrollPublication.setWheelScrollingEnabled(false);

        // hacky scrollPane height, based off the number of rows in the table
        jScrollPublication.setPreferredSize(new Dimension(300, (individualInvoice.getPublications().size() * 18) + 28));
        jScrollPublication.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 10, 0, 10), new LineBorder(Color.GRAY)));

        jPanelInvoiceDetails.add(jScrollPublication);

        // add total price label with its panel
        jLabelInvoiceTotal.setFont(bold);
        jLabelInvoiceTotal.setText(totalPrice);
        Colors.setLabelColors(jLabelInvoiceTotal, null, Colors.DARKGRAY);

        jPanelInvoiceTotal.setLayout(new FlowLayout(FlowLayout.RIGHT));
        jPanelInvoiceTotal.add(jLabelInvoiceTotal);
        jPanelInvoiceDetails.add(jPanelInvoiceTotal);

        jPanelInvoiceDetails.setBorder(new EmptyBorder(10, 0, 10, 0));

        return jPanelInvoiceDetails;
    }

    /**
     * Show the Invoices Panel
     *
     * @param data
     *            Array of all the invoices for the table
     */
    public void showPanel(ArrayList<Invoice> data) {
        setTableData(data);
        view.changePanel(this, view.main.jButtonInvoices);
    }

    // header
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JLabel jLabelHeader;

    // main content
    private javax.swing.JPanel jPanelMainContent;
    private javax.swing.JScrollPane jScrollPane1;

    // customer invoices panels
    private ArrayList<JPanel> jPanelCustomers;

    // invoices data
    private ArrayList<Integer> distinctCustomerIDs;
    private ArrayList<Invoice> invoices;

}
