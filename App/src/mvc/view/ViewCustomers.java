package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import dataStructures.Customer;
import dataStructures.Publication;
import mvc.NDSView;
import staticData.Colors;
import utils.Helpers;
import utils.Settings;

/**
 * PanelCustomers displays a table with a list of given customers.
 */
public class ViewCustomers extends javax.swing.JPanel {

    Settings settings;
    NDSView view;
    ArrayList<Customer> customers;

    public ViewCustomers(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        BoxLayout jPanelHeaderLayout = new BoxLayout(jPanel1, BoxLayout.Y_AXIS);
        jPanel1.setLayout(jPanelHeaderLayout);

        jLabel1.setText(settings.getString("lCustomers"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabel1.setFont(font);

        Font fontSm = new Font(Font.SANS_SERIF, Font.PLAIN, settings.getInt("fSizeStatus"));
        jLabel2.setFont(fontSm);
        jLabel2.setText(settings.getString("colEditable"));

        Font fontMedium = new Font(Font.SANS_SERIF, Font.PLAIN, settings.getInt("fSizeBigButton"));

        jButton1.setText(settings.getString("bNewCustomer"));
        Colors.setButtonColors(jButton1, Colors.SUCCESS, Colors.WHITE);
        jButton1.setFont(fontMedium);

        jButton2.setText(settings.getString("bAbsent"));
        Colors.setButtonColors(jButton2, Colors.SUCCESS, Colors.WHITE);
        jButton2.setFont(fontMedium);

        jPanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanel3.setLayout(new FlowLayout(FlowLayout.CENTER));

        // Add header label
        jPanel2.add(jLabel1);

        // Add Header Button
        jPanel3.add(jButton1);
        jPanel3.add(jButton2);

        // Add information label and panel
        jPanel4.setLayout(new FlowLayout(FlowLayout.RIGHT));
        jPanel4.add(jLabel2);

        // Add header label panel and header buttons to the top panel
        jPanel1.add(jPanel2);
        jPanel1.add(jPanel3);
        jPanel1.add(jPanel4);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jScrollPane1.setViewportView(jTable1);
        jScrollPane1
                .setPreferredSize(new Dimension(settings.getInt("viewPanelWidth"), settings.getInt("viewPanelHeight")));

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }

    /**
     * Set the panel's table data
     *
     * @param data
     *            Array of all the publications
     * @param regions
     *            Regions that will be displayed in the combo box
     * @param headings
     *            Headings for the data
     */

    public void setTableData(ArrayList<Customer> data, String [] regions, String[] headings) {
        this.customers = data;

        javax.swing.table.DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel(new Object[][] {},
                new String[] {}) {

            Class[] types = new Class[] { java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class};

            boolean[] canEdit = new boolean[] { false, false, true, true, true, true, true, false };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }

        };

        tableModel.setColumnIdentifiers(headings);

        for (int i = 0; i < data.size(); i++) {
            tableModel.addRow(data.get(i).toArray());
        }

        jTable1.setModel(tableModel);

        setComboBoxColumn(regions);

        Helpers.setTableColumnWidth(jTable1, 0, settings.getInt("idColWidth"));

        jTable1.updateUI();
    }

    /**
     * Retrieving the value as string at a given position
     *
     * @param row
     *            The row of the cell
     * @param column
     *            The Column where the cell is located
     * @return value of the cell
     */
    public String getStringAt(int row, int column) {
        return (String) jTable1.getModel().getValueAt(row, column);
    }

    /**
     * Retrieving the value as double at a given position
     *
     * @param row
     *            The row of the cell
     * @param column
     *            The Column where the cell is located
     * @return value of the cell
     */
    public int getIntAt(int row, int column) {
        return (int) jTable1.getModel().getValueAt(row, column);
    }

    /**
     * Get a customer object at a specific row
     * @param row The row of the table
     * @return The customer object
     */
    public Customer getCustomerAt(int row) {
        return this.customers.get(row);
    }

    /**
     * Set the combo boxes for the status column
     * @param comboOptions Array of options for the combo box
     */
    private void setComboBoxColumn(String [] comboOptions) {

        // region combo box
        JComboBox comboBoxRegion = new JComboBox();
        comboBoxRegion.setModel(new DefaultComboBoxModel<>(comboOptions));

        // status combo box
        JComboBox comboBoxStatus = new JComboBox();
        comboBoxStatus.setModel(new DefaultComboBoxModel<>(settings.getStringArray("cusStatus")));

        TableColumn tableColumnRegion = jTable1.getColumnModel().getColumn(5);
        tableColumnRegion.setCellEditor(new DefaultCellEditor(comboBoxRegion));

        TableColumn tableColumnStatus = jTable1.getColumnModel().getColumn(6);
        tableColumnStatus.setCellEditor(new DefaultCellEditor(comboBoxStatus));

        DefaultTableCellRenderer rendererRegion = new DefaultTableCellRenderer();
        tableColumnRegion.setCellRenderer(rendererRegion);

        DefaultTableCellRenderer rendererStatus = new DefaultTableCellRenderer();
        tableColumnStatus.setCellRenderer(rendererStatus);
    }

    /**
     * Attaching table listeners to JTable
     *
     * @param listener
     *            The listener to be attached
     */
    public void attachCustomersTableListener(TableModelListener listener) {
        jTable1.getModel().addTableModelListener(listener);
    }

    /**
     * Attach a listener to the New Customer button
     *
     * @param listener
     *            action listener to execute
     */
    public void attachNewCustomerButtonListener(ActionListener listener) {
        jButton1.addActionListener(listener);
    }

    /**
     * Attach a listener to the Absence Button
     *
     * @param listener
     *            action listener to execute
     */
    public void attachNewAbsenceButtonListener(ActionListener listener) {
        jButton2.addActionListener(listener);
    }

    /**
     * Show the Customers Panel
     *
     * @param data
     *            Array of all the customers for the table
     * @param regions
     *            Regions that will be displayed in the combo boxes
     * @param headings
     *            Headings for the data table
     */
    public void showPanel(ArrayList<Customer> data, String[] regions, String[] headings) {
        setTableData(data, regions, headings);
        view.changePanel(this, view.main.jButtonCustomers);
    }

    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox jComboBoxStatus;
    private javax.swing.JTable jTable1;
}
