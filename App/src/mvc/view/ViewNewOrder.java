package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import dataStructures.Customer;
import dataStructures.Publication;
import mvc.NDSView;
import staticData.Colors;
import utils.Settings;
import utils.SimpleDate;
import utils.Validator;

/**
 * PanelNewOrder displays a form with input fields to create a new order.
 */
public class ViewNewOrder extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    private ArrayList<Customer> customersList;
    private ArrayList<Publication> publicationList;

    boolean isQtyValid = true;

    public ViewNewOrder(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jLabelCustomer = new javax.swing.JLabel();
        jComboBoxCustomer = new javax.swing.JComboBox<>();

        jLabelPublication = new javax.swing.JLabel();
        jComboBoxPublication = new javax.swing.JComboBox<>();

        jLabelQty = new javax.swing.JLabel();
        jTextFieldQty = new javax.swing.JTextField();

        jLabelFrequency = new javax.swing.JLabel();
        jComboBoxFrequency = new javax.swing.JComboBox<>();

        jLabelNextDelivery = new javax.swing.JLabel();
        jButtonNextDelivery = new javax.swing.JButton();

        jButtonOK = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jPanelHeader = new javax.swing.JPanel();
        jPanelForm = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();

        BoxLayout mainLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(mainLayout);

        jLabelHeader.setText(settings.getString("lNewOrder"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabelHeader.setFont(font);

        jPanelHeader.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanelHeader.add(jLabelHeader);

        GridLayout panelLayout = new GridLayout(6, 2);
        panelLayout.setVgap(10);
        panelLayout.setHgap(5);
        jPanelForm.setLayout(panelLayout);

        // customer
        jLabelCustomer.setText(settings.getString("lCustomer"));
        jPanelForm.add(jLabelCustomer);

        jPanelForm.add(jComboBoxCustomer);

        // publication
        jLabelPublication.setText(settings.getString("lPublication"));
        jPanelForm.add(jLabelPublication);

        jPanelForm.add(jComboBoxPublication);

        // qty
        jLabelQty.setText(settings.getString("lQty"));
        jPanelForm.add(jLabelQty);

        jPanelForm.add(jTextFieldQty);

        // attach listeners to the qty field
        jTextFieldQty.addFocusListener(new QuantityFocusListener());
        jTextFieldQty.addActionListener(new QuantityActionListener());
        jTextFieldQty.getDocument().addDocumentListener(new QuantityListener());

        // frequency
        jLabelFrequency.setText(settings.getString("lFreq"));
        jPanelForm.add(jLabelFrequency);

        jPanelForm.add(jComboBoxFrequency);

        // next delivery
        jLabelNextDelivery.setText(settings.getString("lNextDel"));
        jPanelForm.add(jLabelNextDelivery);

        jButtonNextDelivery.setText(new SimpleDate().toString());
        Colors.setButtonColors(jButtonOK, Colors.GRAY, Colors.BLACK);
        jPanelForm.add(jButtonNextDelivery);

        // ok/cancel buttons
        jButtonOK.setText(settings.getString("ok"));
        Colors.setButtonColors(jButtonOK, Colors.SUCCESS, Colors.WHITE);
        jPanelForm.add(jButtonOK);

        jButtonCancel.setText(settings.getString("cancel"));
        Colors.setButtonColors(jButtonCancel, Colors.DANGER, Colors.WHITE);
        jPanelForm.add(jButtonCancel);

        add(jPanelHeader);
        add(jPanelForm);

        setMinimumSize(new Dimension(100, 100));
        setPreferredSize(new Dimension(400, 250));
    }

    /**
     * Attach a listener to the New Order's OK button
     *
     * @param listener The action listener to execute
     */
    public void attachNewOrderOKButtonListener(ActionListener listener) {
        jButtonOK.addActionListener(listener);
    }

    /**
     * Attach a listener to the New Order's CANCEL button
     *
     * @param listener The action listener to execute
     */
    public void attachNewOrderCancelButtonListener(ActionListener listener) {
        jButtonCancel.addActionListener(listener);
    }

    /**
     * Attach a listener to the next delivery date picker
     * @param listener The action listener to execute
     */
    public void attachNewOrderNextDeliveryButtonListener(ActionListener listener) {
        jButtonNextDelivery.addActionListener(listener);
    }

    /**
     * Retrieve order's customer name
     *
     * @return The order's name as a string
     */
    public int getOrderCustomer() {
        int idx = jComboBoxCustomer.getSelectedIndex();
        return this.customersList.get(idx).getID();
    }

    /**
     * Retrieve order's publication
     *
     * @return The order's publication as string
     */
    public int getOrderPublication() {
        int idx = jComboBoxPublication.getSelectedIndex();
        return this.publicationList.get(idx).getID();
    }

    /**
     * Retrieve order's quantity
     *
     * @return The order's quantity as string
     */
    public String getOrderQty() {
        return jTextFieldQty.getText();
    }

    /**
     * Retrieve order's frequency
     *
     * @return The order's frequency as string
     */
    public String getOrderFreq() {
        return (String) jComboBoxFrequency.getSelectedItem();
    }

    /**
     * Retrieve order's next delivery
     *
     * @return The order's next delivery as string
     */
    public String getOrderNextDel() {
        return jButtonNextDelivery.getText();
    }

    /**
     * Set the dropdown (combobox) options in the GUI form for a customer selection
     *
     * @param customersList Dropdown options
     */
    public void setComboBoxCustomer(ArrayList<Customer> customersList) {
        this.customersList = customersList;  // assign the customer list to the class list as well

        String [] options = new String[customersList.size()];

        for (int i = 0; i < options.length; i++) {
            options[i] = customersList.get(i).getName() + "  ID:" + customersList.get(i).getID();
        }
        jComboBoxCustomer.setModel(new javax.swing.DefaultComboBoxModel<>(options));
        jComboBoxCustomer.updateUI();
    }

    /**
     * Set the dropdown (combobox) options in the GUI form for a publication selection
     *
     * @param publicationList Dropdown options
     */
    public void setComboBoxPublication(ArrayList<Publication> publicationList) {
        this.publicationList = publicationList;

        String [] options = new String[publicationList.size()];

        for (int i = 0; i < options.length; i++) {
            options[i] = publicationList.get(i).getTitle() + ", " + publicationList.get(i).getPrice();
        }

        jComboBoxPublication.setModel(new javax.swing.DefaultComboBoxModel<>(options));
        jComboBoxPublication.updateUI();
    }

    /**
     * Set the dropdown (combobox) options in the GUI form for a frequency selection
     *
     * @param options Dropdown options
     */
    public void setComboBoxFrequency(String[] options) {
        jComboBoxFrequency.setModel(new javax.swing.DefaultComboBoxModel<>(options));
        jComboBoxPublication.updateUI();
    }

    /**
     * Set the date picker label
     * @param date The string that will override the default date label
     */
    public void setNextDeliveryButtonText(String date) {
        jButtonNextDelivery.setText(date);
    }


    /**
     * Reset the qty text field if the quantity is incorrect (non numbers, or negative numbers)
     */
    private void checkIfQtyIsValid() {
        if (!isQtyValid) {
            jTextFieldQty.setText("0");
        }
    }

    /**
     * Reset all the fields in the GUI form to blank
     */
    public void clearFields() {
        jTextFieldQty.setText("");
    }

    /**
     * Show the New Order Panel
     */
    public void showPanel(ArrayList<Customer> customers, ArrayList<Publication> publications, String [] frequency) {
        clearFields();
        setComboBoxCustomer(customers);
        setComboBoxPublication(publications);
        setComboBoxFrequency(frequency);
        view.changePanel(this, view.main.jButtonOrders);
    }

    private javax.swing.JComboBox<String> jComboBoxCustomer;
    private javax.swing.JComboBox<String> jComboBoxPublication;
    private javax.swing.JComboBox<String> jComboBoxFrequency;
    private javax.swing.JLabel jLabelCustomer;
    private javax.swing.JLabel jLabelQty;
    private javax.swing.JLabel jLabelPublication;
    private javax.swing.JLabel jLabelFrequency;
    private javax.swing.JLabel jLabelNextDelivery;
    private javax.swing.JTextField jTextFieldQty;
    private javax.swing.JButton jButtonNextDelivery;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelForm;
    private javax.swing.JLabel jLabelHeader;

    /**
     * Check qty field on input
     */
    class QuantityListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            isQtyValid = Validator.isPositiveInteger(jTextFieldQty.getText());
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            //
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            //
        }
    }

    /**
     * Check qty field on enter
     */
    class QuantityActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            checkIfQtyIsValid();
        }
    }

    /**
     * Check qty field on focus loss
     */
    class QuantityFocusListener implements  FocusListener {

        @Override
        public void focusGained(FocusEvent focusEvent) {
            //
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {
            checkIfQtyIsValid();
        }
    }
}
