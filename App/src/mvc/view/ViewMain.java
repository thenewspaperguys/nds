package mvc.view;

import mvc.NDSView;
import staticData.Colors;
import utils.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ViewMain extends JFrame {

    private Settings settings;
    private NDSView view;

    public ViewMain(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
    }

    /**
     * Initialise all components and brings the main window up.
     */
    public void init() {
        setTitle(settings.getString("windowTitle"));
        setMinimumSize(new Dimension(settings.getInt("windowWidth"), settings.getInt("windowHeight")));

        setupUX();
        initComponents();

        setVisible(true);
    }

    /**
     * Try to initialise the "Nimbus" look and feel if possible
     */
    private void setupUX() {
        //////////////////////////////////////////////////////////////////////////
        // Set the UI Look and Feel
        //////////////////////////////////////////////////////////////////////////
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ViewMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ViewMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ViewMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ViewMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Initialise main components of the window: side buttons, menu bar, status bar
     * and main view.
     */
    private void initComponents() {

        //////////////////////////////////////////////////////////////////////////
        // Main UI Setup
        //////////////////////////////////////////////////////////////////////////
        jPanelStatus = new JPanel();
        jPanelSideButtons = new JPanel();
        jButtonCustomers = new JButton();
        jButtonOrders = new JButton();
        jButtonPublications = new JButton();
        jButtonDeliveries = new JButton();
        jButtonDrivers = new JButton();
        jButtonSummary = new JButton();
        jButtonStock = new JButton();
        jButtonInvoices = new JButton();
        jButtonRegions = new JButton();
        jPanelMainView = new JPanel();
        jMenuBar = new JMenuBar();
        jMenuFile = new JMenu();
        jMenuHelp = new JMenu();
        jLabelStatus = new JLabel("Status: OK");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        defaultButtonColor = jButtonCustomers.getBackground();

        //////////////////////////////////////////////////////////////////////////
        // Status Panel
        //////////////////////////////////////////////////////////////////////////
        jPanelStatus.setMaximumSize(new Dimension(100, 100));
        jPanelStatus.setPreferredSize(new Dimension(settings.getInt("statusWidth"), settings.getInt("statusHeight")));

        jPanelStatus.setLayout(new FlowLayout(FlowLayout.RIGHT));

        Font font = new Font(Font.MONOSPACED, Font.BOLD, settings.getInt("fSizeStatus"));
        jLabelStatus.setFont(font);

        jPanelStatus.setBorder(BorderFactory.createBevelBorder(0, Color.GRAY, Color.GRAY));
        jPanelStatus.setBackground(Color.GRAY);

        jPanelStatus.add(jLabelStatus);

        getContentPane().add(jPanelStatus, BorderLayout.PAGE_END);

        //////////////////////////////////////////////////////////////////////////
        // Side Buttons Bar
        //////////////////////////////////////////////////////////////////////////
        int bWidth = settings.getInt("bMainWidth");
        int bHeight = settings.getInt("bMainHeight");

        jButtonCustomers.setText("Customers");
        jButtonCustomers.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonOrders.setText("Orders");
        jButtonOrders.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonPublications.setText("Publications");
        jButtonPublications.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonDeliveries.setText("Deliveries");
        jButtonDeliveries.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonInvoices.setText("Invoices");
        jButtonInvoices.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonSummary.setText("Summary");
        jButtonSummary.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonStock.setText("Stock");
        jButtonStock.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonDrivers.setText("Drivers");
        jButtonDrivers.setMaximumSize(new Dimension(bWidth, bHeight));
        jButtonRegions.setText("Regions");
        jButtonRegions.setMaximumSize(new Dimension(bWidth, bHeight));

        BoxLayout jPanelSideButtonLayout = new BoxLayout(jPanelSideButtons, BoxLayout.Y_AXIS);
        jPanelSideButtons.setLayout(jPanelSideButtonLayout);

        jPanelSideButtons.add(jButtonCustomers);
        jPanelSideButtons.add(jButtonOrders);
        jPanelSideButtons.add(jButtonPublications);
        jPanelSideButtons.add(jButtonDeliveries);
        jPanelSideButtons.add(jButtonInvoices);
        jPanelSideButtons.add(jButtonSummary);
        jPanelSideButtons.add(jButtonStock);
        jPanelSideButtons.add(jButtonDrivers);
        jPanelSideButtons.add(jButtonRegions);

        getContentPane().add(jPanelSideButtons, BorderLayout.LINE_START);

        //////////////////////////////////////////////////////////////////////////
        // Main View
        //////////////////////////////////////////////////////////////////////////
        jPanelMainView.setMinimumSize(new Dimension(700, 100));
        jPanelMainView.setPreferredSize(new Dimension(settings.getInt("mainViewWidth"), settings.getInt("mainViewHeight")));
        getContentPane().add(jPanelMainView, BorderLayout.CENTER);

        //////////////////////////////////////////////////////////////////////////
        // Top Menu
        //////////////////////////////////////////////////////////////////////////
        jMenuFile.setText("File");
        jMenuBar.add(jMenuFile);

        jMenuHelp.setText("Help");
        jMenuBar.add(jMenuHelp);

        setJMenuBar(jMenuBar);

        pack();
    }

    /**
     * Attach a listener to the Customer button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachCustomersButtonListener(ActionListener listener) {
        jButtonCustomers.addActionListener(listener);
    }

    /**
     * Attach a listener to the Orders button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachOrdersButtonListener(ActionListener listener) {
        jButtonOrders.addActionListener(listener);
    }

    /**
     * Attach a listener to the Publications button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachPublicationsButtonListener(ActionListener listener) {
        jButtonPublications.addActionListener(listener);
    }

    /**
     * Attach a listener to the Deliveries button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachDeliveriesButtonListener(ActionListener listener) {
        jButtonDeliveries.addActionListener(listener);
    }

    /**
     * Attach a listener to the Invoices button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachInvoicesButtonListener(ActionListener listener) {
        jButtonInvoices.addActionListener(listener);
    }

    /**
     * Attach a listener to the Summary button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachSummaryButtonListener(ActionListener listener) {
        jButtonSummary.addActionListener(listener);
    }

    /**
     * Attach a listener to the Stock button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachStockButtonListener(ActionListener listener) {
        jButtonStock.addActionListener(listener);
    }

    /**
     * Attach a listener to the Drivers button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachDriversButtonListener(ActionListener listener) {
        jButtonDrivers.addActionListener(listener);
    }

    /**
     * Attach a listener to the Regions button in the side button panel
     *
     * @param listener
     *            The action listener that will be executed
     */
    public void attachRegionsButtonListener(ActionListener listener) {
        jButtonRegions.addActionListener(listener);
    }
    /**
     * Highlight a desired button on the side button panel
     *
     * @param button
     *            The button that will be highlighted
     */
    public void setButtonPressed(JButton button) {
        clearButtonsPressed();
        button.setBackground(Colors.PRIMARY);
        button.setForeground(Color.BLACK);
        Colors.setButtonColors(button, Colors.PRIMARY, Colors.WHITE);
    }

    /**
     * Reset all side button panel buttons to their default colors.
     */
    private void clearButtonsPressed() {
        // Lazy solution
        Colors.setButtonColors(jButtonCustomers, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonOrders, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonPublications, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonDeliveries, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonInvoices, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonSummary, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonStock, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonDrivers, defaultButtonColor, Colors.BLACK);
        Colors.setButtonColors(jButtonRegions, defaultButtonColor, Colors.BLACK);
    }

    public JButton jButtonCustomers;
    public JButton jButtonOrders;
    public JButton jButtonPublications;
    public JButton jButtonInvoices;
    public JButton jButtonDeliveries;
    public JButton jButtonDrivers;
    public JButton jButtonSummary;
    public JButton jButtonStock;
    public JButton jButtonRegions;
    public JMenu jMenuFile;
    public JMenu jMenuHelp;
    public JMenuBar jMenuBar;
    public JPanel jPanelStatus;
    public JPanel jPanelSideButtons;
    public JLabel jLabelStatus;

    public JPanel jPanelMainView;

    // Button Colour
    public Color defaultButtonColor;
}
