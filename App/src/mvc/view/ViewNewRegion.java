package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;

import mvc.NDSView;
import staticData.Colors;
import utils.Settings;

/**
 * PanelNewRegion displays a form with input fields to create a new customer.
 */
public class ViewNewRegion extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewNewRegion(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jLabelName = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();

        jButtonOK = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jPanelHeader = new javax.swing.JPanel();
        jPanelForm = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();

        BoxLayout mainLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(mainLayout);

        jLabelHeader.setText(settings.getString("lNewRegions"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabelHeader.setFont(font);

        jPanelHeader.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanelHeader.add(jLabelHeader);

        GridLayout panelLayout = new GridLayout(2, 2);
        panelLayout.setVgap(10);
        panelLayout.setHgap(5);
        jPanelForm.setLayout(panelLayout);

        jLabelName.setText(settings.getString("lName"));
        jPanelForm.add(jLabelName);

        jPanelForm.add(jTextFieldName);

        jButtonOK.setText(settings.getString("ok"));
        Colors.setButtonColors(jButtonOK, Colors.SUCCESS, Colors.WHITE);
        jPanelForm.add(jButtonOK);

        jButtonCancel.setText(settings.getString("cancel"));
        Colors.setButtonColors(jButtonCancel, Colors.DANGER, Colors.WHITE);
        jPanelForm.add(jButtonCancel);

        add(jPanelHeader);
        add(jPanelForm);

        setMinimumSize(new Dimension(100, 100));
        setPreferredSize(new Dimension(400, 150));
    }

    /**
     * Attach a listener to the New Region's OK button
     *
     * @param listener The action listener to execute
     */
    public void attachNewRegionOKButtonListener(ActionListener listener) {
        jButtonOK.addActionListener(listener);
    }

    /**
     * Attach a listener to the New Region's CANCEL button
     *
     * @param listener The action listener to execute
     */
    public void attachNewRegionCancelButtonListener(ActionListener listener) {
        jButtonCancel.addActionListener(listener);
    }

    /**
     * Retrieve region's name
     *
     * @return The region's name as a string
     */
    public String getRegionName() {
        return jTextFieldName.getText();
    }

    /**
     * Reset all the fields in the GUI form to blank
     */
    public void clearFields() {
        jTextFieldName.setText("");
    }

    /**
     * Show the New Region Panel
     */
    public void showPanel() {
        clearFields();
        view.changePanel(this, view.main.jButtonRegions);
    }

    private javax.swing.JLabel jLabelName;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelForm;
    private javax.swing.JLabel jLabelHeader;
}

