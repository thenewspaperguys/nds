package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BoxLayout;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import mvc.NDSView;
import staticData.Colors;
import utils.Settings;
import utils.Validator;

/**
 * PanelNewPublication displays a form with input fields to create a new publication.
 */
public class ViewNewPublication extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    private boolean isPriceValid = true;
    private boolean isStockValid = true;

    public ViewNewPublication(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jLabelTitle = new javax.swing.JLabel();
        jTextFieldTitle = new javax.swing.JTextField();

        jLabelPrice = new javax.swing.JLabel();
        jTextFieldPrice = new javax.swing.JTextField();

        jLabelStock = new javax.swing.JLabel();
        jTextFieldStock = new javax.swing.JTextField();

        jButtonOK = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jPanelHeader = new javax.swing.JPanel();
        jPanelForm = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();

        BoxLayout mainLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(mainLayout);

        jLabelHeader.setText(settings.getString("lNewPublication"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabelHeader.setFont(font);

        jPanelHeader.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanelHeader.add(jLabelHeader);

        GridLayout panelLayout = new GridLayout(4, 2);
        panelLayout.setVgap(10);
        panelLayout.setHgap(5);
        jPanelForm.setLayout(panelLayout);

        // title
        jLabelTitle.setText(settings.getString("lTitle"));
        jPanelForm.add(jLabelTitle);

        jPanelForm.add(jTextFieldTitle);

        // price
        jLabelPrice.setText(settings.getString("lPrice"));
        jPanelForm.add(jLabelPrice);

        jPanelForm.add(jTextFieldPrice);

        // add listeners to the  price field
        jTextFieldPrice.addFocusListener(new PriceFocusListener());
        jTextFieldPrice.addActionListener(new PriceActionListener());
        jTextFieldPrice.getDocument().addDocumentListener(new PriceListener());

        // initial stock
        jLabelStock.setText(settings.getString("lInitStock"));
        jPanelForm.add(jLabelStock);

        jPanelForm.add(jTextFieldStock);

        // add listeners to the initial stock field
        jTextFieldStock.addFocusListener(new StockFocusListener());
        jTextFieldStock.addActionListener(new StockActionListener());
        jTextFieldStock.getDocument().addDocumentListener(new StockListener());

        // ok / cancel buttons
        jButtonOK.setText(settings.getString("ok"));
        Colors.setButtonColors(jButtonOK, Colors.SUCCESS, Colors.WHITE);
        jPanelForm.add(jButtonOK);

        jButtonCancel.setText(settings.getString("cancel"));
        Colors.setButtonColors(jButtonCancel, Colors.DANGER, Colors.WHITE);
        jPanelForm.add(jButtonCancel);

        add(jPanelHeader);
        add(jPanelForm);

        setMinimumSize(new Dimension(100, 100));
        setPreferredSize(new Dimension(400, 250));
    }

    /**
     * Attach a listener to the New Publication's OK button
     *
     * @param listener The action listener to execute
     */
    public void attachNewPublicationOKButtonListener(ActionListener listener) {
        jButtonOK.addActionListener(listener);
    }

    /**
     * Attach a listener to the New Publication's CANCEL button
     *
     * @param listener The action listener to execute
     */
    public void attachNewPublicationCancelButtonListener(ActionListener listener) {
        jButtonCancel.addActionListener(listener);
    }

    /**
     * Retrieve publication's title
     *
     * @return The publication's title as a string
     */
    public String getPublicationTitle() {
        return jTextFieldTitle.getText();
    }

    /**
     * Retrieve publications's price
     *
     * @return The publications's price as string
     */
    public String getPublicationPrice() {
        return jTextFieldPrice.getText();
    }

    /**
     * Retrieve publication's stock
     *
     * @return The publication's stock as string
     */
    public String getPublicationStock() {
        return jTextFieldStock.getText();
    }

    /**
     * Reset the stock text field if the quantity is incorrect (non numbers, or negative numbers)
     */
    private void checkIfStockIsValid() {
        if (!isStockValid) {
            jTextFieldStock.setText("0");
        }
    }

    /**
     * Reset the price text field if the quantity is incorrect (non numbers, or negative numbers)
     */
    private void checkIfPriceIsValid() {
        if (!isPriceValid) {
            jTextFieldPrice.setText("0");
        }
    }

    /**
     * Reset all the fields in the GUI form to blank
     */
    public void clearFields() {
        jTextFieldTitle.setText("");
        jTextFieldPrice.setText("");
        jTextFieldStock.setText("");
    }

    /**
     * Show the New Publication Panel
     */
    public void showPanel() {
        clearFields();
        view.changePanel(this, view.main.jButtonPublications);
    }

    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JLabel jLabelPrice;
    private javax.swing.JLabel jLabelStock;
    private javax.swing.JTextField jTextFieldTitle;
    private javax.swing.JTextField jTextFieldPrice;
    private javax.swing.JTextField jTextFieldStock;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelForm;
    private javax.swing.JLabel jLabelHeader;

    /**
     * Check qty field on input
     */
    class StockListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            isStockValid = Validator.isPositiveInteger(jTextFieldStock.getText());
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            //
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            //
        }
    }

    /**
     * Check qty field on enter
     */
    class StockActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            checkIfStockIsValid();
        }
    }

    /**
     * Check qty field on focus loss
     */
    class StockFocusListener implements FocusListener {

        @Override
        public void focusGained(FocusEvent focusEvent) {
            //
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {
            checkIfStockIsValid();
        }
    }

    /**
     * Check price field on input
     */
    class PriceListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            isPriceValid = Validator.isPositiveDecimal(jTextFieldPrice.getText());
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            //
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            //
        }
    }

    /**
     * Check price field on enter
     */
    class PriceActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            checkIfPriceIsValid();
        }
    }

    /**
     * Check price field on focus loss
     */
    class PriceFocusListener implements  FocusListener {

        @Override
        public void focusGained(FocusEvent focusEvent) {
            //
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {
            checkIfPriceIsValid();
        }
    }
}
