package mvc.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;

import mvc.NDSView;
import staticData.Colors;
import utils.Settings;

/**
 * PanelNewCustomer displays a form with input fields to create a new customer.
 */
public class ViewNewCustomer extends javax.swing.JPanel {

    Settings settings;
    NDSView view;

    public ViewNewCustomer(NDSView view, Settings settings) {
        this.settings = settings;
        this.view = view;
        initComponents();
    }

    /**
     * Initialise all the panel's components
     */
    private void initComponents() {

        jLabelName = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();
        jLabelAddress = new javax.swing.JLabel();
        jTextFieldAddress = new javax.swing.JTextField();
        jLabelPhoneNumber = new javax.swing.JLabel();
        jTextFieldPhoneNumber = new javax.swing.JTextField();
        jLabelEmail = new javax.swing.JLabel();
        jTextFieldEmail = new javax.swing.JTextField();
        jLabelRegion = new javax.swing.JLabel();
        jComboBoxRegion = new javax.swing.JComboBox<>();
        jButtonOK = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jPanelHeader = new javax.swing.JPanel();
        jPanelForm = new javax.swing.JPanel();
        jLabelHeader = new javax.swing.JLabel();

        BoxLayout mainLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(mainLayout);

        jLabelHeader.setText(settings.getString("lNewCustomer"));
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, settings.getInt("fSizeHeader"));
        jLabelHeader.setFont(font);

        jPanelHeader.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanelHeader.add(jLabelHeader);

        GridLayout panelLayout = new GridLayout(6, 2);
        panelLayout.setVgap(10);
        panelLayout.setHgap(5);
        jPanelForm.setLayout(panelLayout);

        jLabelName.setText(settings.getString("lName"));
        jPanelForm.add(jLabelName);

        jPanelForm.add(jTextFieldName);

        jLabelAddress.setText(settings.getString("lAddress"));
        jPanelForm.add(jLabelAddress);

        jPanelForm.add(jTextFieldAddress);

        jLabelPhoneNumber.setText(settings.getString("lPhone"));
        jPanelForm.add(jLabelPhoneNumber);

        jPanelForm.add(jTextFieldPhoneNumber);

        jLabelEmail.setText(settings.getString("lEmail"));
        jPanelForm.add(jLabelEmail);

        jPanelForm.add(jTextFieldEmail);

        jLabelRegion.setText(settings.getString("lRegion"));
        jPanelForm.add(jLabelRegion);

        jPanelForm.add(jComboBoxRegion);

        jButtonOK.setText(settings.getString("ok"));
        Colors.setButtonColors(jButtonOK, Colors.SUCCESS, Colors.WHITE);
        jPanelForm.add(jButtonOK);

        jButtonCancel.setText(settings.getString("cancel"));
        Colors.setButtonColors(jButtonCancel, Colors.DANGER, Colors.WHITE);
        jPanelForm.add(jButtonCancel);

        add(jPanelHeader);
        add(jPanelForm);

        setMinimumSize(new Dimension(100, 100));
        setPreferredSize(new Dimension(400, 250));
    }

    /**
     * Attach a listener to the New Customer's OK button
     *
     * @param listener The action listener to execute
     */
    public void attachNewCustomersOKButtonListener(ActionListener listener) {
        jButtonOK.addActionListener(listener);
    }

    /**
     * Attach a listener to the New Customer's CANCEL button
     *
     * @param listener The action listener to execute
     */
    public void attachNewCustomersCancelButtonListener(ActionListener listener) {
        jButtonCancel.addActionListener(listener);
    }

    /**
     * Retrieve customer's name
     *
     * @return The customer's name as a string
     */
    public String getCustomerName() {
        return jTextFieldName.getText();
    }

    /**
     * Retrieve customer's address
     *
     * @return The customer's address as string
     */
    public String getCustomerAddress() {
        return jTextFieldAddress.getText();
    }

    /**
     * Retrieve customer's phone number
     *
     * @return The customer's phone number as string
     */
    public String getCustomerPhoneNumber() {
        return jTextFieldPhoneNumber.getText();
    }

    /**
     * Retrieve customer's email
     *
     * @return The customer's email as string
     */
    public String getCustomerEmail() {
        return jTextFieldEmail.getText();
    }

    /**
     * Retrieve customer's region
     *
     * @return The customer's region as string
     */
    public String getCustomerRegion() {
        return (String) jComboBoxRegion.getSelectedItem();
    }

    /**
     * Set the dropdown (combobox) options in the GUI form
     *
     * @param options Dropdown options
     */
    public void setComboBox(String[] options) {
        jComboBoxRegion.setModel(new javax.swing.DefaultComboBoxModel<>(options));
        jComboBoxRegion.updateUI();
    }

    /**
     * Reset all the fields in the GUI form to blank
     */
    public void clearFields() {
        jTextFieldName.setText("");
        jTextFieldAddress.setText("");
        jTextFieldPhoneNumber.setText("");
        jTextFieldEmail.setText("");
    }

    /**
     * Show the New Customer Panel
     *
     * @param regions
     *            List of regions to display in the panel
     */
    public void showPanel(String[] regions) {
        clearFields();
        setComboBox(regions);
        view.changePanel(this, view.main.jButtonCustomers);
    }

    private javax.swing.JComboBox<String> jComboBoxRegion;
    private javax.swing.JLabel jLabelName;
    private javax.swing.JLabel jLabelAddress;
    private javax.swing.JLabel jLabelPhoneNumber;
    private javax.swing.JLabel jLabelEmail;
    private javax.swing.JLabel jLabelRegion;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldAddress;
    private javax.swing.JTextField jTextFieldPhoneNumber;
    private javax.swing.JTextField jTextFieldEmail;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelForm;
    private javax.swing.JLabel jLabelHeader;
}
