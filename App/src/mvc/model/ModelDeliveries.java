package mvc.model;

import dataStructures.*;
import mvc.NDSModel;
import utils.Settings;
import utils.SimpleDate;
import utils.ValidationResult;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelDeliveries {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelDeliveries(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Get all orders for dispatch for today
     *
     * @return A ArrayList of order to dispatch today
     */
    public ArrayList<Order> getOrdersForDispatch() {
        ArrayList<Order> orders = new ArrayList<>();

        String query = "SELECT o.order_id, o.cus_id, o.pub_id, o.qty, o.frequency, o.status as order_status, o.next_delivery,"
                + "c.name as customer_name, c.phone_number, c.address, c.email, c.status as customer_status,"
                + "r.name as region_name," + "p.title, p.price, p.pub_id " + "FROM Order_ as o "
                + "INNER JOIN Customer as c ON o.cus_id=c.cus_id " + "INNER JOIN Publication as p ON o.pub_id=p.pub_id "
                + "INNER JOIN Region as r ON c.region_id=r.region_id "
                + "WHERE (o.status = 'Active' AND c.status = 'Active' AND o.next_delivery <= current_date()) "
                + "ORDER BY r.region_id, c.name;";

        try {
            result = stmt.executeQuery(query);
            while (result.next()) {
                Customer customer = new Customer();
                Publication publication = new Publication();
                Order order = new Order();

                customer.setID(result.getInt("cus_id"));
                customer.setName(result.getString("customer_name"));
                customer.setPhoneNumber(result.getString("phone_number"));
                customer.setAddress(result.getString("address"));
                customer.setEmail(result.getString("email"));
                customer.setStatus(result.getString("customer_status"));
                customer.setRegion(result.getString("region_name"));

                publication.setID(result.getInt("pub_id"));
                publication.setTitle(result.getString("title"));
                publication.setPrice(result.getDouble("price"));

                order.setID(result.getInt("order_id"));
                order.setCustomer(customer);
                order.setPublication(publication);
                order.setFrequency(result.getString("frequency"));
                order.setNextDelivery(new SimpleDate(result.getString("next_delivery")));
                order.setQty(result.getInt("qty"));

                orders.add(order);
            }

            return orders;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Dispatch a set of deliveries for the driver
     *
     * @param deliveryList
     *            The delivery to be dispatched
     * @return Return 0 on error, otherwise the number rows affected
     */
    public int dispatchDeliveries(DeliveryList deliveryList) {
        if (deliveryList == null) {
            return 0;
        } else if (deliveryList.getDeliveries().size() == 0) {
            return 0;
        }

        int driverID = deliveryList.getDriver().getID();
        int querySuccesses = 0;

        for (Delivery delivery : deliveryList.getDeliveries()) {
            try {
                int orderID = delivery.getOrder().getID();
                String frequency = delivery.getOrder().getFrequency();
                String interval;
                String deliveryDate = delivery.getDeliveryDate().toStringDb();

                switch (frequency) {
                case "Day":
                    interval = "1 day";
                    break;
                case "Week":
                    interval = "7 day";
                    break;
                default:
                    interval = "1 month";
                    break;
                }

                // insert data into the delivery table and modify the next
                // delivery date according to the frequency
                String queryCreateDelivery = "INSERT INTO Delivery VALUES (null, " + orderID + ", " + driverID
                        + ", 0, 0, '" + deliveryDate + "');";
                String queryUpdateOrderDate = "UPDATE Order_ as o SET o.next_delivery=date_add(current_date(), interval "
                        + interval + ") WHERE o.order_id = " + orderID + ";";

                int resultCreateDelivery = stmt.executeUpdate(queryCreateDelivery);
                int resultUpdateOrderDate = stmt.executeUpdate(queryUpdateOrderDate);

                if (resultCreateDelivery == 0) {
                    System.out.println("Query Error: " + queryCreateDelivery);
                    return 0;
                }
                if (resultUpdateOrderDate == 0) {
                    System.out.println("Query Error: " + queryUpdateOrderDate);
                    return 0;
                }

                querySuccesses++;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return querySuccesses;
    }

    /**
     * Subtract stock after a delivery
     *
     * @param deliveryList
     *            The list of deliveries
     * @return The amount of successfully modified rows
     */
    public int subtractStockAfterDelivery(DeliveryList deliveryList) {
        if (deliveryList == null) {
            return 0;
        } else if (deliveryList.getDeliveries().size() == 0) {
            return 0;
        }

        int querySuccesses = 0;

        for (Delivery delivery : deliveryList.getDeliveries()) {
            try {
                String pubTitle = delivery.getOrder().getPublication().getTitle();
                int pubQtyDispatched = delivery.getOrder().getQty();

                String queryUpdatePublication = "UPDATE Publication as p SET p.stock=(p.stock - " + pubQtyDispatched
                        + ") WHERE p.title = '" + pubTitle + "'";

                int resultUpdateOrderDate = stmt.executeUpdate(queryUpdatePublication);

                if (resultUpdateOrderDate == 0) {
                    System.out.println("Query Error: " + queryUpdatePublication);
                    return 0;
                }

                querySuccesses++;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;
            }
        }
        return querySuccesses;
    }

    /**
     * Check if a delivery can be dispatched according to the available stock
     *
     * @param deliveryList
     *            List of deliveries
     * @return Any possible publications too low in stock
     */
    public ValidationResult checkStockForDispatch(DeliveryList deliveryList) {
        ValidationResult validationResult = new ValidationResult();
        ArrayList<Publication> publicationsAdded = addDeliveryPublicationsQuantities(deliveryList);

        for (Publication publication : publicationsAdded) {
            String query = "SELECT * FROM Publication WHERE title='" + publication.getTitle() + "';";

            try {
                result = stmt.executeQuery(query);

                if (result.next()) {
                    int dbStock = result.getInt("stock");
                    int requestedStock = publication.getQty();

                    if (requestedStock > dbStock) {
                        validationResult.addError(settings.getString("dispNoStock1") + publication.getTitle()
                                + settings.getString("dispNoStock2") + requestedStock + settings.getString("dispNoStock3")
                                + dbStock);
                    }
                } else {
                    return null;
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage() + ", query : " + query);
                return null;
            }
        }

        return validationResult;
    }

    /**
     * Merge publications quantities for a delivery (to check stock levels)
     *
     * @param deliveryList
     *            List of deliveries
     * @return List of publications with their quantities added together
     */
    public ArrayList<Publication> addDeliveryPublicationsQuantities(DeliveryList deliveryList) {
        ArrayList<Publication> publicationsAdded = new ArrayList<>();

        for (Delivery delivery : deliveryList.getDeliveries()) {
            int foundAtIdx = checkIfPublicationIsInArray(publicationsAdded, delivery.getOrder().getPublication());

            // if the publication has been found then add the quantity
            if (foundAtIdx != -1) {
                int currentQty = publicationsAdded.get(foundAtIdx).getQty();
                int newQty = currentQty + delivery.getOrder().getQty();
                publicationsAdded.get(foundAtIdx).setQty(newQty);

            } else {
                // otherwise add a new publication to the list
                Publication publicationNew = delivery.getOrder().getPublication();
                publicationNew.setQty(delivery.getOrder().getQty());
                publicationsAdded.add(publicationNew);
            }
        }

        return publicationsAdded;
    }

    /**
     * Check if a publication is in array of publications
     *
     * @param publications
     *            The array of publications
     * @param publication
     *            The publication of interest
     * @return The index at which the publication was found, -1 if not found
     */
    private int checkIfPublicationIsInArray(ArrayList<Publication> publications, Publication publication) {
        for (int i = 0; i < publications.size(); i++) {
            // check if the name of the publications are the same
            if (publications.get(i).getTitle().equals(publication.getTitle())) {
                return i; // return the index found
            }
        }
        return -1;
    }

    /**
     * Mark a delivery as completed (delivered to the customer)
     *
     * @param delivery
     *            The delivery to be marked as completed
     * @return Returns 0 on error, 1 on success
     */
    public int setDeliveryAsDelivered(Delivery delivery) {
        if (delivery == null) {
            return 0;
        }
        String query = "UPDATE Delivery SET is_delivered = 1 WHERE delivery_id = " + delivery.getID() + " ;";

        try {
            return stmt.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }
}
