package mvc.model;

import mvc.NDSModel;
import utils.Settings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelStock {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelStock(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Get a list of all the publication that are low on stock
     *
     * @return The list of publications low on stock
     */
    public String[] getLowStockPublications() {
        ArrayList<String> lowStockPublication = new ArrayList<>();

        String query = "SELECT stock, title FROM Publication WHERE (stock > 0 AND stock < 10);";

        try {
            result = stmt.executeQuery(query);
            while (result.next()) {
                String output = result.getString("title");
                output += ", " + result.getString("stock") + " " + settings.getString("lowStock");
                lowStockPublication.add(output);
            }

            return lowStockPublication.toArray(new String[lowStockPublication.size()]);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Get a list of all publications with no stock left
     *
     * @return The list of publications with no stock
     */
    public String[] getNoStockLeftPublications() {
        ArrayList<String> noStockPublications = new ArrayList<>();

        String query = "SELECT title FROM Publication WHERE stock = 0;";

        try {
            result = stmt.executeQuery(query);
            while (result.next()) {
                String output = result.getString("title") + ", " + settings.getString("noStock");
                noStockPublications.add(output);
            }

            return noStockPublications.toArray(new String[noStockPublications.size()]);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Modify the stock levels
     * @param pubID The ID of the publication to modify
     * @param stockAdjustment The stock amount to be added
     * @return Rows modified
     */
    public int modifyStock(int pubID, int stockAdjustment) {

        int result = 0;

        String stm = "UPDATE Publication SET stock = stock + (" + stockAdjustment + ") WHERE pub_id = " + pubID + ";";

        try {
            result = stmt.executeUpdate(stm);
            return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }
}
