package mvc.model;

import dataStructures.Customer;
import dataStructures.Order;
import dataStructures.Publication;
import mvc.NDSModel;
import utils.Settings;
import utils.SimpleDate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelOrder {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelOrder(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * This method is used to add a customer's order
     *
     * @return 1 If successful, 0 if the method fails
     */
    public int insertOrder(Order order) {

        int pubID = order.getPublication().getID();
        int cusID = order.getCustomer().getID();
        int qty = order.getQty();
        String frequency = order.getFrequency();
        String status = "Active";
        String nextDelivery = order.getNextDelivery().toStringDb();

        int result = 0;

        String query = "INSERT INTO Order_ VALUES(null, " + pubID + ", " + cusID + ", " + qty + ", '" + frequency
                + "', '" + status + "', '" + nextDelivery + "')";

        try {
            result = stmt.executeUpdate(query);
            return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }

    }

    /**
     * Get a list of all orders
     *
     * @return List of all the orders
     */
    public ArrayList<Order> getAllOrders() {
        ArrayList<Order> orders = new ArrayList<>();

        String query = "SELECT p.pub_id, p.title, p.price,"
                + "c.cus_id, c.region_id, c.status as cus_status, c.name as cus_name, c.phone_number, c.address, c.email,"
                + "r.name as region_name,"
                + "o.order_id, o.qty, o.frequency, o.status as order_status, o.next_delivery " + "FROM Order_ as o "
                + "INNER JOIN Customer as c ON o.cus_id=c.cus_id "
                + "INNER JOIN Region as r ON r.region_id=c.region_id "
                + "INNER JOIN Publication as p ON  o.pub_id=p.pub_id " + "ORDER BY c.cus_id;";

        try {
            result = stmt.executeQuery(query);

            while (result.next()) {
                Publication publication = new Publication();
                Customer customer = new Customer();
                Order order = new Order();

                publication.setID(result.getInt("pub_id"));
                publication.setTitle(result.getString("title"));
                publication.setPrice(result.getDouble("price"));

                customer.setID(result.getInt("cus_id"));
                customer.setRegion(result.getString("region_name"));
                customer.setStatus(result.getString("cus_status"));
                customer.setName(result.getString("cus_name"));
                customer.setPhoneNumber(result.getString("phone_number"));
                customer.setAddress(result.getString("address"));
                customer.setEmail(result.getString("email"));

                order.setID(result.getInt("order_id"));
                order.setQty(result.getInt("qty"));
                order.setFrequency(result.getString("frequency"));
                order.setStatus(result.getString("order_status"));
                order.setNextDelivery(new SimpleDate(result.getString("next_delivery")));

                order.setCustomer(customer);
                order.setPublication(publication);

                orders.add(order);
            }
            return orders;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Modify an order
     *
     * @return 1 If successful, -1 if the method fails
     */
    public int modifyOrder(int oID, int qty, String frequency, String status) {

        int result = -1;
        if (!frequency.equals("Day") && !frequency.equals("Week") && !frequency.equals("Month")) {
            return -1;
        }

        else if (!status.equals("Active") && !status.equals("Inactive")) {
            return -1;
        }

        else if (qty < 1) {
            return -1;
        } else {

            String modO = "UPDATE Order_ SET qty = (" + qty + "), frequency = ('" + frequency + "'), status = ('"
                    + status + "') where order_id = " + oID + ";";

            try {

                result = stmt.executeUpdate(modO);
                return 1;

            } catch (SQLException sqle) {

                System.out.println("Error: failed to display the database: " + sqle);
                return result;

            }

        }
    }
}
