package mvc.model;

import dataStructures.Absent;
import dataStructures.Customer;
import mvc.NDSModel;
import utils.Settings;
import utils.SimpleDate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelAbsent {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelAbsent(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Insert an absence for a particular customer
     *
     * @param absent
     *            The absence object with From and To dates
     * @param customer
     *            The customer object that will have an absence assigned
     * @return Return 1 if successful and 0 if the method fails
     */
    public int insertAbsence(Absent absent, Customer customer) {
        int result = 0;
        int cusID = customer.getID();
        String from = absent.getFrom().toStringDb();
        String to = absent.getTo().toStringDb();

        boolean isAbsenceInCollision = isAbsenceInCollision(cusID, from, to);

        if (isAbsenceInCollision) {
            return -1;
        }

        String query = "INSERT INTO Absent VALUES (" + cusID + ", '" + from + "', '" + to + "')";

        try {
            result = stmt.executeUpdate(query);
            return result;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }

    /**
     * Check if absence is in collision with any current absence
     *
     * @param cusID
     *            ID of the customer of interest
     * @param from
     *            Absence "from" field
     * @param to
     *            Absence "to" field
     * @return true if there are collisions;
     */
    public boolean isAbsenceInCollision(int cusID, String from, String to) {

        String query = "SELECT COUNT(*) AS collision " + "FROM Absent " + "WHERE cus_id=" + cusID + " " + "AND (('"
                + from + "' BETWEEN absent_from AND absent_to) " + "OR ('" + to
                + "' BETWEEN absent_from AND absent_to)); ";

        try {
            result = stmt.executeQuery(query);
            result.next();
            int collisions = result.getInt("collision");
            return collisions != 0;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return true;
        }
    }

    /**
     * Get all absences for a particular customer
     *
     * @param customerID
     *            The ID of the customer
     * @return A list of all absences for a customer
     */
    public ArrayList<Absent> getCustomersAbsence(int customerID) {
        ArrayList<Absent> customerAbsence = new ArrayList<>();
        String query = "SELECT * FROM Absent WHERE cus_id=" + customerID + ";";

        try {
            result = stmt.executeQuery(query);

            while (result.next()) {
                Absent absent = new Absent();
                String from = result.getString("absent_from");
                String to = result.getString("absent_to");

                absent.setFrom(new SimpleDate(from));
                absent.setTo(new SimpleDate(to));

                customerAbsence.add(absent);
            }
            return customerAbsence;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Delete customer's absence if it collides with today's date
     * @param customerId The id of the customer
     * @return Return the amount of rows affected
     */
    public int deleteCustomerAbsence(int customerId) {

        int result = 0;

        String dateToday = new SimpleDate().toStringDb();

        String query = "DELETE FROM Absent WHERE ('" + dateToday + "' BETWEEN absent_from AND absent_to) AND (cus_id = " + customerId + ");";

        try {
            result = stmt.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
        return result;
    }

    /**
     * Update all customers 'Holiday' statuses
     *
     * @return Return true if the update is successful, false otherwise
     */
    public boolean updateAllCustomerAbsentStatuses() {
        String querySetActive = "UPDATE Customer " + "SET status = 'Active' WHERE status='Holiday';";

        String querySetHolidays = "UPDATE Customer " + "SET status = 'Holiday' " + "WHERE status='Active' AND cus_id IN "
                + "(SELECT cus_id " + "FROM Absent " + "WHERE (now() BETWEEN absent_from AND absent_to) " + ");";

        // run query set all customers to active;
        try {
            int result = stmt.executeUpdate(querySetActive);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }

        // run query set all relevant customers to Holiday
        try {
            int result = stmt.executeUpdate(querySetHolidays);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
}
