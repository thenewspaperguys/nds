package mvc.model;

import dataStructures.Publication;
import mvc.NDSModel;
import utils.Settings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelPublication {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelPublication(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Method receives the Publication object and inserts it into the database
     *
     * @param Publication
     *            The Publication object
     * @return Return 1 if successful, 0 if the method fails
     */

    public int insertPublication(Publication Publication) {
        String title = Publication.getTitle();
        int stock = Publication.getStock();
        double price = Publication.getPrice();

        int result = 0;

        String stm = "INSERT INTO Publication VALUES(null," + stock + ", '" + title + "', " + price + ");";

        try {
            result = stmt.executeUpdate(stm);
            return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }

    /**
     * Method which queries the database for a list of all publications.
     *
     * @return list of publications.
     */
    public ArrayList<Publication> getAllPublications() {

        String stm = "SELECT * FROM Publication";
        ArrayList<Publication> publicationList = new ArrayList<Publication>();

        try {
            result = stmt.executeQuery(stm);
            while (result.next()) {
                Publication publication = new Publication();

                publication.setID(result.getInt("pub_id"));
                publication.setStock(result.getInt("stock"));
                publication.setPrice(result.getDouble("price"));
                publication.setTitle(result.getString("title"));

                publicationList.add(publication);
            }
            return publicationList;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Method which receives the id of the publication which user desires to
     * edit. Inserts the data into a SQL statement and updates the database.
     *
     * @param pubID
     *            the id of the publication.
     *
     * @param price
     *            the price of the publication.
     *
     * @param title
     *            the title of the publication
     *
     * @return if everything is correct it returns a 1, else a 0.
     */
    public int modifyPublication(int pubID, double price, String title) {
        int result = 0;

        if (price <= 0)
            return 0;

        int len = title.length();
        for (int i = 0; i < len; i++) {
            char c = title.charAt(i);
            // Test for all positive cases
            if ('0' <= c && c <= '9')
                return result;
            if ('a' <= c && c <= 'z')
                continue;
            if ('A' <= c && c <= 'Z')
                continue;
            if (c == ' ')
                continue;
            if (c == '-')
                continue;
            else {
                return 0;
            }
        }

        String stm = "UPDATE Publication SET price = " + price + ",title = '" + title + "' WHERE pub_id = " + pubID
                + ";";

        try {
            result = stmt.executeUpdate(stm);
            return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }
}
