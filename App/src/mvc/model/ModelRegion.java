package mvc.model;

import dataStructures.Region;
import mvc.NDSModel;
import utils.Settings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelRegion {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelRegion(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Get a list of all the regions
     *
     * @return The list of all the regions
     */
    public String[] getAllRegions() {
        ArrayList<String> regionsList = new ArrayList<>();

        String query = "SELECT name FROM Region;";

        try {
            result = stmt.executeQuery(query);
            while (result.next()) {
                regionsList.add(result.getString("name"));
            }

            return regionsList.toArray(new String[regionsList.size()]);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Get all regions from the database as Region objects
     * @return The list of objects
     */
    public ArrayList<Region> getAllRegionsObj() {

        ArrayList<Region> regionsList = new ArrayList<>();

        String query = "SELECT * FROM Region;";

        try {
            result = stmt.executeQuery(query);

            while (result.next()) {
                String name = result.getString("name");
                int ID = result.getInt("region_id");

                regionsList.add(new Region(ID, name));
            }

            return regionsList;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Search the region's ID by name
     *
     * @param regionName
     *            The name of the region
     * @return The ID of the region
     */
    public int getRegionIDByName(String regionName) {
        String query = "SELECT region_id FROM Region WHERE name = '" + regionName + "';";

        try {
            result = stmt.executeQuery(query);
            if (result.next()) {
                return result.getInt("region_id");
            } else return 0;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    /**
     * Create a new region
     *
     * @param region
     *            The region object to be created
     * @return result If successful, 0 if the method fails
     */
    public int insertRegion(Region region) {
        String name = region.getName();

        int result = 0;
        String query = "INSERT INTO Region VALUES(null, '" + name + "')";

        try {
            result = stmt.executeUpdate(query);
            return result;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }
}
