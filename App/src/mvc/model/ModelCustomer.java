package mvc.model;

import dataStructures.Absent;
import dataStructures.Customer;
import mvc.NDSModel;
import utils.Settings;
import utils.SimpleDate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelCustomer {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelCustomer(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }
    
    
    /**
     * Method which receives the id of the customer which user desires to
     * edit. Inserts the data into a SQL statement and updates the database.
     * @param cusID the id of the customer
     * @param name Customer's name
     * @param phoneNumber Customer's phone number
     * @param email the email of the customer
     * @param address the address of the customer
     * @return if everything is correct it returns a 1, else a 0.
     */
  
    public int modifyCustomer(int cusID, String name, String phoneNumber, String email, String address, String region, String status) {
        
        int result = 0;

        int regionId = model.region.getRegionIDByName(region);

        String stm = "UPDATE Customer SET name = '" + name + "', address = '" + address + "', email = '" + email +
                "', phone_number = '" + phoneNumber + "', status = '" + status +
                "', region_id = '" + regionId + "' WHERE cus_id = " + cusID
                + ";";
        try {
           result = stmt.executeUpdate(stm);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
        return result;
    }
    
    /**
     * Retrieve a Customer object by id
     * @param id Customer ID
     * @return The customer object
     */
    public Customer getCustomerByID(int id) {

        String getAllCustomers = "select * from Customer where cus_id = " + id + ";";

        try {
            result = stmt.executeQuery(getAllCustomers);

            if (result.next()) {
                Customer customer = new Customer();
                customer.setID(result.getInt("cus_id"));
                customer.setName(result.getString("name"));
                customer.setStatus(result.getString("status"));
                customer.setPhoneNumber(result.getString("phone_number"));
                customer.setAddress(result.getString("address"));
                customer.setEmail(result.getString("email"));
                customer.setRegion(result.getString("region_id"));
                return customer;
            }
            return null;

        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    /**
     * Method receives the Customer object and inserts it into the database
     *
     * @param Customer
     *            The Customer object
     * @return 0 if the method fails, 1 if it is a success
     */
    public int insertCustomer(Customer Customer) {
        String name = Customer.getName();
        String status = Customer.getStatus();
        String phoneNumber = Customer.getPhoneNumber();
        String address = Customer.getAddress();
        String email = Customer.getEmail();

        int regionId = model.region.getRegionIDByName(Customer.getRegion());

        if (regionId == 0) {
            return 0;
        }

        int result = 0;

        String stm = "INSERT INTO Customer VALUES(null, '" + name + "', " + regionId + ", '" + status + "', '"
                + phoneNumber + "', '" + address + "', '" + email + "');";

        try {
            result = stmt.executeUpdate(stm);
            return result;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }

    /**
     * Get all customers
     *
     * @return list of all customers
     */

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customersList = new ArrayList<Customer>();

        String query = "SELECT c.*, r.name as region_name, " + "(SELECT a.absent_from " + "FROM Absent as a "
                + "WHERE (now() BETWEEN absent_from AND absent_to) AND c.cus_id=a.cus_id) as absent_from, "
                + "(SELECT a.absent_to " + "FROM Absent as a "
                + "WHERE (now() BETWEEN absent_from AND absent_to) AND c.cus_id=a.cus_id) as absent_to "
                + "FROM Customer as c " + "INNER JOIN Region AS r ON c.region_id = r.region_id;";

        try {
            result = stmt.executeQuery(query);
            while (result.next()) {
                Customer customer = new Customer();

                customer.setID(result.getInt("cus_id"));
                customer.setName(result.getString("name"));
                customer.setRegion(result.getString("region_name"));
                customer.setStatus(result.getString("status"));
                customer.setPhoneNumber(result.getString("phone_number"));
                customer.setAddress(result.getString("address"));
                customer.setEmail(result.getString("email"));

                String absentTo = result.getString("absent_to");
                String absentFrom = result.getString("absent_from");
                if (customer.getStatus().equalsIgnoreCase("holiday") && absentTo != null && absentFrom != null) {
                    Absent absent = new Absent(new SimpleDate(absentFrom), new SimpleDate(absentTo));
                    customer.setAbsent(absent);
                }

                customersList.add(customer);
            }
            return customersList;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
