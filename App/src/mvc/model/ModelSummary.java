package mvc.model;

import dataStructures.Customer;
import dataStructures.Delivery;
import dataStructures.Order;
import dataStructures.Publication;
import mvc.NDSModel;
import utils.Helpers;
import utils.Settings;
import utils.SimpleDate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelSummary {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelSummary(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Retrieve all deliveries done on a specific date
     *
     * @param summaryDate The date to look for deliveries
     * @return A list of all deliveries made on the specific date
     */
    public ArrayList<Delivery> getDeliveriesForSummary(SimpleDate summaryDate) {
        String date = summaryDate.toStringDb();
        ArrayList<Delivery> deliveryList = new ArrayList<>();

        String query = "SELECT d.delivery_id, d.order_id, d.delivery_date, " +
                "o.cus_id, o.qty, " +
                "p.title, p.price, " +
                "c.name, c.address " +
                "FROM Delivery AS d " +
                "INNER JOIN Order_ AS o ON d.order_id=o.order_id " +
                "INNER JOIN Publication AS p ON o.pub_id=p.pub_id " +
                "INNER JOIN Customer AS c ON o.cus_id=c.cus_id " +
                "WHERE delivery_date = '" + date + "' ORDER BY o.cus_id; ";

        try {

            result = stmt.executeQuery(query);

            while (result.next()) {
                Delivery delivery = new Delivery();
                Order order = new Order();
                Publication publication = new Publication();
                Customer customer = new Customer();

                customer.setName(result.getString("name"));
                customer.setID(result.getInt("cus_id"));
                customer.setAddress(result.getString("address"));

                publication.setTitle(result.getString("title"));
                publication.setPrice(result.getDouble("price"));

                order.setQty(result.getInt("qty"));
                order.setID(result.getInt("order_id"));
                order.setCustomer(customer);
                order.setPublication(publication);

                delivery.setDeliveryDate(new SimpleDate(result.getString("delivery_date")));
                delivery.setID(result.getInt("delivery_id"));
                delivery.setOrder(order);

                deliveryList.add(delivery);
            }

            return deliveryList;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Retrieve all deliveries done on a specific date
     *
     * @param year The year for the report generation
     * @return A list of all deliveries made on the specific date
     */
    public ArrayList<Delivery> getDeliveriesForReport(int year) {
        SimpleDate startOfYear = new SimpleDate(1, 1, year);
        SimpleDate endOfYear = new SimpleDate(31, 12, year);

        ArrayList<Delivery> deliveryList = new ArrayList<>();

        String query = "SELECT d.delivery_id, d.order_id, d.delivery_date, " +
                "o.cus_id, o.qty, " +
                "p.title, p.price, " +
                "c.name, c.address " +
                "FROM Delivery AS d " +
                "INNER JOIN Order_ AS o ON d.order_id=o.order_id " +
                "INNER JOIN Publication AS p ON o.pub_id=p.pub_id " +
                "INNER JOIN Customer AS c ON o.cus_id=c.cus_id " +
                "WHERE delivery_date BETWEEN '" + startOfYear.toStringDb() +
                "' AND '" + endOfYear.toStringDb() + "' ORDER BY o.cus_id;";

        try {

            result = stmt.executeQuery(query);

            while (result.next()) {
                Delivery delivery = new Delivery();
                Order order = new Order();
                Publication publication = new Publication();
                Customer customer = new Customer();

                customer.setName(result.getString("name"));
                customer.setID(result.getInt("cus_id"));
                customer.setAddress(result.getString("address"));

                publication.setTitle(result.getString("title"));
                publication.setPrice(result.getDouble("price"));

                order.setQty(result.getInt("qty"));
                order.setID(result.getInt("order_id"));
                order.setCustomer(customer);
                order.setPublication(publication);

                delivery.setDeliveryDate(new SimpleDate(result.getString("delivery_date")));
                delivery.setID(result.getInt("delivery_id"));
                delivery.setOrder(order);

                deliveryList.add(delivery);
            }

            return mergeDeliveriesForReport(deliveryList);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Merge the publication quantities in the report
     * @param deliveries The list of deliveries for the year
     * @return The merged list of deliveries
     */
    private ArrayList<Delivery> mergeDeliveriesForReport(ArrayList<Delivery> deliveries) {
        ArrayList<Delivery> mergedDeliveries = new ArrayList<>();

        for (Delivery delivery : deliveries) {
            addPublicationQtyIfInArray(delivery, mergedDeliveries);
        }

        return mergedDeliveries;
    }

    /**
     * Add a publication to the array if it does not exist, otherwise add its quantity
     * @param targetDelivery The delivery that needs to be checked for a double entry
     * @param deliveries The list of deliveries
     */
    private void addPublicationQtyIfInArray(Delivery targetDelivery, ArrayList<Delivery> deliveries) {

        String targetPublicationTitle = targetDelivery.getOrder().getPublication().getTitle();
        int targetCustomerId = targetDelivery.getOrder().getCustomer().getID();

        int qty = targetDelivery.getOrder().getQty();

        for (Delivery delivery : deliveries) {

            if (delivery.getOrder().getPublication().getTitle().equalsIgnoreCase(targetPublicationTitle)
                    && delivery.getOrder().getCustomer().getID() == targetCustomerId) {
                // add the qty to the existing publication;
                int foundQty = delivery.getOrder().getQty();
                delivery.getOrder().setQty(foundQty + qty);
                return;
            }
        }

        // delivery has not been found in the array, then just add it
        deliveries.add(targetDelivery);
    }
}
