package mvc.model;

import dataStructures.Driver;
import mvc.NDSModel;
import utils.Settings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelDriver {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelDriver(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Create a new driver
     *
     * @param driver
     *            The driver object to be created
     * @return result If successful, 0 if the method fails
     */
    public int insertDriver(Driver driver) {
        String name = driver.getName();
        String phone_number = driver.getPhoneNumber();

        int result = 0;
        String query = "INSERT INTO Driver VALUES(null, '" + name + "', '" + phone_number + "')";

        try {
            result = stmt.executeUpdate(query);
            return result;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }

    /**
     * Get a list of all the drivers
     *
     * @return List of all the drivers
     */
    public ArrayList<Driver> getAllDrivers() {
        ArrayList<Driver> drivers = new ArrayList<>();
        String query = "SELECT * from Driver";

        try {
            result = stmt.executeQuery(query);
            while (result.next()) {
                Driver driver = new Driver();

                driver.setID(result.getInt("driver_id"));
                driver.setName(result.getString("name"));
                driver.setPhoneNumber(result.getString("phone_number"));

                drivers.add(driver);
            }
            return drivers;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }


    /**
     * Modify driver details
     * @param driverID The ID of the driver
     * @param name The new name of the drivers
     * @param phoneNumber The new phone number of the driver
     * @return The number of rows affected
     */
    public int modifyDriver(int driverID, String name, String phoneNumber) {

        int result = 0;

        String stm = "UPDATE Driver SET name = '" + name + "', phone_number = '" + phoneNumber + "' WHERE driver_id = " + driverID + ";";

        try {
            result = stmt.executeUpdate(stm);
            return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return result;
        }
    }
}
