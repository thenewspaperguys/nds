package mvc.model;

import dataStructures.Customer;
import dataStructures.DeliveryList;
import dataStructures.Invoice;
import dataStructures.Publication;
import mvc.NDSModel;
import utils.Helpers;
import utils.Settings;
import utils.SimpleDate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ModelInvoice {

    private NDSModel model;
    private Settings settings;

    private Statement stmt;
    private ResultSet result;


    public ModelInvoice(NDSModel model, Settings settings, Statement stmt) {
        this.model = model;
        this.settings = settings;
        this.stmt = stmt;
    }

    /**
     * Get all customer IDs for invoice generation (all customers that need have Invoices due)
     * @return List of Customer IDs
     */
    private ArrayList<Integer> getAllCustomerIDsForInvoice() {
        ArrayList<Integer> customerIDs = new ArrayList<>();

        String getAllNonBilledCustomers = "select Customer.cus_id " + "from Customer "
                + "inner join Order_ on Customer.cus_id = Order_.cus_id "
                + "inner join Delivery on Order_.order_id = Delivery.order_id " + "where Delivery.is_billed = 0 "
                + "group by Customer.cus_id;";

        try {

            result = stmt.executeQuery(getAllNonBilledCustomers);
            while (result.next()) {
                System.out.println(result.getInt("cus_id"));
                customerIDs.add(result.getInt("cus_id"));
            }
            return customerIDs;

        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    /**
     * Add all publication that got delivered to the customer since the last Invoice
     * @param id Customer if
     * @return List of publications
     */
    private ArrayList<Publication> addPublicationsToInvoice(int id) {
        ArrayList<Publication> publications = new ArrayList<>();

        String getAllNonBilledDelivery = "select Publication.pub_id, Publication.stock, Publication.title, Publication.price, Order_.qty "
                + "from Publication " + "inner join Order_ on Publication.pub_id = Order_.pub_id "
                + "inner join Delivery on Order_.order_id = Delivery.order_id "
                + "inner join Customer on Order_.cus_id = Customer.cus_id " + "where Customer.cus_id = " + id
                + " and Delivery.is_billed=0;";

        try {
            result = stmt.executeQuery(getAllNonBilledDelivery);

            while (result.next()) {
                Publication publication = new Publication();

                publication.setID(result.getInt("pub_id"));
                publication.setTitle(result.getString("title"));
                publication.setPrice(result.getInt("price"));
                publication.setQty(result.getInt("qty"));
                publication.setStock(result.getInt("stock"));

                publications.add(publication);
            }
            return publications;

        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    /**
     * Update all deliveries for a particular customer to "billed"
     * @param id The id of the customer
     * @return 0 if the method fails, 1 if it succeeds
     */
    public int updateIsBilled(int id) {
        ArrayList<Integer>deliveryID = new ArrayList<>();

        String getDeliveryID = "select Delivery.delivery_id from Delivery "
                + "inner join Order_ on Delivery.order_id = Order_.order_id "
                + "inner join Customer on Order_.cus_id = Customer.cus_id " + "where Customer.cus_id = " + id + ";";

        try {
            result = stmt.executeQuery(getDeliveryID);

            while(result.next()) {
                deliveryID.add(result.getInt("delivery_id"));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        for (int i = 0; i < deliveryID.size(); i++) {
            String updateIsBilled = "Update Delivery set is_billed = 1 where delivery_id = " + deliveryID.get(i) + ";";

            try {
                int resultQuery = stmt.executeUpdate(updateIsBilled);

                if (resultQuery == 0) {
                    System.out.println("Error on updating is billed where id = "+deliveryID.get(i)+"");
                    return 0;
                }
            } catch (SQLException e) {
                System.out.println(e);
                return 0;
            }
        }
        return 1;
    }

    /**
     * Create all invoices since the last Invoice generation (1 month by default)
     * @return 0 If the method fails, 1 if it succeeds
     */
    public int createAllInvoices() {
        SimpleDate now = new SimpleDate();
        SimpleDate previousDate = new SimpleDate();
        String getDate = "Select * from InvoiceDate order by invoice_date desc limit 1;";
        try {
            result = stmt.executeQuery(getDate);
            if (result.next()) {
                previousDate = new SimpleDate(result.getString("invoice_date"));
            }
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        }

        //Check if a month or more has passed
        if (now.compareTo(previousDate) >= 0) {

            //Get All Customer IDs
            ArrayList<Integer> customerID = getAllCustomerIDsForInvoice();

            //Iterate through all of the customer IDs
            for (int i = 0; i < customerID.size(); i++) {
                Invoice invoice = new Invoice();

                Customer customer = this.model.customer.getCustomerByID(customerID.get(i));
                invoice.setCustomer(customer);

                ArrayList<Publication> pub = addPublicationsToInvoice(customerID.get(i));
                invoice.setPublications(pub);

                invoice.setPriceCorrection(0);
                invoice.setDeliveryDate(now);

                int resultInvoice = createInvoice(invoice);
                if (resultInvoice == 0) {
                    System.out.println("Error with create Invoice");
                    return resultInvoice;
                }

                int isBilledUpdate = updateIsBilled(customerID.get(i));
                if (isBilledUpdate == 0) {
                    System.out.println("Error with updateIsBilled");
                    return isBilledUpdate;
                }
            }
            now.setMonth(now.getMonth() + 1);
            if (now.getMonth() == 13) {
                now.setMonth(1);
            }
            String setNextMonth = "Update InvoiceDate set invoice_date = '" + now.toStringDb()
                    + "' where invoice_date = '" + previousDate.toStringDb() + "';";
            int resultUpdate = 0;

            try {
                resultUpdate = stmt.executeUpdate(setNextMonth);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (resultUpdate == 0) {
                System.out.println("Error with updating the month");
                return resultUpdate;
            }
            return 1;
        } else {
            return 0;
        }

    }

    /**
     * This method searches for Invoices that have been previously created
     *
     * @param ID
     *            is the ID of the Invoice
     * @return type of Invoice
     */
    public Invoice getInvoiceByID(int ID) {
        Invoice invoice = new Invoice();

        String query = "SELECT i.*, c.*, r.name as region_name " + "FROM Invoice as i "
                + "INNER JOIN Customer as c ON i.cus_id=c.cus_id "
                + "INNER JOIN Region as r ON c.region_id=r.region_id " + "WHERE i.inv_id=" + ID + ";";

        // Get the invoice
        try {
            result = stmt.executeQuery(query);
            result.next();

            // make blank objects
            Customer customer = new Customer();

            // add invoice data
            invoice.setID(result.getInt("inv_id"));
            invoice.setDeliveryDate(new SimpleDate(result.getString("delivery_date")));
            invoice.setPriceCorrection(result.getDouble("price_correction"));

            // add customer data
            customer.setID(result.getInt("cus_id"));
            customer.setName(result.getString("name"));
            customer.setStatus(result.getString("status"));
            customer.setPhoneNumber(result.getString("phone_number"));
            customer.setAddress(result.getString("address"));
            customer.setEmail(result.getString("email"));
            customer.setRegion(result.getString("region_name"));

            // add customer to the invoice
            invoice.setCustomer(customer);

        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }

        // add publications to the invoice

        // Set publications for every invoice
        try {
            ArrayList<Publication> publicationsPerInvoice = new ArrayList<>();

            query = "SELECT i.inv_id, i.pub_id, i.qty," + "p.title, p.price " + "FROM InvDetail as i "
                    + "INNER JOIN Publication as p ON i.pub_id=p.pub_id " + "WHERE i.inv_id=" + ID + ";";

            result = stmt.executeQuery(query);

            // add each publication to the publicationPerInvoice list
            while (result.next()) {
                Publication publication = new Publication();

                publication.setID(result.getInt("pub_id"));
                publication.setTitle(result.getString("title"));
                publication.setPrice(result.getInt("price"));
                publication.setQty(result.getInt("qty"));

                publicationsPerInvoice.add(publication);
            }

            // set the publications list in the invoice
            invoice.setPublications(publicationsPerInvoice);

            return invoice;

        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    /**
     * Get all Invoices for a specific customer name
     * @param name Customer's name
     * @return Customer's Invoices
     */
    public ArrayList<Invoice> getAllInvoices(String name) {
        ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
        ArrayList<Publication> pub = new ArrayList<Publication>();
        Customer customer = new Customer();
        Invoice invoice = new Invoice();
        Publication publication = new Publication();

        String getAllInvoiceQuery = "Select * from Invoice, Customer where Invoice.cus_id = Customer.cus_id and Customer.name like '"
                + name.charAt(0) + "%" + name.charAt(name.length() - 1) + "';";

        try {
            result = stmt.executeQuery(getAllInvoiceQuery);
            while (result.next()) {

                invoice.setID(result.getInt("inv_id"));
                invoice.setDeliveryDate(new SimpleDate(result.getString("delivery_date")));
                invoice.setPriceCorrection(result.getDouble("price_correction"));

                customer.setID(result.getInt("cus_id"));
                customer.setName(result.getString("name"));
                customer.setPhoneNumber(result.getString("phone_number"));
                customer.setAddress(result.getString("address"));
                customer.setRegion(result.getString("region_id"));
                customer.setStatus(result.getString("status"));
                customer.setEmail(result.getString("email"));

                invoice.setCustomer(customer);

                String getAllPublicationsQuery = "select publication.pub_id, publication.stock, publication.title, publication.price from InvDetail, publication, customer, invoice where invoice.cus_id = customer.cus_id and invoice.inv_id = invdetail.inv_id and invdetail.pub_id = publication.pub_id and customer.name like '"
                        + name.charAt(0) + "%" + name.charAt(name.length() - 1) + "'; ";
                result = stmt.executeQuery(getAllPublicationsQuery);
                while (result.next()) {
                    publication.setID(result.getInt("pub_id"));
                    publication.setPrice(result.getDouble("price"));
                    publication.setStock(result.getInt("stock"));
                    publication.setTitle(result.getString("title"));

                    pub.add(publication);
                }
                invoice.setPublications(pub);
                invoiceList.add(invoice);
            }

            return invoiceList;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    /**
     * Get all invoices for all customers
     * @return List of all invoices
     */
    public ArrayList<Invoice> getAllInvoices() {
        ArrayList<Invoice> allInvoices = new ArrayList<>();
        String query = "SELECT i.*, c.*, r.name as region_name " + "FROM Invoice as i "
                + "INNER JOIN Customer as c ON i.cus_id=c.cus_id "
                + "INNER JOIN Region as r ON c.region_id=r.region_id;";

        // Get a list of all invoices
        try {
            result = stmt.executeQuery(query);

            while (result.next()) {
                // make blank objects
                Invoice invoice = new Invoice();
                Customer customer = new Customer();

                // add invoice data
                invoice.setID(result.getInt("inv_id"));
                invoice.setDeliveryDate(new SimpleDate(result.getString("delivery_date")));
                invoice.setPriceCorrection(result.getDouble("price_correction"));

                // add customer data
                customer.setID(result.getInt("cus_id"));
                customer.setName(result.getString("name"));
                customer.setStatus(result.getString("status"));
                customer.setPhoneNumber(result.getString("phone_number"));
                customer.setAddress(result.getString("address"));
                customer.setEmail(result.getString("email"));
                customer.setRegion(result.getString("region_name"));

                // add customer to the invoice
                invoice.setCustomer(customer);

                // add invoice to the array
                allInvoices.add(invoice);
            }

        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }

        // Set publications for every invoice
        try {
            // Iterate through each of the invoices
            for (int i = 0; i < allInvoices.size(); i++) {

                ArrayList<Publication> publicationsPerInvoice = new ArrayList<>();

                query = "SELECT i.inv_id, i.pub_id, i.qty," + "p.title, p.price " + "FROM InvDetail as i "
                        + "INNER JOIN Publication as p ON i.pub_id=p.pub_id " + "WHERE i.inv_id="
                        + allInvoices.get(i).getID() + ";";

                result = stmt.executeQuery(query);

                // add each publication to the publicationPerInvoice list
                while (result.next()) {
                    Publication publication = new Publication();

                    publication.setID(result.getInt("pub_id"));
                    publication.setTitle(result.getString("title"));
                    publication.setPrice(result.getInt("price"));
                    publication.setQty(result.getInt("qty"));

                    publicationsPerInvoice.add(publication);
                }

                // set the publications list in the invoice
                allInvoices.get(i).setPublications(publicationsPerInvoice);
            }

            return allInvoices;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    /**
     * This method creates Invoices
     *
     * @param invoice
     *            is the object type that is being passed into to the method
     */
    public int createInvoice(Invoice invoice) {
        if (invoice.getID() != 0) {
            return 0;
        }
        String createInvoiceQuery = "INSERT INTO Invoice VALUES ( null, " + invoice.getCustomer().getID() + ", 0, '"
                + invoice.getDeliveryDate().toStringDb() + "', 0);";
        try {
            int resultCreateInvoice = stmt.executeUpdate(createInvoiceQuery);
            if (resultCreateInvoice == 0) {
                System.out.println("Query Error: " + createInvoiceQuery);
                return 0;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        String getLastInvoice = "Select Invoice.inv_id from Invoice order by Invoice.inv_id desc limit 1;";
        try {
            result = stmt.executeQuery(getLastInvoice);
            result.next();

            ArrayList<Publication> publication = invoice.getPublications();
            int invID = result.getInt("inv_id");
            for (Publication pub : publication) {
                String createInvDetail = "INSERT INTO InvDetail VALUES ( " + invID + ", " + pub.getID() + ", "
                        + pub.getQty() + ");";
                int resultCreateInvDetail = stmt.executeUpdate(createInvDetail);
                if (resultCreateInvDetail == 0) {
                    System.out.println("Error: " + createInvDetail);
                    return 0;
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return 1;

    }

    /**
     * Get all invoices for a specific region from a given delivery list
     *
     * @param deliveryList
     *            The delivery list for a region
     * @return The list of Invoices
     */
    public ArrayList<Invoice> getNonDeliveredInvoicesForRegion(DeliveryList deliveryList) {
        ArrayList<Integer> distinctCustomerIDs = Helpers.getDistinctCustomersFromDeliveries(deliveryList.getDeliveries());
        ArrayList<Invoice> invoices = new ArrayList<>();

        for (Integer customerID : distinctCustomerIDs) {
            // get all invoices for the customer (if any)
            ArrayList<Integer> invoiceIDs = findNonDeliveredInvoicesForCustomer(customerID);
            if (invoiceIDs == null)
                continue;

            if (invoiceIDs.size() > 0) {
                // add each invoice for the customer to the list of all invoices
                for (Integer invoiceID : invoiceIDs) {
                    invoices.add(getInvoiceByID(invoiceID));
                }
            }
        }
        return invoices;
    }

    /**
     * Find all invoices for a specific customer
     *
     * @param cusID
     *            The customer of interest
     * @return The list of invoice IDs for the customer
     */
    private ArrayList<Integer> findNonDeliveredInvoicesForCustomer(int cusID) {
        ArrayList<Integer> invoices = new ArrayList<>();

        String query = "SELECT i.inv_id FROM Invoice as i WHERE i.is_delivered=0 AND i.cus_id=" + cusID + ";";

        try {
            result = stmt.executeQuery(query);
            while (result.next()) {
                int invID = result.getInt("inv_id");
                invoices.add(invID);
            }
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return invoices;
    }

    /**
     * Set Invoices as delivered
     *
     * @param invoices
     *            List of invoices that will be tagged as delivered
     * @return Amount of successful queries, 0 if even one fails
     */
    public int setDeliveredInvoicesAsDelivered(ArrayList<Invoice> invoices) {
        int querySuccesses = 0;
        String date = new SimpleDate().toStringDb();

        for (Invoice invoice : invoices) {
            String query = "UPDATE Invoice " + "SET is_delivered=1, delivery_date='" + date + "' "
                    + "WHERE Invoice.inv_id=" + invoice.getID() + ";";
            try {
                int result = stmt.executeUpdate(query);
                if (result != 0)
                    querySuccesses++;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;
            }
        }
        return querySuccesses;
    }
}
