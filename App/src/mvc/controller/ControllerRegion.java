package mvc.controller;

import dataStructures.Region;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Settings;
import utils.ValidationLib;
import utils.ValidationResult;

import java.util.ArrayList;

public class ControllerRegion {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;
    private ValidationLib validationLib;


    public ControllerRegion(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.validationLib = new ValidationLib();
    }


    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
        attachRegionPanelButtonListener();
        attachNewRegionButtonListeners();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachRegionsButtonListener((e) -> {
            showRegionsPanel();
        });
    }

    /**
     * Attach listeners to Regions Panel buttons
     */
    private void attachRegionPanelButtonListener() {
        view.region.attachNewRegionButtonListener((e) -> {
            view.newRegion.showPanel();
        });
    }

    /**
     * Attach listeners to creating a new region and to cancelling the creation of a
     * new region
     */
    private void attachNewRegionButtonListeners() {
        // New Driver's Cancel Button
        view.newRegion.attachNewRegionCancelButtonListener((e) -> {
            showRegionsPanel();
        });

        // New Driver's OK Button
        view.newRegion.attachNewRegionOKButtonListener((e) -> {
            Region region = new Region();

            region.setName(view.newRegion.getRegionName());

            ValidationResult errors = validationLib.validateRegion(region);

            if (!errors.isValid()) {
                view.dialog.okDialog(errors.toString());
            } else {
                int result = model.region.insertRegion(region);

                if (result == 0) {
                    view.status.set(settings.getString("regionCreateErr"));
                } else {
                    view.status.set(settings.getString("regionCreateOK"), 5000);

                    showRegionsPanel();
                }
            }
        });
    }
    /**
     * Show the regions panel
     */
    public void showRegionsPanel() {

        ArrayList<Region> regions = model.region.getAllRegionsObj();

        if (regions == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.region.showPanel(regions, settings.getStringArray("headingRegions"));
        }
    }
}
