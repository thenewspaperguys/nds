package mvc.controller;

import dataStructures.Driver;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Settings;
import utils.ValidationLib;
import utils.ValidationResult;

import java.util.ArrayList;

/**
 * Drivers Controller
 */
public class ControllerDriver {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;
    private ValidationLib validationLib;

    public ControllerDriver(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.validationLib = new ValidationLib();
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
        attachDriverPanelButtonListener();
        attachNewDriverButtonListeners();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachDriversButtonListener((e) -> {
            showDriversPanel();
        });
    }

    /**
     * Attach listeners to Driver Panel buttons
     */
    private void attachDriverPanelButtonListener() {
        view.driver.attachNewDriverButtonListener((e) -> {
            view.newDriver.showPanel();
        });
    }

    /**
     * Attach listeners to creating a new driver and to cancelling the creation of a
     * new driver
     */
    private void attachNewDriverButtonListeners() {
        // New Driver's Cancel Button
        view.newDriver.attachNewDriverCancelButtonListener((e) -> {
            showDriversPanel();
        });

        // New Driver's OK Button
        view.newDriver.attachNewDriverOKButtonListener((e) -> {
            Driver driver = new Driver();

            driver.setName(view.newDriver.getDriverName());
            driver.setPhoneNumber(view.newDriver.getDriverPhoneNumber());

            ValidationResult errors = validationLib.validateDriver(driver);

            if (!errors.isValid()) {
                view.dialog.okDialog(errors.toString());
            } else {
                int result = model.driver.insertDriver(driver);

                if (result == 0) {
                    view.status.set(settings.getString("drvCreateErr"));
                } else {
                    view.status.set(settings.getString("drvCreateOK"), 5000);

                    showDriversPanel();
                }
            }
        });
    }

    /**
     * Attach a listener to Driver table table
     */
    private void attachDriverTableListener() {
        view.driver.attachDriversTableListener(tableModelEvent -> {

            int row = tableModelEvent.getFirstRow();
            int driverId = view.driver.getIntAt(row, 0);
            String name = view.driver.getStringAt(row, 1);
            String phoneNumber = view.driver.getStringAt(row, 2);

            // create driver obj
            Driver driver = new Driver(driverId, name, phoneNumber);

            // validate driver
            ValidationResult validationResult = validationLib.validateDriver(driver);

            if (!validationResult.isValid()) {
                // if the driver is not valid
                view.dialog.okDialog(validationResult.toString());
            } else {

                // confirm modification
                if (view.dialog.okCancelDialog(settings.getString("confirm"))) {

                    int result = model.driver.modifyDriver(driverId, name, phoneNumber);

                    if (result == 0) {
                        view.status.set(settings.getString("drvModifyErr"));
                    } else {
                        view.status.set(settings.getString("drvModifyOK"), 5000);

                        showDriversPanel();
                    }
                }
            }
        });
    }

    /**
     * Show drivers panel
     */
    public void showDriversPanel() {
        ArrayList<Driver> drivers = model.driver.getAllDrivers();

        if (drivers == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.driver.showPanel(drivers, settings.getStringArray("headingDrivers"));
            attachDriverTableListener();
        }
    }
}
