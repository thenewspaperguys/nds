package mvc.controller;

import dataStructures.Customer;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Settings;
import utils.ValidationLib;
import utils.ValidationResult;

import java.util.ArrayList;

/**
 * Customer Controller
 */
public class ControllerCustomer {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;
    private ValidationLib validationLib;

    public ControllerCustomer(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.validationLib = new ValidationLib();
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
        attachCustomerPanelButtonListeners();
        attachNewCustomerPanelButtonListeners();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachCustomersButtonListener((e) -> {
            showCustomersPanel();
        });
    }

    /**
     * Attach listeners to the Customer panel buttons
     */
    private void attachCustomerPanelButtonListeners() {
        // New Customer Button
        view.customer.attachNewCustomerButtonListener((e) -> {
            String[] regions = model.region.getAllRegions();
            if (regions != null) {
                view.newCustomer.showPanel(regions);
            } else {
                view.status.set(settings.getString("dbError"), 5000);
            }
        });

        // New Absence button
        view.customer.attachNewAbsenceButtonListener((e) -> {
            controller.absent.showNewAbsencePanel();
        });
    }

    /**
     * Attach listeners to creating a new customer and to cancelling the creation of
     * a new customer
     */
    private void attachNewCustomerPanelButtonListeners() {
        // New Customer's Cancel Button
        view.newCustomer.attachNewCustomersCancelButtonListener((e) -> {
            showCustomersPanel();
        });

        // New Customer's OK Button
        view.newCustomer.attachNewCustomersOKButtonListener((e) -> {
            Customer customer = new Customer();

            customer.setName(view.newCustomer.getCustomerName());
            customer.setAddress(view.newCustomer.getCustomerAddress());
            customer.setPhoneNumber(view.newCustomer.getCustomerPhoneNumber());
            customer.setEmail(view.newCustomer.getCustomerEmail());
            customer.setRegion(view.newCustomer.getCustomerRegion());
            customer.setStatus("Active");

            ValidationResult errors = validationLib.validateCustomer(customer);

            if (!errors.isValid()) {
                view.dialog.okDialog(errors.toString());
            } else {
                int result = model.customer.insertCustomer(customer);

                if (result == 0) {
                    view.status.set(settings.getString("cusCreateErr"));
                } else {
                    view.status.set(settings.getString("cusCreateOK"), 5000);
                    showCustomersPanel();
                }
            }
        });
    }

    /**
     * Attach a listener to Customers table
     */

    private void attachCustomersTableListener() {

        view.customer.attachCustomersTableListener(tableModelEvent -> {

            // retrieve data from the table
            int row = tableModelEvent.getFirstRow();
            int cusID = view.customer.getIntAt(row, 0);
            String name = view.customer.getStringAt(row, 1);
            String phoneNumber = view.customer.getStringAt(row, 2);
            String email = view.customer.getStringAt(row, 3);
            String address = view.customer.getStringAt(row, 4);
            String region = view.customer.getStringAt(row, 5);
            String status = view.customer.getStringAt(row, 6);

            String currentStatus = view.customer.getCustomerAt(row).getStatus();

            // if the status
            if (!validateStatusUpdate(status, currentStatus, cusID)) {
                return;
            }

            // create customer
            Customer customer = new Customer(cusID, name, phoneNumber, email, address, region, status, null);

            // validate customer
            ValidationResult validationResult = validationLib.validateCustomer(customer);

            if (!validationResult.isValid()) {
                // if the customer is not valid, show errors dialog
                view.dialog.okDialog(validationResult.toString());
            } else {
                // confirm customer
                if (view.dialog.okCancelDialog(settings.getString("confirm"))) {
                    // run the sql query
                    int result = model.customer.modifyCustomer(cusID, name, phoneNumber, email, address, region, status);

                    if (result == 0) {
                        // error in the database
                        view.status.set(settings.getString("cusModifyErr"), 5000);
                    } else {
                        // all ok
                        view.status.set(settings.getString("cusModifyOK"), 5000);
                        controller.taskRunner.taskRunnerCheckAbsence();
                        showCustomersPanel();
                    }
                }
            }
        });
    }

    /**
     * Show customers panel
     */
    public void showCustomersPanel() {
        ArrayList<Customer> customers = model.customer.getAllCustomers();
        String[] regions = model.region.getAllRegions();

        if (customers == null || regions == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.customer.showPanel(customers, regions, settings.getStringArray("headingCustomers"));
            attachCustomersTableListener();
        }
    }

    /**
     * Check if updating the customer status is possible
     * @param selectedStatus Selected status by the user
     * @param currentStatus Current status of the customer
     * @return True if the status is supposed to be updates, false otherwise
     */
    private boolean validateStatusUpdate(String selectedStatus, String currentStatus, int customerId) {

        // inactive is always a possibility
        if (selectedStatus.equalsIgnoreCase("inactive")) {
            return true;
        }

        // to holiday from active
        if (selectedStatus.equalsIgnoreCase("holiday") && currentStatus.equalsIgnoreCase("active")) {
            if (view.dialog.okCancelDialog(settings.getString("dialogAddHoliday"))) {
                controller.absent.showNewAbsencePanel(customerId);
            }
            return false;
        }

        // to holiday from inactive
        if (selectedStatus.equalsIgnoreCase("holiday") && currentStatus.equalsIgnoreCase("inactive")) {
            view.dialog.okDialog(settings.getString("dialogHolidayFromInactive"));
            return false;
        }

        // to active from holiday
        if (selectedStatus.equalsIgnoreCase("active") && currentStatus.equalsIgnoreCase("holiday")) {
            if (view.dialog.okCancelDialog(settings.getString("dialogRemoveHoliday"))) {
                // delete overlapping holiday if it exists
                deleteCustomerAbsence(customerId);
            }
            return false;
        }

        // to active from active, and to active from inactive
        // to holiday from holiday
        return true;
    }

    private void deleteCustomerAbsence(int customerID) {
        int result = model.absent.deleteCustomerAbsence(customerID);

        if (result != 0) {
            view.status.set(settings.getString("absDeleted"));
        } else {
            view.status.set(settings.getString("dbError"));
        }
    }
}
