package mvc.controller;

import dataStructures.Delivery;
import mvc.view.ViewPrintService;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.*;

import java.util.ArrayList;

/**
 * Summary Controller
 */
public class ControllerSummary {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;

    public ControllerSummary(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
        attachSummaryPanelButtonListeners();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachSummaryButtonListener((e) -> {
            showSummaryPanel();
        });
    }

    /**
     * Attach listeners to the Summary panel buttons
     */
    private void attachSummaryPanelButtonListeners() {

        // Date picker button
        view.summary.attachSummaryDateButtonListener((e) -> {
            SimpleDate simpleDate = view.dialog.datePicker();
            view.summary.setJButtonDate(simpleDate.toString());
        });

        // Show summary button
        view.summary.attachShowSummaryButtonListener((e) -> {
            // grab the selected date
            SimpleDate simpleDate = view.summary.getSelectedDate();

            // look for deliveries for the specified date
            ArrayList<Delivery> allDeliveries = model.summary.getDeliveriesForSummary(simpleDate);

            if (allDeliveries == null) {
                // db error
                view.status.set(settings.getString("dbError"), 5000);

            } else if (allDeliveries.size() == 0)  {
                // no results
                String msg = settings.getString("noSummary") + simpleDate.toString();
                view.dialog.okDialog(msg);

            } else {
                // print the summary on the screen
                PrintingService printingService = new PrintingService(allDeliveries, settings);
                String sumTitle = settings.getString("lSummary") + " " + simpleDate.toString();
                ViewPrintService framePrintService = new ViewPrintService(printingService.toString(), sumTitle);
            }
        });

        // Show annual report button
        view.summary.attachShowReportButtonListener(e -> {
            int year = view.summary.getSelectedYear();

            // Run a threaded report
            ThreadedReport report = new ThreadedReport(model, view, controller, settings, year, "Report Thread");
            report.run();
        });
    }

    /**
     * Show summary panel
     */
    public void showSummaryPanel() {
        view.summary.showPanel(Helpers.createYearsArray());
    }
}
