package mvc.controller;

import dataStructures.DeliveryList;
import dataStructures.Driver;
import dataStructures.Invoice;
import dataStructures.Order;
import mvc.view.ViewPrintService;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.PrintingService;
import utils.Settings;
import utils.ValidationLib;
import utils.ValidationResult;

import java.util.ArrayList;

/**
 * Deliveries Controller
 */
public class ControllerDeliveries {
    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;
    private ValidationLib validationLib;

    public ControllerDeliveries(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.validationLib = new ValidationLib();
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachDeliveriesButtonListener((e) -> {
                showDeliveriesPanel();
            });
    }

    /**
     * Attach listeners to each region's deliveries buttons
     */
    private void attachDeliveriesDispatchPanelButtonListeners() {

        for (int i = 0; i < view.deliveries.jButtonDispatchRegion.size(); i++) {
            int finalI = i;
            view.deliveries.jButtonDispatchRegion.get(i).addActionListener(actionEvent -> {
                    // grab the deliveries assigned to the region clicked
                    DeliveryList deliveryList = view.deliveries.getDeliveryList(finalI);

                    // show status
                    view.status.set(settings.getString("dispatchingInProgress"));

                    if (checkStock(deliveryList)) {

                        if (dispatchAndSubtractStock(deliveryList)) {
                            PrintingService printingService = new PrintingService(settings);

                            printDeliveryDocument(deliveryList, printingService);
                            printInvoices(deliveryList, printingService);

                            showDeliveriesPanel(); // reset the panel
                        }
                    }
                });
        }
    }

    /**
     * Check for stock availability
     * @param deliveryList List of deliveries
     * @return True on successful check
     */
    private boolean checkStock(DeliveryList deliveryList) {
        boolean isSuccess = false;
        ValidationResult stockCheck = model.deliveries.checkStockForDispatch(deliveryList);

        if (stockCheck == null) {
            // db error, display message
            view.status.set(settings.getString("dispatchingErr"), 5000);

        } else if (!stockCheck.isValid()) {
            // low on stock, display message
            view.status.set(settings.getString("dispatchingErr"), 5000);
            view.dialog.okDialog(stockCheck.toString());
        } else {
            isSuccess = true;
        }

        return isSuccess;
    }

    /**
     * Dispatch orders and subtract the stock
     * @param deliveryList The list of deliveries
     * @return True if successful
     */
    private boolean dispatchAndSubtractStock(DeliveryList deliveryList) {
        boolean isSuccess = false;

        // dispatch deliveries
        int deliveriesToBeDispatched = deliveryList.getDeliveries().size();
        int deliveriesDispatched = model.deliveries.dispatchDeliveries(deliveryList);

        // subtract stock
        int publicationsModified = model.deliveries.subtractStockAfterDelivery(deliveryList);

        // if dispatch and subtraction is ok
        if (deliveriesToBeDispatched == deliveriesDispatched && publicationsModified != 0) {
            view.status.set(settings.getString("dispatchingOK"), 5000);

            isSuccess = true;
        } else {
            view.status.set(settings.getString("dispatchingErr"), 5000);
        }

        return isSuccess;
    }

    /**
     * Print Delivery document on screen
     * @param deliveryList The list of deliveries
     * @param printingService Printing service
     */
    private void printDeliveryDocument(DeliveryList deliveryList, PrintingService printingService) {
        printingService = new PrintingService(deliveryList, settings);
        String delTitle = settings.getString("lRegionHeader") + deliveryList.getDeliveries().get(0).getOrder().getCustomer().getRegion();

        new ViewPrintService(printingService.toString(), delTitle);
    }

    /**
     * Print available invoices for a particular region (region extracted from the delivery list)
     * @param deliveryList The delivery list
     * @param printingService Printing service
     */
    private void printInvoices(DeliveryList deliveryList, PrintingService printingService) {
        // check for available invoices for the region's customers
        ArrayList<Invoice> invoices = model.invoices.getNonDeliveredInvoicesForRegion(deliveryList);
        if (invoices.size() > 0) {
            // set invoices as delivered
            int result = model.invoices.setDeliveredInvoicesAsDelivered(invoices);

            if (result != invoices.size()) {
                view.status.set(settings.getString("dbError"), 5000);
            }

            // print invoices
            for (Invoice invoice : invoices) {
                printingService = new PrintingService(invoice, settings);
                String invTitle = settings.getString("lInvDetailID") + invoice.getID();
                new ViewPrintService(printingService.toString(), invTitle);
            }
        }
    }

    /**
     * Show deliveries to dispatch today panel
     */
    public void showDeliveriesPanel() {
        controller.stock.showLowStockDialog();
        controller.stock.showNoStockDialog();

        ArrayList<Order> orders = model.deliveries.getOrdersForDispatch();
        ArrayList<Driver> drivers = model.driver.getAllDrivers();

        if (orders == null || drivers == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.deliveries.showPanel(orders, drivers);

            attachDeliveriesDispatchPanelButtonListeners();
        }
    }
}
