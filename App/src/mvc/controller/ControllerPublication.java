package mvc.controller;

import dataStructures.Publication;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Casters;
import utils.Settings;
import utils.ValidationLib;
import utils.ValidationResult;

import java.util.ArrayList;

/**
 * Publication Controller
 */
public class ControllerPublication {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;
    private ValidationLib validationLib;

    public ControllerPublication(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.validationLib = new ValidationLib();
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
        attachNewPublicationButtonListeners();
        attachPublicationsPanelButtonListeners();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachPublicationsButtonListener((e) -> {
            showPublicationsPanel();
        });
    }

    /**
     * Attach listeners to the Publications panel buttons
     */
    private void attachPublicationsPanelButtonListeners() {
        // New Publication
        view.publication.attachNewPublicationButtonListener((e) -> {
            view.newPublication.showPanel();
        });
    }

    /**
     * Attach listeners to creating a new publication and to cancelling the creation
     * of a new publication
     */
    private void attachNewPublicationButtonListeners() {
        // New Customer's Cancel Button
        view.newPublication.attachNewPublicationCancelButtonListener((e) -> {
            showPublicationsPanel();
        });

        // New Customer's OK Button
        view.newPublication.attachNewPublicationOKButtonListener((e) -> {
            Publication publication = new Publication();

            int stock = Casters.safeToInt(view.newPublication.getPublicationStock());
            double price = Casters.safeToDouble(view.newPublication.getPublicationPrice());

            publication.setPrice(price);
            publication.setStock(stock);
            publication.setTitle(view.newPublication.getPublicationTitle());

            ValidationResult errors = validationLib.validatePublication(publication);

            if (!errors.isValid()) {
                view.dialog.okDialog(errors.toString());
            } else {
                int result = model.publication.insertPublication(publication);

                if (result == 0) {
                    view.status.set(settings.getString("pubCreateErr"));
                } else {
                    view.status.set(settings.getString("pubCreateOK"), 5000);

                    showPublicationsPanel();
                }
            }
        });
    }

    /**
     * Attach a listener to Publications table
     */
    private void attachPublicationsTableListener() {
        view.publication.attachPublicationTableListener(tableModelEvent -> {

            // retrieve data from the table
            int row = tableModelEvent.getFirstRow();
            String title = view.publication.getStringAt(row, 1);
            double price = view.publication.getDoubleAt(row, 2);
            int pubID = view.publication.getIntAt(row, 0);

            // create publication
            Publication publication = new Publication(pubID, 0, 0, title, price);

            // validate publication
            ValidationResult validationResult = validationLib.validatePublication(publication);

            if (!validationResult.isValid())  {
                // if the publication is not valid, show errors dialog
                view.dialog.okDialog(validationResult.toString());
            } else {
                // confirm modification
                if (view.dialog.okCancelDialog(settings.getString("confirm"))) {
                    // run the sql query
                    int result = model.publication.modifyPublication(pubID, price, title);

                    if (result == 0) {
                        // error in the database
                        view.status.set(settings.getString("pubModifyError"), 5000);
                    } else {
                        // all ok
                        view.status.set(settings.getString("pubModifyOK"), 5000);
                        showPublicationsPanel();
                    }
                }
            }
        });
    }

    /**
     * Show publications panel
     */
    public void showPublicationsPanel() {
        ArrayList<Publication> publications = model.publication.getAllPublications();

        if (publications == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.publication.showPanel(publications, settings.getStringArray("headingPublications"));
            attachPublicationsTableListener();
        }
    }
}
