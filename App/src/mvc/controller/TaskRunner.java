package mvc.controller;

import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Settings;

import javax.swing.Timer;
import java.awt.event.ActionListener;

/**
 * Wrapper class around the timer that repeats tasks at an interval
 */
public class TaskRunner {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;

    private Timer timer;

    public TaskRunner(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
    }

    /**
     * Set a timer to repeat an action every set interval in milliseconds
     * @param timerInterval The amount of time between actions in milliseconds
     * @param actionListener The action to be performed
     */
    private void setTimer(int timerInterval, ActionListener actionListener) {
        timer = new Timer(timerInterval, actionListener);
        timer.setInitialDelay(1000); // wait before kicking in
        timer.start();
    }

    /**
     * Stop the timer
     */
    public void stopTaskRunner() {
        timer.stop();
    }

    /**
     * Start the task runner
     */
    public void startTaskRunner(int minutes) {
        int runEvery = minutes * 60000; // run every x minutes

        setTimer(runEvery, (e) -> {
            // task 1 update all customers Holiday statuses
            taskRunnerCheckAbsence();
            // task 2 create new invoices
            taskRunnerCheckNewInvoices();
        });
    }

    ////////////////////////////////////////////////////////////////////
    // Tasks
    ////////////////////////////////////////////////////////////////////
    /**
     * Update customer absence status periodically
     */
    public void taskRunnerCheckAbsence() {
        boolean updateSuccess = model.absent.updateAllCustomerAbsentStatuses();

        if (updateSuccess) {
            view.status.set(settings.getString("absUpdate"), 5000);
        } else {
            view.status.set(settings.getString("absError"));
        }
    }

    /**
     * Check for new invoices periodically
     */
    public void taskRunnerCheckNewInvoices() {
        int createAllInvoices = model.invoices.createAllInvoices();

        if (createAllInvoices == 0) { view.status.set(settings.getString("invNoNew"), 5000);
        } else {
            view.status.set(settings.getString("invUpdate"), 5000);
        }
    }
}
