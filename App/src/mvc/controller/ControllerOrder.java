package mvc.controller;

import dataStructures.Customer;
import dataStructures.Order;
import dataStructures.Publication;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.*;

import java.util.ArrayList;

/**
 * Orders Controller
 */
public class ControllerOrder {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;
    private ValidationLib validationLib;

    public ControllerOrder(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.validationLib = new ValidationLib();
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
        attachOrdersPanelButtonListeners();
        attachNewOrderButtonListeners();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachOrdersButtonListener((e) -> {
            showOrdersPanel();
        });
    }

    /**
     * Attach listeners to the Order panel buttons
     */
    private void attachOrdersPanelButtonListeners() {
        view.order.attachNewOrderButtonListener((e) -> {
            ArrayList<Customer> customers = model.customer.getAllCustomers();
            ArrayList<Publication> publications = model.publication.getAllPublications();

            if (customers == null || publications == null) {
                view.status.set(settings.getString("dbError"));
            } else {
                view.newOrder.showPanel(customers, publications, settings.getStringArray("ordFrequency"));
            }
        });
    }

    /**
     * Attach listeners to creating a new order and to cancelling the creation of a
     * new order
     */
    private void attachNewOrderButtonListeners() {
        // New Order's Cancel Button
        view.newOrder.attachNewOrderCancelButtonListener((tableModelEvent) -> {
            showOrdersPanel();
        });

        // New Order's OK Button
        view.newOrder.attachNewOrderOKButtonListener((tableModelEvent) -> {
            Order order = new Order();
            int qty = Casters.safeToInt(view.newOrder.getOrderQty());

            order.setQty(qty);
            order.setPublication(new Publication(view.newOrder.getOrderPublication()));
            order.setCustomer(new Customer(view.newOrder.getOrderCustomer()));
            order.setFrequency(view.newOrder.getOrderFreq());
            order.setNextDelivery(new SimpleDate(view.newOrder.getOrderNextDel()));

            ValidationResult errors = validationLib.validateOrder(order);

            if (!errors.isValid()) {
                view.dialog.okDialog(errors.toString());
            } else {
                int result = model.order.insertOrder(order);

                if (result == 0) {
                    view.status.set(settings.getString("ordCreateErr"));
                } else {
                    view.status.set(settings.getString("ordCreateOK"), 5000);

                    showOrdersPanel();
                }
            }
        });

        // Select next delivery
        view.newOrder.attachNewOrderNextDeliveryButtonListener((e) -> {
            SimpleDate simpleDate = view.dialog.datePicker();
            view.newOrder.setNextDeliveryButtonText(simpleDate.toString());
        });
    }

    /**
     * Attach a listener to Orders  table
     */
    private void attachOrderTableListener() {
        view.order.attachOrderTableListener(tableModelEvent -> {
            int row = tableModelEvent.getFirstRow();
            int oID = view.order.getIntAt(row, 0);
            int qty = view.order.getIntAt(row, 3);
            String frequency = view.order.getStringAt(row, 4);
            String status = view.order.getStringAt(row, 5);

            // confirm modification
            if (view.dialog.okCancelDialog(settings.getString("confirm"))) {
                int result = model.order.modifyOrder(oID, qty, frequency, status);

                if (result == -1) {
                    view.status.set(settings.getString("ordModifyErr"), 5000);
                } else {
                    view.status.set(settings.getString("ordModifyOK"), 5000);
                    showOrdersPanel();
                }
            }
        });
    }

    /**
     * Show orders panel
     */
    public void showOrdersPanel() {
        ArrayList<Order> orders = model.order.getAllOrders();

        if (orders == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.order.showPanel(orders, settings.getStringArray("headingOrders"));
            attachOrderTableListener();
        }
    }
}
