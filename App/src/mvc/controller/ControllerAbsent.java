package mvc.controller;

import dataStructures.Absent;
import dataStructures.Customer;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Settings;
import utils.SimpleDate;
import utils.ValidationLib;

import java.util.ArrayList;

/**
 * Absent Controller
 */
public class ControllerAbsent {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;
    private ValidationLib validationLib;

    public ControllerAbsent(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
        this.validationLib = new ValidationLib();
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachNewAbsenceButtonListeners();
    }

    /**
     * Attach listeners to the New Absence panel
     */
    private void attachNewAbsenceButtonListeners() {

        // New Absence dropdown customer picker
        view.absent.attachComboBoxActionListener((e) -> {
            int customerID = view.absent.getAbsentCustomerID();
            ArrayList<Absent> allCustomerAbsence = model.absent.getCustomersAbsence(customerID);

            view.absent.setTableData(allCustomerAbsence);
        });

        // "From" absence button listener
        view.absent.attachFromAbsenceButtonListener((e) -> {
            SimpleDate simpleDate = view.dialog.datePicker();
            view.absent.setFromAbsence(simpleDate);
        });

        // "To" absence button listener
        view.absent.attachToAbsenceButtonListener((e) -> {
            SimpleDate simpleDate = view.dialog.datePicker();
            view.absent.setToAbsence(simpleDate);
        });

        // Create new absence button listener
        view.absent.attachNewAbsenceButtonListener((e) -> {
            Absent absent = view.absent.getSelectedAbsence();
            Customer customer = view.absent.getCurrentlySelectedAbsentCustomer();

            if (absent == null) {
                // no dates were selected
                view.dialog.okDialog(settings.getString("eSelectDate"));
            } else if (!absent.isFirstBeforeSecond()) {
                // the first date must be smaller or the same as the second date
                view.dialog.okDialog(settings.getString("eFirstSmaller"));
            } else {
                // date selected correctly, insert the data
                int result = model.absent.insertAbsence(absent, customer);

                if (result == -1) {
                    // the date already exists or it overlaps
                    view.dialog.okDialog(settings.getString("eDateCollision"));
                } else if (result == 0) {
                    // tdb error
                    view.status.set(settings.getString("dbError"));
                } else {
                    // update absence statuses
                    controller.taskRunner.taskRunnerCheckAbsence();
                    // success
                    refreshAbsenceTable(customer.getID());
                }
            }
        });
    }

    /**
     * Show absence panel
     */
    public void showNewAbsencePanel() {
        ArrayList<Customer> customers = model.customer.getAllCustomers();

        if (customers != null) {
            view.absent.showPanel(customers, settings.getStringArray("headingAbsent"));

            // show the first customer on load
            int customerID = view.absent.getAbsentCustomerID();
            ArrayList<Absent> allCustomerAbsence = model.absent.getCustomersAbsence(customerID);

            view.absent.setTableData(allCustomerAbsence);

        } else {
            view.status.set(settings.getString("dbError"), 5000);
        }
    }

    /**
     * Show new absence panel for a particular customer
     * @param customerID The customer ID
     */
    public void showNewAbsencePanel(int customerID) {
        ArrayList<Customer> customers = model.customer.getAllCustomers();

        if (customers != null) {
            view.absent.showPanel(customers, settings.getStringArray("headingAbsent"));

            ArrayList<Absent> allCustomerAbsence = model.absent.getCustomersAbsence(customerID);

            view.absent.setTableData(allCustomerAbsence);
            view.absent.setComboBoxSelection(customerID);

        } else {
            view.status.set(settings.getString("dbError"), 5000);
        }
    }

    /**
     * Reload the absence table
     * @param customerID ID of the customer
     */
    private void refreshAbsenceTable(int customerID) {
        ArrayList<Absent> allCustomerAbsence = model.absent.getCustomersAbsence(customerID);

        view.absent.setTableData(allCustomerAbsence);
    }
}
