package mvc.controller;

import dataStructures.Invoice;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Settings;

import java.util.ArrayList;

/**
 * Invoices Controller
 */
public class ControllerInvoices {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;

    public ControllerInvoices(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachInvoicesButtonListener((e) -> {
            showInvoicesPanel();
        });
    }

    /**
     * Show invoices panel
     */
    public void showInvoicesPanel() {
        ArrayList<Invoice> invoices = model.invoices.getAllInvoices();

        if (invoices == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.invoices.showPanel(invoices);
        }
    }
}
