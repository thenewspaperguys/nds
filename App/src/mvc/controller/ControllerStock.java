package mvc.controller;

import dataStructures.Publication;
import mvc.NDSController;
import mvc.NDSModel;
import mvc.NDSView;
import utils.Settings;

import java.util.ArrayList;

/**
 * Stock Controller
 */
public class ControllerStock {

    private NDSModel model;
    private NDSView view;
    private NDSController controller;
    private Settings settings;

    public ControllerStock(NDSModel model, NDSView view, NDSController controller, Settings settings) {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.settings = settings;
    }

    /**
     * Initialise listeners
     */
    public void init() {
        attachMainWindowButtonListener();
    }

    /**
     * Attach listener to the main window side panel button
     */
    private void attachMainWindowButtonListener() {
        // Main Menu Button
        view.main.attachStockButtonListener((e) -> {
            showStockPanel();
        });
    }

    /**
     * Attach a listener to the stock table
     */
    private void attachStockTableListener() {
        view.stock.attachStockTableListener(tableModelEvent -> {

            int row = tableModelEvent.getFirstRow();
            int stockAdjustment = view.stock.getValueAt(row, 3);
            int pubID = view.stock.getValueAt(row, 0);

            // confirm modification
            if (view.dialog.okCancelDialog(settings.getString("confirm"))) {

                int result = model.stock.modifyStock(pubID, stockAdjustment);

                if (result == 0) {
                    view.status.set(settings.getString("stockCreateErr"));
                } else {
                    view.status.set(settings.getString("stockCreateOK"), 5000);

                    showStockPanel();
                }
            }
        });
    }

    /**
     * Show stock panel
     */
    public void showStockPanel() {
        ArrayList<Publication> publications = model.publication.getAllPublications();

        if (publications == null) {
            view.status.set(settings.getString("dbError"), 5000);
        } else {
            view.stock.showPanel(publications, settings.getStringArray("headingStocks"));

            attachStockTableListener();
        }
    }

    /**
     * Show a low stock dialog
     */
    public void showLowStockDialog() {
        String[] lowStock = model.stock.getLowStockPublications();
        view.dialog.fromStringArray(lowStock, settings.getString("lowStockPopup"));
    }

    /**
     * Show no stock left dialog
     */
    public void showNoStockDialog() {
        String[] noStock = model.stock.getNoStockLeftPublications();
        view.dialog.fromStringArray(noStock, settings.getString("noStockPopup"));
    }
}
