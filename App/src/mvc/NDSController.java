package mvc;

import mvc.controller.*;
import utils.*;

/**
 * MVC's Controller Class Main processing engine and business logic
 */

public class NDSController {

    /**
     * Default constructor
     */
    public NDSController(Settings settings) {
        this.settings = settings;
    }

    /**
     * Initialise the view and the model
     */
    public void init() {
        // initialise view
        initView();
        // initialise model
        initModel();
        // initialise sub controllers
        initSubClasses();
        // initialise task runner
        initTaskRunner();

        // show orders panel on startup
        order.showOrdersPanel();
    }

    /**
     * Initialise View
     */
    private void initView() {
        view = new NDSView(this.settings);
        view.init();
    }

    /**
     * Initialise Model
     */
    private void initModel() {
        model = new NDSModel(this.settings);
        if (model.init()) {
            view.status.set(settings.getString("dbOk"), 5000);
        } else {
            view.status.set(settings.getString("dbError"));
        }
    }

    /**
     * Initialise sub controller classes
     */
    private void initSubClasses() {
        customer = new ControllerCustomer(model, view, this, settings);
        customer.init();

        absent = new ControllerAbsent(model, view, this, settings);
        absent.init();

        publication = new ControllerPublication(model, view, this, settings);
        publication.init();

        order = new ControllerOrder(model, view, this, settings);
        order.init();

        driver = new ControllerDriver(model, view, this, settings);
        driver.init();

        invoices = new ControllerInvoices(model, view, this, settings);
        invoices.init();

        deliveries = new ControllerDeliveries(model, view, this, settings);
        deliveries.init();

        summary = new ControllerSummary(model, view, this, settings);
        summary.init();

        stock = new ControllerStock(model, view, this, settings);
        stock.init();

        region = new ControllerRegion(model, view, this, settings);
        region.init();
    }

    /**
     * Initialise the Task Runner
     */
    private void initTaskRunner() {
        taskRunner = new TaskRunner(model, view, this, settings);
        taskRunner.startTaskRunner(60);
    }

    private NDSView view;
    private NDSModel model;
    final private Settings settings;

    public ControllerCustomer customer;
    public ControllerPublication publication;
    public ControllerOrder order;
    public ControllerAbsent absent;
    public ControllerDriver driver;
    public ControllerInvoices invoices;
    public ControllerDeliveries deliveries;
    public ControllerSummary summary;
    public ControllerStock stock;
    public ControllerRegion region;

    public TaskRunner taskRunner;
}
