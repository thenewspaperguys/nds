package mvc;


import java.sql.*;

import mvc.model.*;
import utils.Settings;

/**
 * MVC's Model Class
 * Initialises connection to MySQL or MariaDB database
 */
public class NDSModel {

    /**
     * Default constructor.
     */
    public NDSModel(Settings settings) {
        this.settings = settings;
    }

    /**
     * Parametrised constructor
     *
     * @param user
     *            Username to the database
     * @param pass
     *            Password to the database
     */
    public NDSModel(String user, String pass) {
        init(user, pass);
    }

    /**
     * Initialise sub model classes
     */
    private void initSubClasses() {
        order = new ModelOrder(this, settings, stmt);

        driver = new ModelDriver(this, settings, stmt);

        customer = new ModelCustomer(this, settings, stmt);

        publication = new ModelPublication(this, settings, stmt);

        invoices = new ModelInvoice(this, settings, stmt);

        deliveries = new ModelDeliveries(this, settings, stmt);

        absent = new ModelAbsent(this, settings, stmt);

        region = new ModelRegion(this, settings, stmt);

        stock = new ModelStock(this, settings, stmt);

        summary = new ModelSummary(this, settings, stmt);
    }

    /**
     * Initialise the database connection
     *
     * @param user
     *            Username to the database
     * @param pass
     *            Password to the database
     */
    public boolean init(String user, String pass) {
        boolean init = createConnection(user, pass);
        initSubClasses();

        return init;
    }

    public boolean init() {
        boolean init = createConnection("root", "root") || createConnection("root", "admin");
        initSubClasses();

        return init;
    }

    /**
     * Attempt to create an SQL DB connection
     *
     * @param user
     *            The username to the database
     * @param pass
     *            The password to the database
     * @return True if the connection is successful, otherwise false
     */
    private boolean createConnection(String user, String pass) {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost/nds", user, pass);
            stmt = con.createStatement();
            return true;
        } catch (Exception err) {
            System.out.println("Error: Failed to connect to database\n" + err.getMessage());
            return false;
        }
    }

    /**
     * Close the database connection
     */
    public void closeDb() {

        try {
            con.close();

        } catch (SQLException sqle) {
            System.out.println("Error: failed to close the database\n" + sqle.getMessage());
        }
    }

    private Connection con;
    private Statement stmt;

    private Settings settings;

    public ModelOrder order;
    public ModelDriver driver;
    public ModelCustomer customer;
    public ModelPublication publication;
    public ModelInvoice invoices;
    public ModelDeliveries deliveries;
    public ModelAbsent absent;
    public ModelRegion region;
    public ModelStock stock;
    public ModelSummary summary;
}
