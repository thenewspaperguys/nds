package mvc;
/**
 * MVC's View Class. Display data information and menus for the user
 */

import javax.swing.JButton;
import javax.swing.JPanel;

import mvc.view.*;
import utils.Settings;

public class NDSView {

    /**
     * Default constructor.
     */
    public NDSView(Settings settings) {
        this.settings = settings;
    }

    /**
     * Initialise all all sub-panels. Sub-panels are the views that appear in the
     * main section of the window.
     */
    public void init() {
        // Main Window
        main = new ViewMain(this, settings);
        main.init();

        // Customer View
        customer = new ViewCustomers(this, settings);
        newCustomer = new ViewNewCustomer(this, settings);
        absent = new ViewAbsent(this, settings);

        // Orders View
        order = new ViewOrders(this, settings);
        newOrder = new ViewNewOrder(this, settings);

        // Publications
        publication = new ViewPublications(this, settings);
        newPublication = new ViewNewPublication(this, settings);

        // Deliveries
        deliveries = new ViewDeliveries(this, settings);

        // Invoices
        invoices = new ViewInvoices(this, settings);

        // Summary
        summary = new ViewSummary(this, settings);

        // Stock
        stock = new ViewStock(this, settings);

        // Drivers
        driver = new ViewDrivers(this, settings);
        newDriver = new ViewNewDriver(this, settings);

        // Regions
        region = new ViewRegion(this, settings);
        newRegion = new ViewNewRegion(this, settings);

        // Status bar
        status = new ViewStatus(this, settings);

        // Dialog
        dialog = new ViewDialogs(this, settings);
    }

    /**
     * Change the current view of the application to the desired panel
     *
     * @param panel
     *            Panel that will be shown in the application's main view
     * @param button
     *            Button that will be highlighted as the indicator of the active
     *            panel
     */
    public void changePanel(JPanel panel, JButton button) {
        main.setButtonPressed(button);
        main.jPanelMainView.removeAll();
        main.jPanelMainView.add(panel);
        main.jPanelMainView.updateUI();
    }

    // Settings
    private Settings settings;

    // Main Window
    public ViewMain main;

    // View Panels
    public ViewCustomers customer;
    public ViewNewCustomer newCustomer;
    public ViewAbsent absent;

    public ViewOrders order;
    public ViewNewOrder newOrder;

    public ViewPublications publication;
    public ViewNewPublication newPublication;

    public ViewDeliveries deliveries;

    public ViewInvoices invoices;

    public ViewSummary summary;

    public ViewStock stock;

    public ViewDrivers driver;
    public ViewNewDriver newDriver;

    public ViewRegion region;
    public ViewNewRegion newRegion;

    // Status bar
    public ViewStatus status;

    // Dialogs
    public ViewDialogs dialog;
}
