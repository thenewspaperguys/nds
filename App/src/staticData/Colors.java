package staticData;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class containing static Color constants and a convenience method to color a
 * button
 */
public class Colors {

    final public static Color PRIMARY = new Color(2, 117, 216);
    final public static Color DARKPRIMARY = new Color(2, 75, 140);
    final public static Color SUCCESS = new Color(22, 114, 22);
    final public static Color INFO = new Color(91, 192, 222);
    final public static Color WARNING = new Color(160, 103, 0);
    final public static Color DANGER = new Color(187, 53, 49);

    // Override BLACK and WHITE of the "Color" class to keep everything in one class
    final public static Color BLACK = new Color(0, 0, 0);
    final public static Color WHITE = new Color(255, 255, 255);
    final public static Color GRAY = new Color(214, 217, 223);
    final public static Color DARKGRAY = new Color(60, 60, 60);

    /**
     * Set the color of a button's background and foreground
     * 
     * @param button
     *            The button to be affected
     * @param background
     *            Color of the background
     * @param foreground
     *            Color of the foreground
     */
    public static void setButtonColors(JButton button, Color background, Color foreground) {
        if (background != null) {
            button.setBackground(background);
        }
        if (foreground != null) {
            button.setForeground(foreground);
        }
    }

    /**
     * Set the color of a panel's background and foreground
     * 
     * @param panel
     *            The button to be affected
     * @param background
     *            Color of the background
     * @param foreground
     *            Color of the foreground
     */
    public static void setPanelColors(JPanel panel, Color background, Color foreground) {
        if (background != null) {
            panel.setBackground(background);
        }
        if (foreground != null) {
            panel.setForeground(foreground);
        }
    }

    /**
     * Set the color of a label's background and foreground
     * 
     * @param label
     *            The button to be affected
     * @param background
     *            Color of the background
     * @param foreground
     *            Color of the foreground
     */
    public static void setLabelColors(JLabel label, Color background, Color foreground) {
        if (background != null) {
            label.setBackground(background);
        }
        if (foreground != null) {
            label.setForeground(foreground);
        }
    }
}
