package dataStructures;

import utils.SimpleDate;

/**
 * Order Data Structure for convenient information passing in the application
 */
public class Order {
    private Customer customer;
    private Publication publication;
    private int qty, ID;
    private String frequency, status;
    private SimpleDate nextDelivery;

    public Order() {
        //
    }

    public Order(int ID, Customer customer, Publication publication, int qty, String frequency, String status, SimpleDate nextDelivery) {
        this.ID = ID;
        this.customer = customer;
        this.publication = publication;
        this.qty = qty;
        this.frequency = frequency;
        this.nextDelivery = nextDelivery;
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SimpleDate getNextDelivery() {
        return nextDelivery;
    }

    public void setNextDelivery(SimpleDate nextDelivery) {
        this.nextDelivery = nextDelivery;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     *
     * @return Array of Objects
     */
    public Object[] toArray() {

        // output the array as idx[0] customer, idx[1] nested array of publications
        return new Object[] { ID, customer.getName(), publication.getTitle(), qty, frequency, status, nextDelivery };
    }
}
