package dataStructures;

public class Region {

    private int ID;
    private String name;

    public Region() {
        //
    }

    public Region(int ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     *
     * @return Array of Objects
     */
    public Object[] toArray() {
        return new Object[]{ID, name};
    }
}

