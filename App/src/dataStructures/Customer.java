package dataStructures;

import utils.SimpleDate;

/**
 * Customer Data Structure for convenient information passing in the application
 */
public class Customer {
    private int ID;
    private String name, phoneNumber, email, address, region, status;
    private Absent absent;

    public Customer() {
        //
    }

    public Customer(int ID) {
        this.ID = ID;
    }

    public Customer(int ID, String name, String phoneNumber, String email, String address, String region, String status,
            Absent absent) {
        this.ID = ID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.region = region;
        this.absent = absent;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Absent getAbsent() {
        return absent;
    }

    public void setAbsent(Absent absent) {
        this.absent = absent;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     *
     * @return Array of Objects
     */
    public Object[] toArray() {
        // TODO absent implementation
        String absentTo = "";
        if (getAbsent() != null) {
            SimpleDate simpleDate = getAbsent().getTo();
            absentTo = simpleDate.toString();
        }
        return new Object[] { ID, name, phoneNumber, email, address, region, status, absentTo };
    }
}
