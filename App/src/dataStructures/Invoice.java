package dataStructures;

import java.util.ArrayList;

import utils.SimpleDate;

/**
 * Invoice Data Structure for convenient information passing in the
 * application
 */
public class Invoice {
    private int ID;
    private Customer customer;
    private ArrayList<Publication> publications;
    private SimpleDate deliveryDate;
    private double priceCorrection;
    private boolean isDelivered;

    public Invoice() {

    }

    public Invoice(int iD, Customer customer, ArrayList<Publication> publications, SimpleDate deliveryDate,
            double priceCorrection, boolean isDelivered) {
        ID = iD;
        this.customer = customer;
        this.publications = publications;
        this.deliveryDate = deliveryDate;
        this.priceCorrection = priceCorrection;
        this.isDelivered = isDelivered;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<Publication> getPublications() {
        return publications;
    }

    public void setPublications(ArrayList<Publication> publications) {
        this.publications = publications;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public SimpleDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(SimpleDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public double getPriceCorrection() {
        return priceCorrection;
    }

    public void setPriceCorrection(double priceCorrection) {
        this.priceCorrection = priceCorrection;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean delivered) {
        isDelivered = delivered;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     * 
     * @return Array of Objects
     */
    public Object[] toArray() {
        // convert orders ArrayList to an array
        Object[] publicationsArray = new Object[publications.size()];

        for (int i = 0; i < publications.size(); i++) {
            publicationsArray[i] = publications.get(i).toArray();
        }

        // output the last index (idx[3]) as the array of orders
        return new Object[] { ID, deliveryDate, priceCorrection, publicationsArray, customer.toArray() };
    }
}
