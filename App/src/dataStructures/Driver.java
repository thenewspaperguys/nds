package dataStructures;

/**
 * Driver Data Structure for convenient information passing in the
 * application
 */
public class Driver {
    private int ID;
    private String name, phoneNumber;

    public Driver() {
        //
    }

    public Driver(int iD, String name, String phoneNumber) {
        ID = iD;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     * 
     * @return Array of Objects
     */
    public Object[] toArray() {
        return new Object[] { ID, name, phoneNumber };
    }
}
