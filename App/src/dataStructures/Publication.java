package dataStructures;

/**
 * Publication Data Structure for convenient information passing in the
 * application
 */
public class Publication {
    private int ID, stock, qty;
    private String title;
    private double price;

    public Publication() {
        //
    }

    public Publication(int ID) {
        this.ID = ID;
    }

    public Publication(int iD, int stock, int qty, String title, double price) {
        ID = iD;
        this.qty = qty;
        this.stock = stock;
        this.title = title;
        this.price = price;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    /**
     * Convert the object to an Array of objects for the convenience of
     * displaying the information in the GUI tables
     *
     * @return Array of Objects
     */
    public Object[] toArray() {
        return new Object[] { ID, title, price, stock, qty };
    }

    public Object[] toArrayStock() {
        return new Object[] { ID, title, stock, 0 };
    }
}
