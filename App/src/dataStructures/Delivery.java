package dataStructures;

import utils.SimpleDate;

/**
 * Delivery Data Structure for convenient information passing in the application
 */
public class Delivery {
    private int ID;
    private Order order;
    private boolean isBilled, isDelivered;
    SimpleDate deliveryDate;

    public Delivery() {
        //
    }

    public Delivery(int iD, Order order, boolean isBilled, boolean isDelivered, SimpleDate deliveryDate) {
        ID = iD;
        this.order = order;
        this.isBilled = isBilled;
        this.isDelivered = isDelivered;
        this.deliveryDate = deliveryDate;
    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public boolean isBilled() {
        return isBilled;
    }

    public void setBilled(boolean isBilled) {
        this.isBilled = isBilled;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean isDelivered) {
        this.isDelivered = isDelivered;
    }

    public SimpleDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(SimpleDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     *
     * @return Array of Objects
     */
    public Object[] toArray() {
        return new Object[] { ID, order.toArray(), isBilled, isDelivered };
    }
}
