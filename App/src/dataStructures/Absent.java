package dataStructures;

import utils.SimpleDate;

/**
 * Absent Data Structure for convenient information passing in the application
 */
public class Absent {
    private SimpleDate from, to;

    public Absent() {
        //
    }

    public Absent(SimpleDate from, SimpleDate to) {
        this.from = from;
        this.to = to;
    }

    public SimpleDate getFrom() {
        return from;
    }

    public void setFrom(SimpleDate from) {
        this.from = from;
    }

    public SimpleDate getTo() {
        return to;
    }

    public void setTo(SimpleDate to) {
        this.to = to;
    }

    public boolean isValid() {
        return from != null && to != null;
    }

    public boolean isFirstBeforeSecond() {
        return from.compareTo(to) <= 0;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     * 
     * @return Array of objects
     */
    public Object[] toArray() {
        return new Object[] { from, to };
    }

}
