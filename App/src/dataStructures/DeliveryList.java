package dataStructures;

import java.util.ArrayList;

/**
 * DeliveryList Data Structure for convenient information passing in the
 * application
 */
public class DeliveryList {
    private Driver driver;
    private ArrayList<Delivery> deliveries;

    public DeliveryList() {
        //
    }

    public DeliveryList(Driver driver, ArrayList<Delivery> deliveries) {
        this.driver = driver;
        this.deliveries = deliveries;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public ArrayList<Delivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(ArrayList<Delivery> deliveries) {
        this.deliveries = deliveries;
    }

    /**
     * Convert the object to an Array of objects for the convenience of displaying
     * the information in the GUI tables
     * 
     * @return Array of Objects
     */
    public Object[] toArray() {
        return new Object[] { driver.toArray(), deliveries.toArray() };
    }
}
