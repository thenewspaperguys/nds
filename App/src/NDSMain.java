import mvc.NDSController;
import utils.Settings;

/**
 * Newspaper Delivery System
 * Main wrapper class
 * 
 * @author Damian Chrzanowski, Simon Harper, Mateusz Dziduch, Fei Wang, Conor Farrell
 */
public class NDSMain {

    public static void main(String[] args) {

        Settings settings = new Settings("./AppSettings.xml");

        NDSController app = new NDSController(settings);

        app.init();
    }
}
