DROP DATABASE IF EXISTS nds;
CREATE DATABASE IF NOT EXISTS nds;
USE nds;

DROP TABLE IF EXISTS Region;
DROP TABLE IF EXISTS Customer;
DROP TABLE IF EXISTS Absent;
DROP TABLE IF EXISTS Driver;
DROP TABLE IF EXISTS Publication;
DROP TABLE IF EXISTS Order_;
DROP TABLE IF EXISTS Delivery;
DROP TABLE IF EXISTS Invoice;
DROP TABLE IF EXISTS InvDetail;

CREATE TABLE Region (
region_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
name VARCHAR(30)
);

CREATE TABLE Customer (
cus_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
name VARCHAR(100) NOT NULL,
region_id INTEGER NOT NULL,
status VARCHAR(10) NOT NULL,
phone_number VARCHAR(20) NOT NULL,
address TEXT NOT NULL,
email VARCHAR(40),

FOREIGN KEY (region_id) REFERENCES Region(region_id)
);


CREATE TABLE Absent (
cus_id INTEGER NOT NULL,
absent_from DATE NOT NULL,
absent_to DATE NOT NULL
);


CREATE TABLE Driver (
driver_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
name VARCHAR(100) NOT NULL,
phone_number VARCHAR(20) NOT NULL
);

CREATE TABLE Publication (
pub_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
stock INTEGER NOT NULL,
title VARCHAR(200) NOT NULL,
price FLOAT NOT NULL
);


CREATE TABLE Order_ (
order_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
pub_id INTEGER NOT NULL,
cus_id INTEGER NOT NULL,
qty INTEGER NOT NULL,
frequency VARCHAR(5) NOT NULL,
status VARCHAR(10) NOT NULL,
next_delivery DATE NOT NULL,
FOREIGN KEY (pub_id) REFERENCES Publication(pub_id),
FOREIGN KEY (cus_id) REFERENCES Customer(cus_id)
);


CREATE TABLE Delivery (
delivery_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
order_id INTEGER NOT NULL,
driver_id INTEGER NOT NULL,
is_delivered TINYINT(1) NOT NULL,
is_billed TINYINT(1) NOT NULL,
delivery_date DATE NOT NULL,
FOREIGN KEY (order_id) REFERENCES Order_(order_id),
FOREIGN KEY (driver_id) REFERENCES Driver(driver_id)
);


CREATE TABLE Invoice (
inv_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
cus_id INTEGER NOT NULL,
price_correction FLOAT,
delivery_date DATE,
is_delivered TINYINT(1) NOT NULL,
FOREIGN KEY (cus_id) REFERENCES Customer(cus_id)
);

CREATE TABLE InvDetail (
inv_id INTEGER NOT NULL,
pub_id INTEGER NOT NULL,
qty INTEGER NOT NULL,
PRIMARY KEY (inv_id, pub_id),
FOREIGN KEY (inv_id) REFERENCES Invoice(inv_id),
FOREIGN KEY (pub_id) REFERENCES Publication(pub_id)
);

CREATE TABLE InvoiceDate (
invoice_date DATE
);


INSERT INTO Region VALUES ( null, 'Monksland');
INSERT INTO Region VALUES ( null, 'Retreat Heights');
INSERT INTO Region VALUES ( null, 'Auburn Heights');
INSERT INTO Region VALUES ( null, 'Wellmount');
INSERT INTO Region VALUES ( null, 'Willow');
INSERT INTO Region VALUES ( null, 'Glen Abhainn');
INSERT INTO Region VALUES ( null, 'Valley Court');
INSERT INTO Region VALUES ( null, 'Rockbrooke');




INSERT INTO Customer VALUES ( null, 'Joe Mullins', 1, 'Active', '0871234567', 'Monksland', 'joemullins123@gmail.com');
INSERT INTO Customer VALUES ( null, 'Jenna Burke', 2, 'Active', '0871234568', 'Retreat Heights', 'jennaburke1@gmail.com');
INSERT INTO Customer VALUES ( null, 'David McCabe', 3, 'Active', '0871234569', 'Auburn Heights', 'davidmccabe2018@gmail.com');
INSERT INTO Customer VALUES ( null, 'Stephen Browne', 4, 'Inactive', '0871234570', 'Wellmount', 'stephenbrowne76@gmail.com');
INSERT INTO Customer VALUES ( null, 'Adam Byrne', 5, 'Inactive', '0871234571', 'Willow', 'adambyrne0@gmail.com');
INSERT INTO Customer VALUES ( null, 'Jack Flynn', 6, 'Active', '0871234572', 'Glen Abhainn', 'jack2@gmail.com');
INSERT INTO Customer VALUES ( null, 'Sophie Williams', 7, 'Inactive', '0871234573', 'Valley Court', 'swilliams@gmail.com');
INSERT INTO Customer VALUES ( null, 'Mary Conlon', 8, 'Active', '0871234574', 'Rockbrooke', 'conlonmary@gmail.com');
INSERT INTO Customer VALUES ( null, 'Rob Blake', 3, 'Inactive', '0871234575', 'Auburn Heights', 'rb123@gmail.com');
INSERT INTO Customer VALUES ( null, 'Claire FitzGerald', 4, 'Active', '0871234576', 'Wellmount', 'claireee@gmail.com');




INSERT INTO Publication VALUES ( null, 50, 'The Irish Times', 2.00);
INSERT INTO Publication VALUES ( null, 50, 'The Sun', 1.00);
INSERT INTO Publication VALUES ( null, 50, 'The Daily Star', 1.50);
INSERT INTO Publication VALUES ( null, 50, 'The Irish Independant', 2.00);
INSERT INTO Publication VALUES ( null, 50, 'Irish Daily Mail', 1.40);
INSERT INTO Publication VALUES ( null, 50, 'Time Magazine', 4.99);
INSERT INTO Publication VALUES ( null, 50, 'Mens Health', 2.50);
INSERT INTO Publication VALUES ( null, 50, 'Rolling Stone', 4.99);
INSERT INTO Publication VALUES ( null, 50, 'Taste of Home', 1.99);
INSERT INTO Publication VALUES ( null, 50, 'Cosmopolitan', 4.99);
INSERT INTO Publication VALUES ( null, 50, 'Harry Potter', 9.99);
INSERT INTO Publication VALUES ( null, 50, 'The Boy In The Striped Pyjamas', 11.99);
INSERT INTO Publication VALUES ( null, 50, 'The Kings Speech', 10.00);
INSERT INTO Publication VALUES ( null, 50, 'King Lear', 8.00);
INSERT INTO Publication VALUES ( null, 50, 'The Golden Compass', 5.99);


INSERT INTO Driver VALUES ( null, 'Ben Reilly', '0871239876');
INSERT INTO Driver VALUES ( null, 'Brad Layfeild', '0871239875');
INSERT INTO Driver VALUES ( null, 'John Lynch', '0871239874');
INSERT INTO Driver VALUES ( null, 'Karl White', '0871239873');
INSERT INTO Driver VALUES ( null, 'Michael Peterson', '0871239872');
INSERT INTO Driver VALUES ( null, 'Brian Nolan', '0871239871');
INSERT INTO Driver VALUES ( null, 'Cody Walters', '0871239870');
INSERT INTO Driver VALUES ( null, 'Chris Farrell', '0871239869');
INSERT INTO Driver VALUES ( null, 'Paul Higgins', '0871239868');
INSERT INTO Driver VALUES ( null, 'Trevor Phillips', '0871239867');


INSERT INTO Order_ VALUES ( null, 3, 4, 2, 'Day', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 1, 4, 3, 'Day', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 7, 4, 1, 'Week', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 7, 1, 2, 'Day', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 5, 5, 1, 'Month', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 9, 5, 1, 'Month', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 10, 3, 3, 'Day', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 7, 3, 1, 'Week', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 8, 3, 2, 'Day', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 4, 3, 1, 'Month', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 9, 10, 1, 'Week', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 2, 7, 3, 'Day', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 6, 7, 1, 'Day', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 4, 2, 2, 'Week', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 8, 2, 2, 'Month', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 9, 2, 1, 'Month', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 6, 9, 1, 'Day', 'Inactive', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 8, 8, 2, 'Month', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 10, 6, 1, 'Day', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 1, 6, 2, 'Day', 'Active', '2018-02-16');
INSERT INTO Order_ VALUES ( null, 2, 6, 3, 'Day', 'Active', '2018-02-16');


INSERT INTO Delivery VALUES ( null, 18, 1, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 12, 1, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 7, 2, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 4, 2, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 2, 3, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 8, 3, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 11, 4, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 17, 4, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 20, 5, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 19, 5, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 6, 6, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 21, 6, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 3, 7, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 9, 7, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 5, 8, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 1, 8, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 13, 9, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 15, 9, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 14, 10, 0, 0, '2018-03-15');
INSERT INTO Delivery VALUES ( null, 16, 10, 0, 0, '2018-03-20');
INSERT INTO Delivery VALUES ( null, 10, 10, 0, 0, '2018-03-20');


INSERT INTO Absent VALUES ( 4, '2018-01-30', '2018-05-23');
INSERT INTO Absent VALUES ( 5, '2018-01-15', '2018-06-15');
INSERT INTO Absent VALUES ( 7, '2018-01-07', '2018-03-10');
INSERT INTO Absent VALUES ( 9, '2018-02-03', '2018-02-26');

INSERT INTO InvoiceDate VALUES('2018-01-1');
